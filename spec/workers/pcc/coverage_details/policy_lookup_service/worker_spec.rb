# frozen_string_literal: true

require 'rails_helper'

describe PCC::CoverageDetails::PolicyLookupService::Worker, vcr: true do
  subject(:worker_perform) do
    PCC::CoverageDetails::PolicyLookupService::Worker.new.perform(async_request.id, job.id, policy&.id, subject_args)
  end

  let(:async_request) { create(:api_async_request, company: lincoln, user: user) }
  let(:lincoln) { create(:fleet_company, :lincoln) }
  let(:user) { create(:user, company: lincoln) }
  let(:vin) { '5TFMU4FNXFX073781' }
  let!(:job) do
    create :fleet_managed_job, {
      fleet_company: lincoln,
      service_location: service_location,
    }
  end

  let!(:service_location) do
    create :location, {
      lat: 37.924513,
      lng: -121.230777,
    }
  end

  let(:warranty_coverage_notes) { 'These are the warranty coverage notes' }
  let!(:lincoln_warranty_program) do
    ClientProgram.create!(name: "Warranty",
                          company: lincoln,
                          code: 'lincoln_warranty',
                          coverage_filename: 'lincoln_warranty',
                          coverage_notes: warranty_coverage_notes,
                          priority: false,
                          rank: 4)
  end

  let!(:lincoln_manual_program) do
    ClientProgram.create!(name: "Manual Entry",
                          company: lincoln,
                          code: 'lincoln_manual',
                          coverage_filename: 'lincoln_manual',
                          priority: false,
                          rank: 20)
  end

  let!(:advance_auto_site) do
    create :fleet_site, :advance_auto_site, {
      company: lincoln,
    }
  end

  let!(:fell_street_site) do
    create :fleet_site, :fell_street_site, {
      company: lincoln,
    }
  end

  let!(:sunrise_auto_site) do
    create :fleet_site, :sunrise_auto_site, {
      company: lincoln,
    }
  end

  let!(:dr_auto_site) do
    create :fleet_site, :dr_auto_site, {
      company: lincoln,
    }
  end

  let!(:domi_autobody_site) do
    create :poi_site, :domi_autobody_site, {
      company: lincoln,
    }
  end

  let(:pcc_coverage) do
    PCC::Coverage.create!({
      pcc_policy: policy,
      pcc_vehicle_id: pcc_vehicle&.id,
      pcc_driver_id: pcc_driver&.id,
    })
  end

  context 'No Policy' do
    let(:policy) { nil }
    let(:pcc_vehicle) { nil }
    let(:pcc_driver) { nil }

    describe "Manual Covered" do
      let(:subject_args) do
        {
          "job" => {
            :id => job.id,
            "service" => { "name" => ServiceCode::BATTERY_JUMP },
            "vehicle" => {
              "year" => Time.now.year - 6,
              "odometer" => 50000,
            },
          },
        }
      end

      let(:policy_coverage_return) do
        {
          async_request_id: async_request.id,
          input: {
            pcc_coverage_id: pcc_coverage.id,
            pcc_policy_id: nil,
            pcc_vehicle_id: nil,
            pcc_driver_id: nil,
            programs: [
              {
                code: "lincoln_manual",
                coverage_issues: [],
                coverage_notes: nil,
                priority: false,
                failed_conditions: [
                  "job.service == \"Accident Tow\"",
                  "job.service == \"Tow\"",
                ],
                limitations: [],
                name: "Manual Entry",
                rank: 20.0,
                result: "Covered",
                successful_conditions: [
                  "vehicle.odometer < 70000",
                  "vehicle.year >= Utils::YearsAgo(6)",
                ],
              },
            ],
          },
        }
      end

      it 'returns manual covered' do
        expect(PCC::Coverage).to receive(:create!).and_return(pcc_coverage)
        expect(PCC::Publishers::Websocket).to receive(:call).with(policy_coverage_return)

        worker_perform
      end
    end

    describe "Manual Not Covered" do
      let(:subject_args) do
        {
          "job" => {
            :id => job.id,
            "vehicle" => {
              "year" => Time.now.year - 7,
            },
          },
        }
      end

      let(:policy_coverage_return) do
        {
          async_request_id: async_request.id,
          input: {
            pcc_coverage_id: pcc_coverage.id,
            pcc_policy_id: nil,
            pcc_vehicle_id: nil,
            pcc_driver_id: nil,
            programs: [
              {
                code: "lincoln_manual",
                coverage_issues: ["Model year must be 2013 or newer"],
                coverage_notes: nil,
                priority: false,
                failed_conditions: ["vehicle.year >= Utils::YearsAgo(6)"],
                limitations: ["Customer pays full cost of service"],
                name: "Manual Entry",
                rank: 20.0,
                result: "NotCovered",
                successful_conditions: [],
              },
            ],
          },
        }
      end

      it 'returns manual not covered' do
        expect(PCC::Coverage).to receive(:create!).and_return(pcc_coverage)
        expect(PCC::Publishers::Websocket).to receive(:call).with(policy_coverage_return)

        worker_perform
      end
    end
  end

  context "has policy" do
    let(:policy_uuid) { "823b8fe3080e910a1b019677565550fe" }
    let(:program_expired) { false }
    let!(:policy) do
      create :pcc_policy, {
        job: job,
        uuid: policy_uuid,
        data: {
          "uuid": policy_uuid,
          "client_code": "lincoln",
          "client_name": "Lincoln Motor Company",
          "programs": programs,
          "vehicles": [
            {
              "uuid": "21d5c75810593bdf27b30f9fe1e67ae0",
            },
          ],
          "customers": [
            {
              "uuid": "a1808a6edb72214c684cda285fd9f2d8",
            },
          ],
        },
      }
    end

    let!(:pcc_vehicle) do
      create :pcc_vehicle, {
        "uuid": "21d5c75810593bdf27b30f9fe1e67ae0",
      }
    end

    let!(:pcc_driver) do
      create :pcc_user, {
        "uuid": "a1808a6edb72214c684cda285fd9f2d8",
      }
    end

    context 'warranty' do
      let(:programs) do
        [
          {
            "uuid": "c9ac68f03f9153c706556718e7f8da3e",
            "start_date": "2018-12-01",
            "end_date": "2024-12-01",
            "cancel_date": nil,
            "program_code": "lincoln_warranty",
            "program_name": "Lincoln Warranty",
            "program_identifier": nil,
            "expired": program_expired,
            "site_code": "12345T",
            "details": {
              "dealer_code": "L25261",
            },
          },
        ]
      end

      describe "covered" do
        let(:subject_args) do
          {
            "pcc_policy_id" => policy.id,
            "pcc_vehicle_id" => pcc_vehicle.id,
            "pcc_driver_id" => pcc_driver.id,
            "job" => {
              "service" => { "name" => ServiceCode::BATTERY_JUMP },
              "vehicle" => {
                "year" => 1980,
                "odometer" => 55000,
              },
              "location" => {
                "service_location" => { "lat" => 37.924513, "lng" => -121.230777 }, # Stockton, CA
                "dropoff_location" => { "site_id" => fell_street_site.id },
              },
            },
          }
        end

        let(:policy_coverage_return) do
          {
            async_request_id: async_request.id,
            input: {
              pcc_coverage_id: pcc_coverage.id,
              pcc_policy_id: policy.id,
              pcc_vehicle_id: pcc_vehicle.id,
              pcc_driver_id: pcc_driver.id,
              programs: [
                {
                  code: "lincoln_warranty",
                  coverage_issues: [],
                  coverage_notes: warranty_coverage_notes,
                  priority: false,
                  failed_conditions: [
                    "job.service == \"Accident Tow\"",
                    "job.service == \"Tow\"",
                  ],
                  limitations: [],
                  name: "Lincoln Warranty",
                  rank: 4.0,
                  result: "Covered",
                  site_code: "12345T",
                  successful_conditions: [
                    "vehicle.odometer < 70000",
                    "program.expired == false",
                  ],
                },
              ],
            },
          }
        end

        it 'returns covered result' do
          expect(PCC::Coverage).to receive(:create!).and_return(pcc_coverage)
          expect(PCC::Publishers::Websocket).to receive(:call).with(policy_coverage_return)

          worker_perform
        end
      end

      describe "not covered" do
        let(:subject_args) do
          {
            "pcc_policy_id" => policy.id,
            "pcc_vehicle_id" => pcc_vehicle.id,
            "pcc_driver_id" => pcc_driver.id,
            "job" => {
              "service" => { "name" => ServiceCode::ACCIDENT_TOW },
              "vehicle" => {
                "year" => 1980,
                "odometer" => 100000,
              },
              "location" => {
                "service_location" => { "lat" => 37.924513, "lng" => -121.230777 }, # Stockton, CA,
                "dropoff_location" => { "site_id" => advance_auto_site.id },
              },
            },
          }
        end

        let(:policy_coverage_return) do
          {
            async_request_id: async_request.id,
            input: {
              pcc_coverage_id: pcc_coverage.id,
              pcc_policy_id: policy.id,
              pcc_vehicle_id: pcc_vehicle.id,
              pcc_driver_id: pcc_driver.id,
              programs: [
                {
                  code: "lincoln_warranty",
                  coverage_issues: ["Odometer Limit: 70000 miles"],
                  coverage_notes: warranty_coverage_notes,
                  failed_conditions: ["vehicle.odometer < 70000"],
                  priority: false,
                  limitations: ["Customer pays full cost of service"],
                  name: "Lincoln Warranty",
                  rank: 4.0,
                  result: "NotCovered",
                  site_code: "12345T",
                  successful_conditions: [
                    "program.expired == false",
                  ],
                },
              ],
            },
          }
        end

        it 'returns not covered result' do
          expect(PCC::Coverage).to receive(:create!).and_return(pcc_coverage)
          expect(PCC::Publishers::Websocket).to receive(:call).with(policy_coverage_return)

          worker_perform
        end
      end

      describe "partially covered" do
        let(:subject_args) do
          {
            "pcc_policy_id" => policy.id,
            "pcc_vehicle_id" => pcc_vehicle.id,
            "pcc_driver_id" => pcc_driver.id,
            "job" => {
              "service" => { "name" => ServiceCode::TOW },
              "vehicle" => {
                "year" => 1980,
                "odometer" => 55000,
              },
              "location" => {
                "service_location" => { "lat" => 37.924513, "lng" => -121.230777 }, # Stockton, CA,
                "dropoff_location" => { "site_id" => dr_auto_site.id },
              },
            },
          }
        end

        let(:policy_coverage_return) do
          {
            async_request_id: async_request.id,
            input: {
              pcc_coverage_id: pcc_coverage.id,
              pcc_policy_id: policy.id,
              pcc_vehicle_id: pcc_vehicle.id,
              pcc_driver_id: pcc_driver.id,
              programs: [
                {
                  code: "lincoln_warranty",
                  coverage_issues: ["Distance limit: 100 miles"],
                  coverage_notes: warranty_coverage_notes,
                  priority: false,
                  failed_conditions: ["dropoff.distance < 100"],
                  limitations: ["Customer pays overage: 426 miles"],
                  name: "Lincoln Warranty",
                  rank: 4.0,
                  result: "PartiallyCovered",
                  site_code: "12345T",
                  successful_conditions: [
                    "dropoff.type == \"Dealership\"",
                    "job.service == \"Tow\"",
                    "vehicle.odometer < 70000",
                    "program.expired == false",
                  ],
                },
              ],
            },
          }
        end

        it 'returns partially covered result' do
          expect(PCC::Coverage).to receive(:create!).and_return(pcc_coverage)
          expect(PCC::Publishers::Websocket).to receive(:call).with(policy_coverage_return)

          worker_perform
        end
      end

      describe "missing args" do
        let(:subject_args) do
          {
            "pcc_policy_id" => policy.id,
            "pcc_vehicle_id" => pcc_vehicle.id,
            "pcc_driver_id" => pcc_driver.id,
            "job" => {
              "vehicle" => {
                "year" => 1980,
                "odometer" => 55000,
              },
              "location" => {
                "service_location" => { "lat" => 37.924513, "lng" => -121.230777 }, # Stockton, CA,
                "dropoff_location" => { "site_id" => advance_auto_site.id },
              },
            },
          }
        end

        let(:policy_coverage_return) do
          {
            async_request_id: async_request.id,
            input: {
              pcc_coverage_id: pcc_coverage.id,
              pcc_policy_id: policy.id,
              pcc_vehicle_id: pcc_vehicle.id,
              pcc_driver_id: pcc_driver.id,
              programs: [
                {
                  code: "lincoln_warranty",
                  coverage_notes: warranty_coverage_notes,
                  priority: false,
                  name: "Lincoln Warranty",
                  rank: 4.0,
                  result: "Failed",
                  site_code: "12345T",
                  failed_reasons: ["Missing: job.service"],
                },
              ],
            },
          }
        end

        it 'returns failed message' do
          expect(PCC::Coverage).to receive(:create!).and_return(pcc_coverage)
          expect(PCC::Publishers::Websocket).to receive(:call).with(policy_coverage_return)

          worker_perform
        end
      end

      describe "missing drop" do
        let(:subject_args) do
          {
            "pcc_policy_id" => policy.id,
            "pcc_vehicle_id" => pcc_vehicle.id,
            "pcc_driver_id" => pcc_driver.id,
            "job" => {
              "service" => { "name" => ServiceCode::TOW },
              "vehicle" => {
                "year" => 1980,
                "odometer" => 55000,
              },
              "location" => {
                "service_location" => { "lat" => 34.5, "lng" => -90.5 },
              },
            },
          }
        end

        let(:policy_coverage_return) do
          {
            async_request_id: async_request.id,
            input: {
              pcc_coverage_id: pcc_coverage.id,
              pcc_policy_id: policy.id,
              pcc_vehicle_id: pcc_vehicle.id,
              pcc_driver_id: pcc_driver.id,
              programs: [
                {
                  code: "lincoln_warranty",
                  coverage_notes: warranty_coverage_notes,
                  priority: false,
                  name: "Lincoln Warranty",
                  rank: 4.0,
                  result: "Failed",
                  site_code: "12345T",
                  failed_reasons: ["Missing: dropoff.type"],
                },
              ],
            },
          }
        end

        it 'returns failed message' do
          expect(PCC::Coverage).to receive(:create!).and_return(pcc_coverage)
          expect(PCC::Publishers::Websocket).to receive(:call).with(policy_coverage_return)

          worker_perform
        end
      end

      describe "program expired" do
        let(:program_expired) { true }

        let(:subject_args) do
          {
            "pcc_policy_id" => policy.id,
            "pcc_vehicle_id" => pcc_vehicle.id,
            "pcc_driver_id" => pcc_driver.id,
            "job" => {
              "vehicle" => {
                "year" => 1980,
                "odometer" => 55000,
              },
              "location" => {
                "service_location" => { "lat" => 34.5, "lng" => -90.5 },
                "dropoff_location" => { "site_id" => advance_auto_site.id },
              },
            },
          }
        end

        let(:policy_coverage_return) do
          {
            async_request_id: async_request.id,
            input: {
              pcc_coverage_id: pcc_coverage.id,
              pcc_policy_id: policy.id,
              pcc_vehicle_id: pcc_vehicle.id,
              pcc_driver_id: pcc_driver.id,
              programs: [
                {
                  code: "lincoln_warranty",
                  coverage_issues: ["Program Expired"],
                  coverage_notes: warranty_coverage_notes,
                  priority: false,
                  failed_conditions: ["program.expired == false"],
                  limitations: ["Customer pays full cost of service"],
                  name: "Lincoln Warranty",
                  rank: 4.0,
                  result: "NotCovered",
                  site_code: "12345T",
                  successful_conditions: [],
                },
              ],
            },
          }
        end

        it 'returns not covered result' do
          expect(PCC::Coverage).to receive(:create!).and_return(pcc_coverage)
          expect(PCC::Publishers::Websocket).to receive(:call).with(policy_coverage_return)

          worker_perform
        end
      end
    end

    context 'Lincoln For Life' do
      let!(:program_name) { "Lincoln For Life" }
      let!(:program_code) { "lincoln_for_life" }
      let(:lfl_coverage_notes) { 'These are the LFL coverage notes' }
      let!(:lincoln_lfl_program) do
        ClientProgram.create!(name: program_name,
                              company: lincoln,
                              code: program_code,
                              coverage_filename: program_code,
                              coverage_notes: lfl_coverage_notes,
                              rank: 19)
      end

      let(:programs) do
        [
          {
            "uuid": "c9ac68f03f9153c706556718e7f8da3e",
            "start_date": "2018-12-01",
            "end_date": "2024-12-01",
            "cancel_date": nil,
            "program_code": program_code,
            "program_name": program_name,
            "program_identifier": nil,
            "expired": program_expired,
            "site_code": "12345T",
            "details": {
              "dealer_code": "L25261",
            },
          },
        ]
      end

      let(:subject_args) do
        {
          "pcc_policy_id" => policy.id,
          "pcc_vehicle_id" => pcc_vehicle.id,
          "pcc_driver_id" => pcc_driver.id,
          "job" => {
            "service" => { "name" => ServiceCode::BATTERY_JUMP },
          },
        }
      end

      describe "Not expired Battery Jump" do
        let(:policy_coverage_return) do
          {
            async_request_id: async_request.id,
            input: {
              pcc_coverage_id: pcc_coverage.id,
              pcc_policy_id: policy.id,
              pcc_vehicle_id: pcc_vehicle.id,
              pcc_driver_id: pcc_driver.id,
              programs: [
                {
                  code: program_code,
                  coverage_issues: [],
                  coverage_notes: lfl_coverage_notes,
                  priority: false,
                  failed_conditions: [
                    "job.service == \"Accident Tow\"",
                    "job.service == \"Tow\"",
                  ],
                  limitations: [],
                  name: program_name,
                  rank: 19.0,
                  result: "Covered",
                  site_code: "12345T",
                  successful_conditions: [
                    "program.expired == false",
                  ],
                },
              ],
            },
          }
        end

        it 'returns covered result' do
          expect(PCC::Coverage).to receive(:create!).and_return(pcc_coverage)
          expect(PCC::Publishers::Websocket).to receive(:call).with(policy_coverage_return)

          worker_perform
        end
      end

      describe "Expired Battery Jump" do
        let(:program_expired) { true }
        let(:policy_coverage_return) do
          {
            async_request_id: async_request.id,
            input: {
              pcc_coverage_id: pcc_coverage.id,
              pcc_policy_id: policy.id,
              pcc_vehicle_id: pcc_vehicle.id,
              pcc_driver_id: pcc_driver.id,
              programs: [
                {
                  code: program_code,
                  coverage_issues: ["Program Expired"],
                  coverage_notes: lfl_coverage_notes,
                  priority: false,
                  failed_conditions: [
                    "program.expired == false",
                  ],
                  limitations: ["Customer pays full cost of service"],
                  name: program_name,
                  rank: 19.0,
                  result: "NotCovered",
                  site_code: "12345T",
                  successful_conditions: [],
                },
              ],
            },
          }
        end

        it 'returns not covered result' do
          expect(PCC::Coverage).to receive(:create!).and_return(pcc_coverage)
          expect(PCC::Publishers::Websocket).to receive(:call).with(policy_coverage_return)

          worker_perform
        end
      end
    end

    context 'Lincoln Major Daily Rental (MDR)' do
      let!(:program_name) { "Major Daily Rental" }
      let!(:program_code) { "lincoln_major_daily_rental" }
      let(:coverage_notes) { 'These are the MDR coverage notes' }
      let!(:lincoln_mdr_program) do
        ClientProgram.create!(name: program_name,
                              company: lincoln,
                              code: program_code,
                              coverage_filename: program_code,
                              coverage_notes: coverage_notes,
                              rank: 1)
      end

      let(:programs) do
        [
          {
            "uuid": "c9ac68f03f9153c706556718e7f8da3e",
            "start_date": "2018-12-01",
            "end_date": "2024-12-01",
            "cancel_date": nil,
            "program_code": program_code,
            "program_name": program_name,
            "program_identifier": nil,
            "expired": program_expired,
            "site_code": "12345T",
            "details": {
              "dealer_code": "L25261",
            },
          },
        ]
      end

      let(:subject_args) do
        {
          "pcc_policy_id" => policy.id,
          "pcc_vehicle_id" => pcc_vehicle.id,
          "pcc_driver_id" => pcc_driver.id,
          "job" => {
            "service" => { "name" => ServiceCode::TOW },
            "vehicle" => {
              "year" => 1980,
              "odometer" => 55000,
            },
            "location" => {
              "service_location" => { "lat" => 37.7830241, "lng" => -122.4249327 },
              "dropoff_location" => { "site_id" => fell_street_site.id },
            },
          },
        }
      end

      describe "Not expired Tow to Dealer" do
        let(:policy_coverage_return) do
          {
            async_request_id: async_request.id,
            input: {
              pcc_coverage_id: pcc_coverage.id,
              pcc_policy_id: policy.id,
              pcc_vehicle_id: pcc_vehicle.id,
              pcc_driver_id: pcc_driver.id,
              programs: [
                {
                  code: program_code,
                  coverage_issues: [],
                  coverage_notes: coverage_notes,
                  priority: false,
                  failed_conditions: [],
                  limitations: [],
                  name: program_name,
                  rank: 1.0,
                  result: "Covered",
                  site_code: "12345T",
                  successful_conditions: [
                    "dropoff.type == \"Dealership\"",
                    "vehicle.odometer < 70000",
                    "job.service == \"Tow\"",
                    "program.expired == false",
                  ],
                },
              ],
            },
          }
        end

        it 'returns covered result' do
          expect(PCC::Coverage).to receive(:create!).and_return(pcc_coverage)
          expect(PCC::Publishers::Websocket).to receive(:call).with(policy_coverage_return)

          worker_perform
        end
      end
    end

    context 'Lincoln ESP Collision' do
      let!(:program_name) { "ESP" }
      let!(:program_code) { "lincoln_esp_99996" }
      let(:coverage_notes) { 'These are the ESP coverage notes' }
      let!(:lincoln_esp_program) do
        ClientProgram.create!(name: program_name,
                              company: lincoln,
                              code: program_code,
                              coverage_filename: 'lincoln_esp_collision',
                              coverage_notes: coverage_notes,
                              rank: 5)
      end

      let(:programs) do
        [
          {
            "uuid": "c9ac68f03f9153c706556718e7f8da3e",
            "start_date": "2018-12-01",
            "end_date": "2024-12-01",
            "cancel_date": nil,
            "program_code": program_code,
            "program_name": program_name,
            "program_identifier": nil,
            "expired": program_expired,
            "site_code": "12345T",
            "details": {
              "esp_program_code": "NNF000",
              "odometer_start": 11507,
              "odometer_limit": 75000,
              "plan_sold_date": "11262018",
            },
          },
        ]
      end

      context 'Tow to Dealership' do
        let(:subject_args) do
          {
            "pcc_policy_id" => policy.id,
            "pcc_vehicle_id" => pcc_vehicle.id,
            "pcc_driver_id" => pcc_driver.id,
            "job" => {
              "service" => { "name" => ServiceCode::TOW },
              "vehicle" => {
                "year" => 1980,
                "odometer" => 55000,
              },
              "location" => {
                "service_location" => { "lat" => 37.924513, "lng" => -121.230777 }, # Stockton, CA
                "dropoff_location" => { "site_id" => fell_street_site.id },
              },
            },
          }
        end

        describe "Not expired Roadside" do
          let(:policy_coverage_return) do
            {
              async_request_id: async_request.id,
              input: {
                pcc_coverage_id: pcc_coverage.id,
                pcc_policy_id: policy.id,
                pcc_vehicle_id: pcc_vehicle.id,
                pcc_driver_id: pcc_driver.id,
                programs: [
                  {
                    code: program_code,
                    coverage_issues: ["Cost limit: $100"],
                    coverage_notes: coverage_notes,
                    priority: false,
                    failed_conditions: [],
                    limitations: ["Customer pays overage"],
                    name: program_name,
                    rank: 5.0,
                    result: "PartiallyCovered",
                    site_code: "12345T",
                    successful_conditions: [
                      "dropoff.type == \"Dealership\"",
                      "job.service == \"Tow\"",
                      "vehicle.odometer < program.odometer_limit",
                      "program.expired == false",
                    ],
                  },
                ],
              },
            }
          end

          it 'returns partially covered result' do
            expect(PCC::Coverage).to receive(:create!).and_return(pcc_coverage)
            expect(PCC::Publishers::Websocket).to receive(:call).with(policy_coverage_return)

            worker_perform
          end
        end
      end

      context 'Accident Tow to Collision Center' do
        let(:subject_args) do
          {
            "pcc_policy_id" => policy.id,
            "pcc_vehicle_id" => pcc_vehicle.id,
            "pcc_driver_id" => pcc_driver.id,
            "job" => {
              "service" => { "name" => ServiceCode::ACCIDENT_TOW },
              "vehicle" => {
                "year" => 1980,
                "odometer" => 55000,
              },
              "location" => {
                "service_location" => { "lat" => 37.724994, "lng" => -122.393401 }, # Bayview
                "dropoff_location" => { "site_id" => domi_autobody_site.id },
              },
            },
          }
        end

        describe "Not expired Roadside" do
          let(:policy_coverage_return) do
            {
              async_request_id: async_request.id,
              input: {
                pcc_coverage_id: pcc_coverage.id,
                pcc_policy_id: policy.id,
                pcc_vehicle_id: pcc_vehicle.id,
                pcc_driver_id: pcc_driver.id,
                programs: [
                  {
                    code: program_code,
                    coverage_issues: ["Cost limit: $100"],
                    coverage_notes: coverage_notes,
                    priority: false,
                    failed_conditions: ["job.service == \"Tow\""],
                    limitations: ["Customer pays overage"],
                    name: program_name,
                    rank: 5.0,
                    result: "PartiallyCovered",
                    site_code: "12345T",
                    successful_conditions: [
                      "dropoff.nearest == true",
                      "dropoff.type == \"Auto Body Shop\"",
                      "job.service == \"Accident Tow\"",
                      "vehicle.odometer < program.odometer_limit",
                      "program.expired == false",
                    ],
                  },
                ],
              },
            }
          end

          it 'returns partially covered result' do
            dump_table(Company)
            dump_table(Site)

            expect(PCC::Coverage).to receive(:create!).and_return(pcc_coverage)
            expect(PCC::Publishers::Websocket).to receive(:call).with(policy_coverage_return)

            worker_perform
          end
        end
      end
    end
  end
end
