# frozen_string_literal: true

require 'rails_helper'

describe CreateOrUpdateInvoice do
  let(:swoop_company) { create(:super_company) }

  context 'Partner To Swoop Invoice' do
    let(:partner_company) do
      create(:rescue_company, {
        name: 'Finish Line Towing',
      })
    end

    let(:partners_swoop_account) do
      create(
        :account,
        name: 'Swoop',
        company: partner_company,
        client_company: swoop_company
      )
    end

    let!(:job) do
      create(:rescue_job, {
        rescue_company: partner_company,
        invoice: nil,
        account: partners_swoop_account,
        service_code: ServiceCode.find_by(name: 'Tow'),
      })
    end

    let!(:flat_rate) do
      create(:clean_flat_rate, {
        company: partner_company,
        live: true,
      })
    end

    it 'create invoice' do
      CreateOrUpdateInvoice.new.perform(job.id)
      job.reload
      expect(job.invoice).not_to be_nil
      expect(job.invoice.line_items.length).to eql(2)
    end

    it "cancels invoices on a canceled job" do
      CreateOrUpdateInvoice.new.perform(job.id)
      job.reload
      job.update!(status: "Canceled")
      cancel_service = InvoiceService::Cancel::CancelPartnerInvoice.new(job)
      expect(InvoiceService::Cancel::CancelPartnerInvoice).to receive(:new).with(job).and_return(cancel_service)
      expect(cancel_service).to receive(:call).and_call_original
      CreateOrUpdateInvoice.new.perform(job.id)
    end
  end
end
