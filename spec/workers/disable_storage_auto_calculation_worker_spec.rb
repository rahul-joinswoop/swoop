# frozen_string_literal: true

require "rails_helper"

describe DisableStorageAutoCalculationWorker, freeze_time: true do
  subject { described_class.new.perform(Date.current.to_s) }

  # job1 has the correct invoicing item but is created too recently
  let!(:job1) { create :job, :stored_with_invoice }
  let!(:invoicing_item1) { InvoicingLineItem.find_by(job: job1) }

  # job2 has the correct invoicing item and is created within the right time range
  let!(:job2) { Timecop.freeze((Date.current - 90.days) - 1.minute) { create :job, :stored_with_invoice } }
  let!(:invoicing_item2) { InvoicingLineItem.find_by(job: job2) }

  # job3 is created within the right time range but has auto_calculated_quantity: false
  let!(:job3) { Timecop.freeze((Date.current - 90.days) - 1.minute) { create :job, :stored_with_invoice } }
  let!(:invoicing_item3) { InvoicingLineItem.find_by(job: job3).tap { |i| i.update! auto_calculated_quantity: false } }

  it 'works' do
    subject
    expect { invoicing_item1.reload }
      .not_to change { invoicing_item1 }

    expect { invoicing_item2.reload }
      .to change(invoicing_item2, :auto_calculated_quantity)
      .from(true)
      .to(false)

    expect { invoicing_item3.reload }
      .not_to change { invoicing_item3 }
  end
end
