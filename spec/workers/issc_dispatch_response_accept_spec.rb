# frozen_string_literal: true

require "rails_helper"

# this is actually a Sidekiq worker
describe IsscDispatchResponseAccept do
  subject(:worker_perform) do
    IsscDispatchResponseAccept.new(connection_manager: issc_connection_manager).perform(job_id)
  end

  let(:issc_connection_manager) { ISSC::ConnectionManager.new }

  let(:job) do
    create(
      :fleet_motor_club_job,
      status: job_status,
      partner_dispatcher: partner_dispatcher,
      bta: bta,
      issc_dispatch_request: issc_dispatch_request,
      fleet_company: fleet_company,
      eta_explanation: job_eta_explanation,
    )
  end
  let(:job_id) { job.id }
  let(:partner_dispatcher) { create(:user, full_name: 'A Dispatcher Full Name', phone: '1111111') }

  let(:time_of_arrival) { create_list(:time_of_arrival, 1) }
  let(:bta) do
    create(
      :time_of_arrival,
      time: bta_time,
      created_at: time_now,
      updated_at: time_now
    )
  end
  let(:job_eta_explanation) { nil }

  let(:time_now) { Time.now }

  let(:issc_dispatch_request) do
    create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123")
  end

  let(:provider_supports_location) { false }

  before do
    allow_any_instance_of(Issc).to receive(:publish_account)
    allow(Job).to receive(:find).with(job_id).and_return(job)
    allow(Issc).to receive(:provider_supports_location?).and_return(provider_supports_location)
  end

  context 'when ISSC connection is ready' do
    let(:issc_client_mock) { double(:issc_client_mock, dispatchresponse: nil) }

    before do
      allow(issc_connection_manager).to receive(:ensure_connected).and_yield(issc_client_mock)
    end

    context 'and job.fleet_company is Agero' do
      let(:fleet_company) { create(:fleet_company, :agero) }
      let(:issc) { create(:ago_issc) }

      # MAX ETA accepted by Agero w/o explanation is 90.minutes
      let(:bta_time) { time_now + 89.minutes }

      context 'and job.status is submitted' do
        let(:job_status) { Job::STATUS_SUBMITTED }

        it 'calls issc_client with expected params' do
          expect(issc_client_mock).to receive(:dispatchresponse).with(
            job,
            {
              DispatchID: "123123123",
              JobID: "ISSC_JOB_123",
              ServiceProviderResponse: "Accepted",
              Source: :Manual,
              ClientID: "AGO",
              ContractorID: nil,
              ProviderContactName: "A Dispatcher Full Name",
              ProviderPhoneNumber: "1111111",
              ETA: 89,
            }
          )

          worker_perform
        end

        context 'and ETA is greater than 90 minutes' do
          let(:bta_time) { time_now + 91.minutes }
          let(:job_eta_explanation) do
            create(:job_eta_explanation, text: explanation_text)
          end

          context 'and job_eta_explanation is None' do
            let(:explanation_text) { "None" }

            it 'calls issc_client with expected eta explanation mapped' do
              expect(issc_client_mock).to receive(:dispatchresponse).with(
                job,
                {
                  DispatchID: "123123123",
                  JobID: "ISSC_JOB_123",
                  ServiceProviderResponse: "Accepted",
                  Source: :Manual,
                  ClientID: "AGO",
                  ContractorID: nil,
                  ProviderContactName: "A Dispatcher Full Name",
                  ProviderPhoneNumber: "1111111",
                  ETA: 91,
                  ETAExplanation: "None",
                }
              )

              worker_perform
            end
          end

          context 'and job_eta_explanation is Traffic' do
            let(:explanation_text) { "Traffic" }

            it 'calls issc_client with expected eta explanation mapped' do
              expect(issc_client_mock).to receive(:dispatchresponse).with(
                job,
                {
                  DispatchID: "123123123",
                  JobID: "ISSC_JOB_123",
                  ServiceProviderResponse: "Accepted",
                  Source: :Manual,
                  ClientID: "AGO",
                  ContractorID: nil,
                  ProviderContactName: "A Dispatcher Full Name",
                  ProviderPhoneNumber: "1111111",
                  ETA: 91,
                  ETAExplanation: "Traffic",
                }
              )

              worker_perform
            end
          end

          context 'and job_eta_explanation is Weather' do
            let(:explanation_text) { "Weather" }

            it 'calls issc_client with expected eta explanation mapped' do
              expect(issc_client_mock).to receive(:dispatchresponse).with(
                job,
                {
                  DispatchID: "123123123",
                  JobID: "ISSC_JOB_123",
                  ServiceProviderResponse: "Accepted",
                  Source: :Manual,
                  ClientID: "AGO",
                  ContractorID: nil,
                  ProviderContactName: "A Dispatcher Full Name",
                  ProviderPhoneNumber: "1111111",
                  ETA: 91,
                  ETAExplanation: "Weather",
                }
              )

              worker_perform
            end
          end

          context 'and job_eta_explanation is nil' do
            let(:explanation_text) { nil }

            it 'calls issc_client with expected eta explanation mapped' do
              expect(issc_client_mock).to receive(:dispatchresponse).with(
                job,
                {
                  DispatchID: "123123123",
                  JobID: "ISSC_JOB_123",
                  ServiceProviderResponse: "Accepted",
                  Source: :Manual,
                  ClientID: "AGO",
                  ContractorID: nil,
                  ProviderContactName: "A Dispatcher Full Name",
                  ProviderPhoneNumber: "1111111",
                  ETA: 91,
                  ETAExplanation: "",
                }
              )

              worker_perform
            end
          end
        end
      end
    end

    # Quest
    context 'and job.fleet_company is Quest' do
      let(:fleet_company) { create(:fleet_company, :quest) }
      let(:issc) { create(:quest_issc) }

      let(:bta_time) { time_now + 89.minutes }

      context 'and job.status is submitted' do
        let(:job_status) { Job::STATUS_SUBMITTED }

        it 'calls issc_client with expected params' do
          expect(issc_client_mock).to receive(:dispatchresponse).with(
            job,
            {
              DispatchID: "123123123",
              JobID: "ISSC_JOB_123",
              ServiceProviderResponse: "Accepted",
              Source: :Manual,
              ClientID: "QUEST",
              ContractorID: "QUEST12345",
              ProviderContactName: "A Dispatcher Full Name",
              ProviderPhoneNumber: "1111111",
              ETA: 89,
            }
          )

          worker_perform
        end

        context 'and ETA is greater than 90 minutes' do
          let(:bta_time) { time_now + 91.minutes }
          let(:job_eta_explanation) do
            create(:job_eta_explanation, text: explanation_text)
          end

          context "and job_eta_explanation is Construction" do
            let(:explanation_text) { "Construction" }

            it 'calls issc_client with expected eta explanation mapped' do
              expect(issc_client_mock).to receive(:dispatchresponse).with(
                job,
                {
                  DispatchID: "123123123",
                  JobID: "ISSC_JOB_123",
                  ServiceProviderResponse: "Accepted",
                  Source: :Manual,
                  ClientID: "QUEST",
                  ContractorID: "QUEST12345",
                  ProviderContactName: "A Dispatcher Full Name",
                  ProviderPhoneNumber: "1111111",
                  ETA: 91,
                  ETAExplanation: "Construction",
                }
              )

              worker_perform
            end
          end

          context 'and job_eta_explanation is Other' do
            let(:explanation_text) { "Other" }

            it 'calls issc_client with expected eta explanation mapped' do
              expect(issc_client_mock).to receive(:dispatchresponse).with(
                job,
                {
                  DispatchID: "123123123",
                  JobID: "ISSC_JOB_123",
                  ServiceProviderResponse: "Accepted",
                  Source: :Manual,
                  ClientID: "QUEST",
                  ContractorID: "QUEST12345",
                  ProviderContactName: "A Dispatcher Full Name",
                  ProviderPhoneNumber: "1111111",
                  ETA: 91,
                  ETAExplanation: "Other",
                }
              )

              worker_perform
            end
          end

          context 'and job_eta_explanation is "Rural Disablement"' do
            let(:explanation_text) { "Rural Disablement" }

            it 'calls issc_client with expected eta explanation mapped' do
              expect(issc_client_mock).to receive(:dispatchresponse).with(
                job,
                {
                  DispatchID: "123123123",
                  JobID: "ISSC_JOB_123",
                  ServiceProviderResponse: "Accepted",
                  Source: :Manual,
                  ClientID: "QUEST",
                  ContractorID: "QUEST12345",
                  ProviderContactName: "A Dispatcher Full Name",
                  ProviderPhoneNumber: "1111111",
                  ETA: 91,
                  ETAExplanation: "RuralDisablement",
                }
              )

              worker_perform
            end
          end

          context 'and job_eta_explanation is "Service Provider Issue"' do
            let(:explanation_text) { "Service Provider Issue" }

            it 'calls issc_client with expected eta explanation mapped' do
              expect(issc_client_mock).to receive(:dispatchresponse).with(
                job,
                {
                  DispatchID: "123123123",
                  JobID: "ISSC_JOB_123",
                  ServiceProviderResponse: "Accepted",
                  Source: :Manual,
                  ClientID: "QUEST",
                  ContractorID: "QUEST12345",
                  ProviderContactName: "A Dispatcher Full Name",
                  ProviderPhoneNumber: "1111111",
                  ETA: 91,
                  ETAExplanation: "ServiceProviderIssue",
                }
              )

              worker_perform
            end
          end

          context 'and job_eta_explanation is Traffic' do
            let(:explanation_text) { "Traffic" }

            it 'calls issc_client with expected eta explanation mapped' do
              expect(issc_client_mock).to receive(:dispatchresponse).with(
                job,
                {
                  DispatchID: "123123123",
                  JobID: "ISSC_JOB_123",
                  ServiceProviderResponse: "Accepted",
                  Source: :Manual,
                  ClientID: "QUEST",
                  ContractorID: "QUEST12345",
                  ProviderContactName: "A Dispatcher Full Name",
                  ProviderPhoneNumber: "1111111",
                  ETA: 91,
                  ETAExplanation: "Traffic",
                }
              )

              worker_perform
            end
          end

          context 'and job_eta_explanation is Weather' do
            let(:explanation_text) { "Weather" }

            it 'calls issc_client with expected eta explanation mapped' do
              expect(issc_client_mock).to receive(:dispatchresponse).with(
                job,
                {
                  DispatchID: "123123123",
                  JobID: "ISSC_JOB_123",
                  ServiceProviderResponse: "Accepted",
                  Source: :Manual,
                  ClientID: "QUEST",
                  ContractorID: "QUEST12345",
                  ProviderContactName: "A Dispatcher Full Name",
                  ProviderPhoneNumber: "1111111",
                  ETA: 91,
                  ETAExplanation: "Weather",
                }
              )

              worker_perform
            end
          end

          context 'and job_eta_explanation is nil' do
            let(:explanation_text) { nil }

            it 'calls issc_client with expected eta explanation mapped' do
              expect(issc_client_mock).to receive(:dispatchresponse).with(
                job,
                {
                  DispatchID: "123123123",
                  JobID: "ISSC_JOB_123",
                  ServiceProviderResponse: "Accepted",
                  Source: :Manual,
                  ClientID: "QUEST",
                  ContractorID: "QUEST12345",
                  ProviderContactName: "A Dispatcher Full Name",
                  ProviderPhoneNumber: "1111111",
                  ETA: 91,
                  ETAExplanation: "",
                }
              )

              worker_perform
            end
          end
        end
      end
    end
  end
end
