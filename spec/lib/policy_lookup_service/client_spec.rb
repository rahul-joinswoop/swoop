# frozen_string_literal: true

require 'rails_helper'

describe PolicyLookupService::Client do
  let(:service_url) { Configuration.pls_api.url }
  let(:payload) { { "foo" => "bar" } }

  describe "get" do
    it "makes an authenticated for the specified resource for a client" do
      stub_request(:get, "#{service_url}/bar/foo").with(headers: {
        "Content-Type" => "application/json; charset=utf-8",
        "Authorization" => "Bearer #{Configuration.pls_api.access_token}",
      }).to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: payload.to_json)
      response = PolicyLookupService::Client.new("bar").get("/foo")
      expect(response).to eq payload
    end

    it "raises an error on an error response from the server" do
      stub_request(:get, "#{service_url}/bar/foo").to_return(status: 500, body: "server error")
      expect { PolicyLookupService::Client.new("bar").get("/foo") }.to raise_error(PolicyLookupService::Client::Error)
    end
  end

  describe "pii" do
    it "requests the PII URL and return the payload" do
      stub_request(:get, "http://example.com/pii/123").to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: payload.to_json)
      response = PolicyLookupService::Client.new("bar").pii("http://example.com/pii/123")
      expect(response).to eq payload
    end
  end

  describe "search" do
    it "queries for vehicle policies by vin" do
      stub_request(:get, "#{service_url}/bar/vehicles").with(query: { vin: "12345678" }, headers: {
        "Content-Type" => "application/json; charset=utf-8",
        "Authorization" => "Bearer #{Configuration.pls_api.access_token}",
      }).to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: payload.to_json)
      response = PolicyLookupService::Client.new("bar").search(vin: "12345678")
      expect(response).to eq payload
    end

    it "queries for vehicle policies by license_number" do
      stub_request(:get, "#{service_url}/bar/vehicles").with(query: { license_number: "LX8890" }, headers: {
        "Content-Type" => "application/json; charset=utf-8",
        "Authorization" => "Bearer #{Configuration.pls_api.access_token}",
      }).to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: payload.to_json)
      response = PolicyLookupService::Client.new("bar").search(license_number: "LX8890")
      expect(response).to eq payload
    end

    it "queries of customer policies by last_name and postal_code" do
      stub_request(:get, "#{service_url}/bar/customers").with(query: { last_name: "Doe", postal_code: "60304" }, headers: {
        "Content-Type" => "application/json; charset=utf-8",
        "Authorization" => "Bearer #{Configuration.pls_api.access_token}",
      }).to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: payload.to_json)
      response = PolicyLookupService::Client.new("bar").search(last_name: "Doe", postal_code: "60304")
      expect(response).to eq payload
    end

    it "queries of customer policies by phone_number and country" do
      stub_request(:get, "#{service_url}/bar/customers").with(query: { phone_number: "4145551212", country: "US" }, headers: {
        "Content-Type" => "application/json; charset=utf-8",
        "Authorization" => "Bearer #{Configuration.pls_api.access_token}",
      }).to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: payload.to_json)
      response = PolicyLookupService::Client.new("bar").search(phone_number: "4145551212", country: "US")
      expect(response).to eq payload
    end

    it "queries of customer policies by policy_number" do
      stub_request(:get, "#{service_url}/bar/policies").with(query: { policy_number: "12345678" }, headers: {
        "Content-Type" => "application/json; charset=utf-8",
        "Authorization" => "Bearer #{Configuration.pls_api.access_token}",
      }).to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: payload.to_json)

      response = PolicyLookupService::Client.new("bar").search(policy_number: "12345678")

      expect(response).to eq payload
    end
  end

  describe "policy" do
    it "queries of customer policies by phone_number and country" do
      stub_request(:get, "#{service_url}/bar/policies/uniqueid").with(headers: {
        "Content-Type" => "application/json; charset=utf-8",
        "Authorization" => "Bearer #{Configuration.pls_api.access_token}",
      }).to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: payload.to_json)
      response = PolicyLookupService::Client.new("bar").policy("uniqueid")
      expect(response).to eq payload
    end
  end

  describe "vehicle" do
    it "queries of customer policies by phone_number and country" do
      stub_request(:get, "#{service_url}/bar/vehicles/uniqueid").with(headers: {
        "Content-Type" => "application/json; charset=utf-8",
        "Authorization" => "Bearer #{Configuration.pls_api.access_token}",
      }).to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: payload.to_json)
      response = PolicyLookupService::Client.new("bar").vehicle("uniqueid")
      expect(response).to eq payload
    end
  end

  describe "customer" do
    it "queries of customer policies by phone_number and country" do
      stub_request(:get, "#{service_url}/bar/customers/uniqueid").with(headers: {
        "Content-Type" => "application/json; charset=utf-8",
        "Authorization" => "Bearer #{Configuration.pls_api.access_token}",
      }).to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: payload.to_json)
      response = PolicyLookupService::Client.new("bar").customer("uniqueid")
      expect(response).to eq payload
    end
  end

  describe "processing_statuses" do
    it "returns the latest processing statuses" do
      payload = { processing_statuses: ["foo"] }
      stub_request(:get, "#{service_url}/bar/processing_statuses.json").with(headers: {
        "Content-Type" => "application/json; charset=utf-8",
        "Authorization" => "Bearer #{Configuration.pls_api.access_token}",
      }).to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: payload.to_json)
      response = PolicyLookupService::Client.new("bar").processing_statuses
      expect(response).to eq ["foo"]
    end
  end

  describe "client config overrides" do
    it "uses client specific configuration values" do
      stub_request(:get, "https://pls.example.com/tester/foo").with(headers: {
        "Content-Type" => "application/json; charset=utf-8",
        "Authorization" => "Bearer foobar",
      }).to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: payload.to_json)
      response = PolicyLookupService::Client.new("tester").get("/foo")
      expect(response).to eq payload
    end
  end
end
