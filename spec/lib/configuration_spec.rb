# frozen_string_literal: true

require 'rails_helper'

describe Configuration do
  # This uses the s3 config file to test the functionality of our config class
  describe '.s3' do
    it "can load a Hash from a YAML file for a specified environment" do
      expect(Configuration.load_config("s3.yml", "development")["secret_key"]).to eq "stubbed"
    end

    it "loads and returns singleton objects" do
      expect(Configuration.s3.object_id).to eq Configuration.s3.object_id
    end

    it "is backed by a frozen hash with indifferent access" do
      expect(Configuration.s3.config).to be_a(HashWithIndifferentAccess)
      expect(Configuration.s3.config).to be_frozen
    end

    it "works for regular values" do
      expect(Configuration.s3.secret_key).to eq "stubbed"
    end

    it "works for interpolated values" do
      expect(Configuration.s3.endpoint).to eq(ENV['S3_ENDPOINT'] || 'http://localhost:4567')
    end

    it "returns nil for undefined values" do
      expect(Configuration.s3.fluffy_bunnies).to eq nil
    end

    it "can use the hash syntax" do
      expect(Configuration[:s3].secret_key).to eq "stubbed"
      expect(Configuration.s3[:secret_key]).to eq "stubbed"
      expect(Configuration.s3["secret_key"]).to eq "stubbed"
    end

    it "returns the names of loaded configurations" do
      expect(Configuration.names).to include("s3")
    end

    it "returns the configuration hash" do
      expect(Configuration[:s3].to_h).to eq Configuration[:s3].config
      expect(Configuration[:s3].to_hash).to eq Configuration[:s3].config
    end
  end
end
