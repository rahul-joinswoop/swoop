# frozen_string_literal: true

require "rails_helper"

RSpec.describe Swoop::SwoopNativeBrowser do
  let(:ios) { Browser.new("ios  Swoop/1.15.33.265 (086e96f7d)").platform }
  let(:android) { Browser.new("android  Swoop/41.292 (ce09a0d8b)").platform }
  let(:other) { Browser.new("kinda broken Swoop thing").platform }
  let(:not_swoop) { Browser.new("Mozilla/5.0 (Linux; Android 6.0.1; SM-G920V Build/MMB29K)").platform }

  context "browser monkey patch" do
    it "identifies swoop urls" do
      expect(ios.ios?).to be true
      expect(ios.subject.swoop?).to be true
    end

    it "ignores non swoop" do
      expect(not_swoop).not_to respond_to(:swoop?)
    end

    it "falls back to other types that match" do
      expect(other.id).to eq(:other)
    end
  end

  describe "#version" do
    it "grabs the version" do
      expect(android.version).to eq("41.292")
      expect(ios.version).to eq("1.15.33.265")
    end

    it "fails gracefully on the version not there" do
      expect(other.version).to be_nil
    end
  end

  describe "#build" do
    it "grabs the build" do
      expect(android.subject.build).to eq("ce09a0d8b")
      expect(ios.subject.build).to eq("086e96f7d")
    end

    it "fails gracefully on the build not there" do
      expect(other.subject.build).to be_nil
    end
  end

  describe "#name" do
    it "attaches the native name" do
      expect(android.name).to eq("android (native)")
      expect(ios.name).to eq("ios (native)")
    end
  end
end
