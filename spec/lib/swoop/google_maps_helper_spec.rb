# frozen_string_literal: true

require "rails_helper"

RSpec.describe Swoop::GoogleMapsHelper do
  describe ".query_url" do
    subject { Swoop::GoogleMapsHelper.query_url('service', {}) }

    let(:expected) do
      "https://maps.googleapis.com/maps/api/service/json?channel=dev&client=#{ENV['GOOGLE_PREMIUM_CLIENT_ID']}&signature="
    end

    it { is_expected.to include(expected) }
  end
end
