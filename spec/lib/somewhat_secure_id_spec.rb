# frozen_string_literal: true

require "rails_helper"

describe SomewhatSecureID do
  let(:model) { create :user }
  let(:encoded) { SomewhatSecureID.encode(model) }
  let(:non_base62_encoded) { Faker::Hipster.sentence }
  let(:bad_encoded) { SecureRandom.hex.to_i(16).base62_encode }
  let(:attributes) { model.attributes.find { |k, v| k != "id" && v.present? } }
  let(:key) { attributes[0].to_sym }
  let(:value) { attributes[1] }

  describe "#encode" do
    subject { encoded }

    it "works" do
      expect { subject }.not_to raise_error
      expect(subject).to be_a(String)
    end
  end

  describe "#decode" do
    subject { SomewhatSecureID.decode(encoded) }

    shared_examples "decode" do
      it "works" do
        expect { subject }.not_to raise_error
        (klass, id) = subject
        expect(model).to be_a(klass.constantize)
        expect(model.id).to eq(id.to_i)
      end
    end

    it_behaves_like "decode"

    context "with an odd blocksize" do
      # this is a magic number - for some reason this combination ends up
      # 31 bytes long instead of 32 when encrypted, so we use it as a test case
      let(:model) { User.new(id: 6) }

      it_behaves_like "decode"
    end
  end

  context "with multiple models" do
    let(:encoded_multiple) { SomewhatSecureID.encode(model, another_model) }
    let(:another_model) { create :user }
    let(:decoded) { SomewhatSecureID.decode(encoded_multiple) }

    it "works" do
      expect { encoded_multiple }.not_to raise_error
      expect { decoded }.not_to raise_error
      (klass, id) = decoded
      expect(model).to be_a(klass.constantize)
      expect(model.id).to eq(id.to_i)
    end
  end

  describe "#load" do
    subject { SomewhatSecureID.load(encoded) }

    it "works" do
      expect { subject }.not_to raise_error
      expect(subject).to eq(model)
    end

    # non-bang method should not raise error but it should return nil
    context "with an non-base62 encoded id" do
      let(:encoded) { non_base62_encoded }

      it "works" do
        expect { subject }.not_to raise_error
        expect(subject).to be_nil
      end
    end

    context "with an invalid id" do
      let(:encoded) { bad_encoded }

      it "works" do
        expect { subject }.not_to raise_error
        expect(subject).to be_nil
      end
    end

    context "with a valid search clause" do
      subject do
        search = {}
        search[key] = value
        SomewhatSecureID.load(encoded, search)
      end

      it "works " do
        expect { subject }.not_to raise_error
        expect(subject).to eq(model)
      end
    end

    context "with an invalid search clause" do
      subject do
        search = {}
        search[key] = nil
        SomewhatSecureID.load(encoded, search)
      end

      it "works " do
        expect { subject }.not_to raise_error
        expect(subject).to be_blank
      end
    end
  end

  describe "#load!" do
    subject { SomewhatSecureID.load!(encoded) }

    it "works" do
      expect { subject }.not_to raise_error
      expect(subject).to eq(model)
    end

    # bang method should raise errors
    context "with an non-base62 encoded id" do
      let(:encoded) { non_base62_encoded }

      it "works" do
        expect { subject }.to raise_error(SomewhatSecureID::Error)
      end
    end

    context "with an invalid id" do
      let(:encoded) { bad_encoded }

      it "works" do
        expect { subject }.to raise_error(SomewhatSecureID::Error)
      end
    end

    context "with a valid search clause" do
      subject do
        search = {}
        search[key] = value
        SomewhatSecureID.load!(encoded, search)
      end

      it "works " do
        expect { subject }.not_to raise_error
        expect(subject).to eq(model)
      end
    end

    context "with an invalid search clause" do
      subject do
        search = {}
        search[key] = nil
        SomewhatSecureID.load!(encoded, search)
      end

      it "works " do
        expect { subject }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe "#to_ssid" do
    subject { model.to_ssid }

    it "works" do
      expect { subject }.not_to raise_error
      expect(subject).to eq(encoded)
    end
  end
end
