# frozen_string_literal: true

require 'rails_helper'
require_relative '../services/external/google_directions_service_shared'
describe TruckSimulator do
  include_context 'google directions service shared'
  describe '#simulate' do
    subject do
      TruckSimulator.simulate truck: truck, origin: origin, destination: destination, waypoints: waypoints, duration: duration
    end

    let(:truck) { create :rescue_vehicle }
    let(:duration) { nil }
    let(:uuid) { SecureRandom.uuid }
    let(:directions_id) { "TruckSimulator:#{uuid}" }
    let(:redis) { RedisClient[:default] }

    # have to do all of these before we process our response in TruckSimulator since we
    # mutate it there
    let!(:total_time) do
      duration || response.dig('routes', 0, 'legs').reduce(0.0) { |memo, cur| memo + cur.dig('duration', 'value').to_f }
    end
    let!(:start_location) { response.dig('routes', 0, 'legs', 0, 'start_location') }
    let!(:end_location) { response.dig('routes', 0, 'legs').last['end_location'] }

    shared_examples 'it works' do
      it 'works' do
        expect(External::GoogleDirectionsService)
          .to receive(:get)
          .and_return(response)

        expect(SecureRandom)
          .to receive(:uuid)
          .once
          .and_return(uuid)

        # we should enqueue one worker
        expect(TruckSimulatorWorker)
          .to receive(:perform_async)
          .once
          .with(truck.id, directions_id)

        # our truck should have its location updated to the beginning point from
        # our directions
        expect(UpdateVehicleLocationWorker)
          .to receive(:perform_async)
          .once
          .with(truck.company_id, truck.id, start_location['lat'], start_location['lng'], anything)

        subject

        # our directions will be executed every TruckSimulator::UPDATE_FREQUENCY_SECONDS seconds
        # so the total amount of time our list takes shouldn't be any longer than the actual time
        # we get from google (or are provided as a duration)
        expect(redis.llen(directions_id) * TruckSimulator::UPDATE_FREQUENCY_SECONDS).to be <= total_time

        # our final point should be within 1m of our requested destination
        delta_m = Geocoder::Calculations.distance_between(
          MultiJson.load(redis.lindex(directions_id, -1)),
          [end_location['lat'], end_location['lng']],
          units: :km
        ) / 1000
        expect(delta_m).to be < 1
      end
    end

    it_behaves_like 'it works'

    context 'with duration set' do
      let(:duration) { 300 }

      it_behaves_like "it works"
    end
  end
end
