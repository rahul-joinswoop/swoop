# frozen_string_literal: true

require "rails_helper"

describe LegacyBase62Encryptor do
  let(:key) { SecureRandom.hex(32) }
  let(:iv) { SecureRandom.hex(16) }
  let(:separator) { "^" }
  let(:single_value) { Faker::Hipster.sentence }
  let(:multiple_values) { Faker::Lorem.words }
  let(:value_with_separator) { single_value + subject.send(:separator) }

  describe "#initialize" do
    shared_examples "encrypts and decrypts" do
      describe "a single value" do
        it "works" do
          encrypted = subject.encrypt(single_value)
          expect(encrypted).not_to eq(single_value)
          (decrypted,) = subject.decrypt(encrypted)
          expect(decrypted).to eq(single_value)
        end
        it "raise with a value including the separator" do
          expect(Rollbar).to receive(:error).once
          expect { subject.encrypt(value_with_separator) }
            .to raise_error(LegacyBase62Encryptor::Error)
        end
      end

      describe "a single value in an array" do
        it "works" do
          encrypted = subject.encrypt([single_value])
          expect(encrypted).not_to eq([single_value])
          (decrypted,) = subject.decrypt(encrypted)
          expect(decrypted).to eq(single_value)
        end
      end

      describe "multiple values in an array" do
        it "works" do
          encrypted = subject.encrypt(multiple_values)
          expect(encrypted).not_to eq(multiple_values)
          decrypted = subject.decrypt(encrypted)
          expect(decrypted).to eq(multiple_values)
        end
        it "raise with a value including the separator" do
          expect(Rollbar).to receive(:error).once
          expect { subject.encrypt(multiple_values + [value_with_separator]) }
            .to raise_error(LegacyBase62Encryptor::Error)
        end
      end

      describe "multiple values as args" do
        it "works" do
          encrypted = subject.encrypt(*multiple_values)
          expect(encrypted).not_to eq(multiple_values)
          decrypted = subject.decrypt(encrypted)
          expect(decrypted).to eq(multiple_values)
        end
      end
    end

    context "with opts" do
      subject { LegacyBase62Encryptor.new(opts) }

      let(:opts) { { key: key, iv: iv, separator: separator } }

      it "uses defaults" do
        expect(subject.key).to eq([key].pack("H*"))
        expect(subject.iv).to eq([iv].pack("H*"))
        expect(subject.separator).to eq(separator)
      end
      it_behaves_like "encrypts and decrypts"
    end
  end
end
