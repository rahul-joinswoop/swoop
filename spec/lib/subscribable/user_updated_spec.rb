# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Subscribable::UserUpdated do
  context "with a single user" do
    context "that's not deleted" do
      let!(:user) { create :user }

      it "works" do
        expect(Rollbar).not_to receive(:error)
        expect(GraphQL::UserUpdatedWorker).to receive(:perform_async).with(user.id).once
        Subscribable.trigger_user_updated user
      end
    end

    context "that is deleted" do
      let!(:user) { create :user, deleted_at: Time.current }

      it "works" do
        expect(Rollbar).not_to receive(:error)
        expect(GraphQL::UserUpdatedWorker).not_to receive(:perform_async)
        Subscribable.trigger_user_updated user
      end
    end

    context "with a non-user but present? value" do
      let!(:user) { { a: 1 } }

      it "works" do
        expect(Rollbar).to receive(:error).with(Subscribable::UserUpdated::ERROR, arg: user)
        expect(GraphQL::UserUpdatedWorker).not_to receive(:perform_async)
        Subscribable.trigger_user_updated user
      end
    end

    context "with a blank? value" do
      let!(:user) { nil }

      it "works" do
        expect(Rollbar).not_to receive(:error)
        expect(GraphQL::UserUpdatedWorker).not_to receive(:perform_async)
        Subscribable.trigger_user_updated user
      end
    end
  end

  context "with a relation" do
    context "with non-deleted users" do
      let!(:users) { create_list :user, 2 }

      it "works" do
        expect(Rollbar).not_to receive(:error)
        expect(GraphQL::UserUpdatedWorker).to receive(:perform_async).with(*users.map(&:id)).once
        Subscribable.trigger_user_updated User.all
      end
    end

    context "with a deleted user" do
      let!(:users) { create_list :user, 2 }
      let!(:user) { create :user, deleted_at: Time.current }

      it "works" do
        expect(Rollbar).not_to receive(:error)
        expect(GraphQL::UserUpdatedWorker).to receive(:perform_async).with(*users.map(&:id)).once
        Subscribable.trigger_user_updated User.all
      end
    end
  end
end
