# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Subscribable::AuctionBidStatusChanged do
  context "without a status change" do
    let!(:bid) do
      b = create(:bid)
      b.save!
      b
    end

    it "works" do
      expect(Rollbar).not_to receive(:error)
      expect(GraphQL::AuctionBidStatusChangedWorker).not_to receive(:perform_async)
      Subscribable.trigger_auction_bid_status_changed bid
    end
  end

  context "on creation" do
    let!(:bid) { create :bid }

    it "works" do
      expect(Rollbar).not_to receive(:error)
      expect(GraphQL::AuctionBidStatusChangedWorker)
        .to receive(:perform_async)
        .with(bid.id, status: [nil, Auction::Bid::REQUESTED])

      Subscribable.trigger_auction_bid_status_changed bid
    end
  end

  context "with a status change" do
    let!(:bid) do
      b = create :bid
      b.update! status: Auction::Bid::SUBMITTED
      b
    end

    it "works" do
      expect(Rollbar).not_to receive(:error)
      expect(GraphQL::AuctionBidStatusChangedWorker)
        .to receive(:perform_async)
        .with(bid.id, status: [Auction::Bid::REQUESTED, Auction::Bid::SUBMITTED])

      Subscribable.trigger_auction_bid_status_changed bid
    end
  end

  context "with a non-bid but present? value" do
    let!(:bid) { { a: 1 } }

    it "works" do
      expect(Rollbar).to receive(:error).with(Subscribable::AuctionBidStatusChanged::ERROR, arg: bid)
      expect(GraphQL::AuctionBidStatusChangedWorker).not_to receive(:perform_async)
      Subscribable.trigger_auction_bid_status_changed bid
    end
  end

  context "with a blank? value" do
    let!(:bid) { nil }

    it "works" do
      expect(Rollbar).not_to receive(:error)
      expect(GraphQL::AuctionBidStatusChangedWorker).not_to receive(:perform_async)
      Subscribable.trigger_auction_bid_status_changed bid
    end
  end
end
