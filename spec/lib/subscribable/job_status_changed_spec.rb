# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Subscribable::JobStatusChanged do
  context "with a job" do
    shared_examples "it works" do
      it "calls trigger_job_status_changed" do
        expect(Rollbar).not_to receive(:error)
        expect(GraphQL::JobStatusChangedWorker).to receive(:perform_async)
          .with(job.id, delta)
          .once
        Subscribable.trigger_job_status_changed job
      end
    end

    context "with a status change" do
      let(:job) do
        job = create :job, status: :assigned
        job.enroute!
        job
      end
      let(:delta) do
        {
          status: [job.status_before_last_save, job.status],
        }
      end

      it_behaves_like "it works"

      context "with a rescue_driver change" do
        let(:rescue_company) { create :rescue_company }
        let(:rescue_driver) { create :user, :rescue, :driver, company: rescue_company }
        let(:new_rescue_driver) { create :user, :rescue, :driver, company: rescue_company }
        let(:job) do
          job = create :rescue_job, status: :assigned, rescue_company: rescue_company, rescue_driver: rescue_driver
          job.update! rescue_driver: new_rescue_driver
          job
        end
        let(:delta) { { rescue_driver_id: [rescue_driver.id, new_rescue_driver.id] } }

        it_behaves_like "it works"
      end

      context "with a rescue_company change" do
        let(:rescue_company) { create :rescue_company }
        let(:job) do
          job = create :rescue_job, status: :draft, rescue_company_id: nil
          job.update! rescue_company: rescue_company
          job
        end
        let(:delta) { { rescue_company_id: [nil, rescue_company.id] } }

        it_behaves_like "it works"
      end

      context "with all three" do
        let(:job) { create :rescue_job, :with_rescue_driver, status: :assigned }
        let(:delta) do
          {
            rescue_company_id: [nil, job.rescue_company_id],
            rescue_driver_id: [nil, job.rescue_driver_id],
            status: [nil, 'assigned'],
          }
        end

        it_behaves_like "it works"
      end
    end

    context "with a non-job but present? value" do
      let!(:job) { { a: 1 } }

      it "works" do
        expect(Rollbar).to receive(:error).with(Subscribable::JobStatusChanged::ERROR, arg: job)
        expect(GraphQL::JobStatusChangedWorker).not_to receive(:perform_async)
        Subscribable.trigger_job_status_changed job
      end
    end

    context "with a blank? value" do
      let!(:job) { nil }

      it "works" do
        expect(Rollbar).not_to receive(:error)
        expect(GraphQL::JobStatusChangedWorker).not_to receive(:perform_async)
        Subscribable.trigger_job_status_changed job
      end
    end
  end
end
