# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Subscribable::PartnerVehicleChanged do
  shared_examples "it works" do
    it "calls trigger_partner_vehicle_changed" do
      expect(Rollbar).not_to receive(:error)
      expect(GraphQL::PartnerVehicleChangedWorker)
        .to receive(:perform_async)
        .with(vehicle.id)
        .once
      Subscribable.trigger_partner_vehicle_changed vehicle
    end
  end

  context "with a partner vehicle" do
    let(:vehicle) { create :rescue_vehicle }

    it_behaves_like "it works"
  end
end
