# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Subscribable::JobUpdated do
  context "with a single job" do
    context "and an active status" do
      shared_examples "it works" do
        it "calls trigger_job_updated" do
          expect(Rollbar).not_to receive(:error)
          expect(GraphQL::JobUpdatedWorker)
            .to receive(:perform_async)
            .with({ ids: [job.id], delta: delta }.deep_stringify_keys)
            .once
          Subscribable.trigger_job_updated job
        end
      end

      context "with a rescue_company" do
        let(:job) { create :rescue_job, status: :assigned }
        let(:delta) { { rescue_company_id: [nil, job.rescue_company_id] } }

        it_behaves_like "it works"

        context "and a rescue_driver" do
          let(:job) { create :rescue_job, :with_rescue_driver, status: :assigned }
          let(:delta) do
            {
              rescue_company_id: [nil, job.rescue_company_id],
              rescue_driver_id: [nil, job.rescue_driver_id],
            }
          end

          it_behaves_like "it works"
        end
      end
    end

    context "and an inactive status" do
      let!(:job) { create :job, status: :predraft }

      it "works" do
        expect(Rollbar).not_to receive(:error)
        expect(GraphQL::JobUpdatedWorker).not_to receive(:perform_async)
        Subscribable.trigger_job_updated job
      end
    end

    context "with a non-job but present? value" do
      let!(:job) { { a: 1 } }

      it "works" do
        expect(Rollbar).to receive(:error).with(Subscribable::JobUpdated::ERROR, arg: job)
        expect(GraphQL::JobUpdatedWorker).not_to receive(:perform_async)
        Subscribable.trigger_job_updated job
      end
    end

    context "with a blank? value" do
      let!(:job) { nil }

      it "works" do
        expect(Rollbar).not_to receive(:error)
        expect(GraphQL::JobUpdatedWorker).not_to receive(:perform_async)
        Subscribable.trigger_job_updated job
      end
    end
  end

  context "with a relation" do
    let!(:jobs) { create_list :job, 2, status: :assigned }
    let(:job_ids) { jobs.sort_by(&:updated_at).reverse.map(&:id) }

    context "with active jobs" do
      it "works" do
        expect(Rollbar).not_to receive(:error)
        expect(GraphQL::JobUpdatedWorker)
          .to receive(:perform_async)
          .with({ ids: job_ids, delta: {} }.deep_stringify_keys)
          .once
        Subscribable.trigger_job_updated Job.all
      end
    end

    context "with inactive jobs" do
      let!(:job) { create :job, status: :predraft }

      it "works" do
        expect(Rollbar).not_to receive(:error)
        expect(GraphQL::JobUpdatedWorker)
          .to receive(:perform_async)
          .with({ ids: job_ids, delta: {} }.deep_stringify_keys)
          .once
        Subscribable.trigger_job_updated Job.all
      end
    end
  end
end
