# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::Client do
  let(:client) { Agero::Rsc::Client.new(access_token: "mytoken") }
  let!(:company) { create(:company, accounting_email: "testemail@joinswoop.com") }
  let!(:token) { create(:api_access_token, company: company, access_token: "mytoken") }

  describe "url" do
    it "creates endpoint URLs by combining the base_url with a path" do
      expect(client.url("/test")).to eq(client.url("/test"))
      expect(client.url("test/stuff?foo=bar")).to eq("https://agero.example.com/v1/test/stuff?foo=bar")
    end
  end

  describe "get" do
    describe "without raising errors" do
      it "sends a GET request with default headers and query params to a specified URL" do
        stub_request(:get, client.url("/test"))
          .with(query: { "test" => "value" })
          .to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: '{"foo": "bar"}')

        response = client.get("/test", params: { test: "value" })
        expect(response.code).to eq 200
        expect(response.success?).to eq true
        expect(response.content_type).to eq "application/json"
        expect(response.body).to eq '{"foo": "bar"}'
        expect(response.payload).to eq("foo" => "bar")
      end

      it "sends a GET request with a global timeout" do
        stub_request(:get, client.url("/test"))
          .with(query: { "test" => "value" })
          .to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: '{"foo": "bar"}')

        response = client.get("/test", params: { test: "value" }, timeout: 5)
        expect(response.code).to eq 200
      end

      it "sends a GET request with fine grained timeouts" do
        stub_request(:get, client.url("/test"))
          .with(query: { "test" => "value" })
          .to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: '{"foo": "bar"}')

        response = client.get("/test", params: { test: "value" }, timeout: { connect: 2, read: 2, write: 5 })
        expect(response.code).to eq 200
      end

      it "adds an AuditIssc" do
        stub_request(:get, client.url("/test"))
          .with(query: { "test" => "value" })
          .to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: '{"foo": "bar"}')

        expect { client.get("/test", params: { test: "value" }) }.to change(AuditIssc, :count).by(1)
      end

      it "returns an error response" do
        stub_request(:get, client.url("/test"))
          .to_return(status: 500, headers: { "Content-Type" => "application/json" }, body: '{"error": "boom"}')

        response = client.get("/test")
        expect(response.code).to eq 500
        expect(response.server_error?).to eq true
        expect(response.content_type).to eq "application/json"
        expect(response.body).to eq '{"error": "boom"}'
        expect(response.payload).to eq("error" => "boom")
      end

      it "adds an AuditIssc when error" do
        stub_request(:get, client.url("/test"))
          .to_return(status: 500, headers: { "Content-Type" => "application/json" }, body: '{"error": "boom"}')

        expect { client.get("/test") }.to change(AuditIssc, :count).by(1)
      end
    end

    describe "with raising errors" do
      it "sends a GET request with default headers and query params to a specified URL" do
        stub_request(:get, client.url("/test"))
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: '{"foo": "bar"}')

        response = client.get!("/test")
        expect(response.payload).to eq("foo" => "bar")
      end

      it "raises an error" do
        stub_request(:get, client.url("/test"))
          .to_return(status: 500, headers: { "Content-Type" => "application/json" }, body: '{"error": "boom"}')

        error = nil
        begin
          client.get!("/test")
        rescue Agero::Rsc::Error => e
          error = e
        end

        expect(error.response.payload).to eq("error" => "boom")
      end
    end
  end

  describe "post" do
    describe "without raising errors" do
      it "sends a GET request with default headers and query params to a specified URL" do
        stub_request(:post, client.url("/test"))
          .with(body: MultiJson.dump("test" => "value"), headers: { "Content-Type" => "application/json; charset=utf-8" })
          .to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: '{"foo": "bar"}')

        response = client.post("/test", params: { test: "value" })
        expect(response.code).to eq 200
        expect(response.success?).to eq true
        expect(response.content_type).to eq "application/json"
        expect(response.body).to eq '{"foo": "bar"}'
        expect(response.payload).to eq("foo" => "bar")
      end

      it "returns an error response" do
        stub_request(:post, client.url("/test"))
          .to_return(status: 500, headers: { "Content-Type" => "application/json" }, body: '{"error": "boom"}')

        response = client.post("/test")
        expect(response.code).to eq 500
        expect(response.server_error?).to eq true
        expect(response.content_type).to eq "application/json"
        expect(response.body).to eq '{"error": "boom"}'
        expect(response.payload).to eq("error" => "boom")
      end
    end

    describe "with raising errors" do
      it "sends a GET request with default headers and query params to a specified URL" do
        stub_request(:post, client.url("/test"))
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: '{"foo": "bar"}')

        response = client.post!("/test")
        expect(response.payload).to eq("foo" => "bar")
      end

      it "raises an error" do
        stub_request(:post, client.url("/test"))
          .to_return(status: 500, headers: { "Content-Type" => "application/json" }, body: '{"error": "boom"}')

        error = nil
        begin
          client.post!("/test")
        rescue Agero::Rsc::Error => e
          error = e
        end

        expect(error.response.payload).to eq("error" => "boom")
      end
    end
  end

  describe "put" do
    describe "without raising errors" do
      it "sends a GET request with default headers and query params to a specified URL" do
        stub_request(:put, client.url("/test"))
          .with(body: MultiJson.dump("test" => "value"), headers: { "Content-Type" => "application/json; charset=utf-8" })
          .to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: '{"foo": "bar"}')

        response = client.put("/test", params: { test: "value" })
        expect(response.code).to eq 200
        expect(response.success?).to eq true
        expect(response.content_type).to eq "application/json"
        expect(response.body).to eq '{"foo": "bar"}'
        expect(response.payload).to eq("foo" => "bar")
      end

      it "returns an error response" do
        stub_request(:put, client.url("/test"))
          .to_return(status: 500, headers: { "Content-Type" => "application/json" }, body: '{"error": "boom"}')

        response = client.put("/test")
        expect(response.code).to eq 500
        expect(response.server_error?).to eq true
        expect(response.content_type).to eq "application/json"
        expect(response.body).to eq '{"error": "boom"}'
        expect(response.payload).to eq("error" => "boom")
      end
    end

    describe "with raising errors" do
      it "sends a GET request with default headers and query params to a specified URL" do
        stub_request(:put, client.url("/test"))
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: '{"foo": "bar"}')

        response = client.put!("/test")
        expect(response.payload).to eq("foo" => "bar")
      end

      it "raises an error" do
        stub_request(:put, client.url("/test"))
          .to_return(status: 500, headers: { "Content-Type" => "application/json" }, body: '{"error": "boom"}')

        error = nil
        begin
          client.put!("/test")
        rescue Agero::Rsc::Error => e
          error = e
        end

        expect(error.response.payload).to eq("error" => "boom")
      end
    end
  end

  describe "delete" do
    describe "without raising errors" do
      it "sends a delete request with default headers and query params to a specified URL" do
        stub_request(:delete, client.url("/test"))
          .with(query: { "test" => "value" })
          .to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: '{"foo": "bar"}')

        response = client.delete("/test", params: { test: "value" })
        expect(response.code).to eq 200
        expect(response.success?).to eq true
        expect(response.content_type).to eq "application/json"
        expect(response.body).to eq '{"foo": "bar"}'
        expect(response.payload).to eq("foo" => "bar")
      end

      it "returns an error response" do
        stub_request(:delete, client.url("/test"))
          .to_return(status: 500, headers: { "Content-Type" => "application/json" }, body: '{"error": "boom"}')

        response = client.delete("/test")
        expect(response.code).to eq 500
        expect(response.server_error?).to eq true
        expect(response.content_type).to eq "application/json"
        expect(response.body).to eq '{"error": "boom"}'
        expect(response.payload).to eq("error" => "boom")
      end
    end

    describe "with raising errors" do
      it "sends a delete request with default headers and query params to a specified URL" do
        stub_request(:delete, client.url("/test"))
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: '{"foo": "bar"}')

        response = client.delete!("/test")
        expect(response.payload).to eq("foo" => "bar")
      end

      it "raises an error" do
        stub_request(:delete, client.url("/test"))
          .to_return(status: 500, headers: { "Content-Type" => "application/json" }, body: '{"error": "boom"}')

        error = nil
        begin
          client.delete!("/test")
        rescue Agero::Rsc::Error => e
          error = e
        end

        expect(error.response.payload).to eq("error" => "boom")
      end
    end
  end

  describe "authentication" do
    it "authenticates with a Bearer Token by default" do
      stub_request(:post, client.url("/test"))
        .with(headers: { "Authorization" => "Bearer mytoken" })
        .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: '{"foo": "bar"}')

      response = client.post!("/test")
      expect(response.payload).to eq("foo" => "bar")
    end

    it "authenticates with Basic Auth using the client id and secret" do
      stub_request(:post, client.url("/test"))
        .with(basic_auth: [client.client_id, client.client_secret])
        .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: '{"foo": "bar"}')

      response = client.post!("/test", auth: :basic)
      expect(response.payload).to eq("foo" => "bar")
    end
  end

  describe "access token errors" do
    it "raises an AccessTokenError if the auth method was using an access token and the response was 401" do
      stub_request(:post, client.url("/test")).to_return(status: 401)
      expect { client.post!("/test") }.to raise_error(Agero::Rsc::AccessTokenError)
    end

    it "does not raise an AccessTokenError if the response was not 401" do
      stub_request(:post, client.url("/test")).to_return(status: 403)
      expect { client.post!("/test") }.to raise_error { |error|
        expect(error).not_to be_a(Agero::Rsc::AccessTokenError)
        expect(error).to be_a(Agero::Rsc::Error)
      }
    end

    it "does not raise an AccessTokenError if the request did not user a bearer token" do
      stub_request(:post, client.url("/test")).to_return(status: 401)
      expect { client.post!("/test", auth: :basic) }.to raise_error(Agero::Rsc::CredentialsError)
    end

    it "automatically invalidates existing access tokens if AccessTokenError raised" do
      stub_request(:post, client.url("/test"))
        .with(headers: { "Authorization" => "Bearer mytoken" })
        .to_return(status: 401)

      emails_sent = ActionMailer::Base.deliveries.count

      expect { client.post!("/test") }.to raise_error(Agero::Rsc::AccessTokenError)

      expect { token.reload }.to change(token, :connected)

      expect(ActionMailer::Base.deliveries.count).to eq(emails_sent + 1)
    end
  end

  describe "forbidden errors" do
    it "raises a ForbiddenError if the response was 403" do
      stub_request(:post, client.url("/test")).to_return(status: 403)
      expect { client.post!("/test") }.to raise_error(Agero::Rsc::ForbiddenError)
    end
  end

  describe "headers" do
    it "is able to add and override headers on the request" do
      stub_request(:post, client.url("/test"))
        .with(headers: { "X-Foo" => "Bar" })
        .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: '{"foo": "bar"}')

      response = client.post!("/test", headers: { "X-Foo" => "Bar" })
      expect(response.payload).to eq({ "foo" => "bar" })
    end
  end
end
