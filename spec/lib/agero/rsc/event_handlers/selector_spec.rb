# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::EventHandlers::Selector do
  subject(:find_handler_to) do
    Agero::Rsc::EventHandlers::Selector.find_handler_to(
      event: event, callback_token: callback_token
    )
  end

  let(:event) do
    { notificationCode: notification_code, vendorId: vendor_id }
  end

  let(:access_token) { 'Token: 12345' }
  let(:callback_token) { 'Token: CALLBACK12345' }
  let(:api_callback_token_from_request) { callback_token }
  let(:vendor_id) { '50230' }

  let!(:api_access_token) do
    create(
      :api_access_token,
      access_token: access_token,
      callback_token: api_callback_token_from_request,
      vendorid: vendor_id
    )
  end

  context 'when notificationCode is 1' do
    let(:notification_code) { 1 }

    it { is_expected.to be_instance_of(Agero::Rsc::EventHandlers::NewJobOffered) }
  end

  context 'when notificationCode is 7' do
    let(:notification_code) { 7 }

    it { is_expected.to be_instance_of(Agero::Rsc::EventHandlers::EtaRejected) }
  end

  context 'when notificationCode is 8' do
    let(:notification_code) { 8 }

    it { is_expected.to be_instance_of(Agero::Rsc::EventHandlers::JobApproved) }
  end

  context 'when notificationCode is 5' do
    let(:notification_code) { 5 }

    it { is_expected.to be_instance_of(Agero::Rsc::EventHandlers::AgeroCanceled) }
  end

  context 'when notificationCode is 23' do
    let(:notification_code) { 23 }

    it { is_expected.to be_instance_of(Agero::Rsc::EventHandlers::AgeroCanceled) }
  end

  context 'when notificationCode is not known' do
    let(:notification_code) { -1 }

    it "raises an error" do
      expect { subject }.to raise_error(ArgumentError)
    end
  end
end
