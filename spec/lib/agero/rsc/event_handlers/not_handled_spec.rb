# frozen_string_literal: true

require "rails_helper"

describe Agero::Rsc::EventHandlers::NotHandled do
  include_context "basic rsc setup"

  subject(:handler_instance) do
    Agero::Rsc::EventHandlers::NotHandled.new(
      event: event, callback_token: callback_token
    ).handle!
  end

  let(:event) do
    {
      notificationEventId: 1000,
      notificationCode: 1000,
      vendorId: vendor_id,
      facilityId: facility_id,
      dispatchSource: 'eDispatch',
      dispatchRequestNumber: 987654321,
      title: 'Im not handled',
    }
  end

  let(:vendor_id) { '10000' }
  let(:facility_id) { '1' }

  let(:callback_token) { 'Token: CALLBACK12345' }
  let(:access_token) { 'Token: 12345' }
  let!(:api_access_token) do
    create(
      :api_access_token,
      access_token: access_token,
      callback_token: callback_token,
      vendorid: vendor_id
    )
  end
end
