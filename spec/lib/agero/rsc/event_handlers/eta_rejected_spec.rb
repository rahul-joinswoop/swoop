# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::EventHandlers::EtaRejected do
  describe '#handle!' do
    subject(:handle_eta_rejected) do
      Agero::Rsc::EventHandlers::EtaRejected.new(
        event: event, callback_token: callback_token
      ).handle!
    end

    let(:event) do
      {
        notificationEventId: notification_event_id,
        notificationCode: notification_code,
        vendorId: vendor_id,
        facilityId: facility_id,
        dispatchSource: dispatch_source,
        dispatchRequestNumber: dispatch_request_number,
        title: title,
      }
    end

    let(:notification_event_id) { '123456789' }
    let(:notification_code) { 7 }
    let(:vendor_id) { '50230' }
    let(:facility_id) { '1' }
    let(:dispatch_source) { 'eDispatch' }
    let(:dispatch_request_number) { 123456789 }
    let(:title) { 'ETA Rejected' }

    let(:access_token) { 'Token: 12345' }
    let(:callback_token) { 'Token: CALLBACK12345' }

    let!(:api_access_token) do
      create(
        :api_access_token,
        access_token: access_token,
        callback_token: callback_token,
        vendorid: vendor_id
      )
    end

    before do
      allow(Agero::Rsc::EtaRejectedWorker).to receive(:perform_async)
    end

    it 'schedules Agero::Rsc::EtaRejectedWorker with the given event' do
      handle_eta_rejected

      expect(Agero::Rsc::EtaRejectedWorker).to have_received(:perform_async).with(
        callback_token, event
      )
    end
  end
end
