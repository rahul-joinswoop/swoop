# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::EventHandlers::NewJobOffered do
  describe '#handle!' do
    subject(:handle_new_job_offered) do
      Agero::Rsc::EventHandlers::NewJobOffered.new(
        event: event, callback_token: callback_token
      ).handle!
    end

    let(:event) do
      {
        notificationEventId: notification_event_id,
        notificationCode: notification_code,
        vendorId: vendor_id,
        facilityId: facility_id,
        dispatchSource: dispatch_source,
        dispatchRequestNumber: dispatch_request_number,
        title: title,
      }
    end

    let(:notification_event_id) { '123456789' }
    let(:notification_code) { 1 }
    let(:vendor_id) { '50230' }
    let(:facility_id) { '1' }
    let(:dispatch_source) { 'eDispatch' }
    let(:dispatch_request_number) { 123456789 }
    let(:title) { 'New Offer' }

    let(:access_token) { 'Token: 12345' }
    let(:callback_token) { 'Token: CALLBACK12345' }

    let!(:api_access_token) do
      create(
        :api_access_token,
        access_token: access_token,
        callback_token: callback_token,
        vendorid: vendor_id
      )
    end

    before do
      allow(Agero::Rsc::NewJobOfferedWorker).to receive(:perform_async)
    end

    it 'schedules Agero::Rsc::NewJobOfferedWorker with the given event' do
      handle_new_job_offered

      expect(Agero::Rsc::NewJobOfferedWorker).to have_received(:perform_async).with(
        callback_token, event
      )
    end
  end
end
