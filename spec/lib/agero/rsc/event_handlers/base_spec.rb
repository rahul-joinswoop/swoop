# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::EventHandlers::Base do
  before do
    stub_class 'EventHandlerMock', Agero::Rsc::EventHandlers::Base
  end

  subject(:event_handler_instance_mock) do
    EventHandlerMock.new(event: event, callback_token: callback_token)
  end

  let(:callback_token) { 'Token: CALLBACK12345' }

  let(:event) do
    {
      notificationEventId: notification_event_id,
      notificationCode: notification_code,
      vendorId: vendor_id,
      facilityId: facility_id,
      dispatchSource: dispatch_source,
      dispatchRequestNumber: dispatch_request_number,
      title: title,
    }
  end

  let(:notification_event_id) { '123456789' }
  let(:notification_code) { 1 }
  let(:vendor_id) { '50230' }
  let(:facility_id) { '1' }
  let(:access_token) { 'Token: 1234567890' }
  let(:dispatch_source) { 'eDispatch' }
  let(:dispatch_request_number) { 123456789 }
  let(:title) { 'New Offer' }

  context '#handle!' do
    context 'when subclass does not implement it' do
      it 'raises NotImplementedError' do
        expect { event_handler_instance_mock.send(:handle!) }.to raise_error(NotImplementedError)
      end
    end
  end
end
