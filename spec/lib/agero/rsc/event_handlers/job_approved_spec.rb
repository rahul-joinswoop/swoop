# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::EventHandlers::JobApproved do
  describe '#handle!' do
    subject(:handle_job_approved) do
      Agero::Rsc::EventHandlers::JobApproved.new(
        event: event, callback_token: callback_token
      ).handle!
    end

    let(:event) do
      {
        notificationEventId: notification_event_id,
        notificationCode: notification_code,
        vendorId: vendor_id,
        facilityId: facility_id,
        dispatchSource: dispatch_source,
        dispatchRequestNumber: dispatch_request_number,
        title: title,
      }
    end

    let(:notification_event_id) { '123456789' }
    let(:notification_code) { 8 }
    let(:vendor_id) { '50230' }
    let(:facility_id) { '1' }
    let(:dispatch_source) { 'eDispatch' }
    let(:dispatch_request_number) { 123456789 }
    let(:title) { 'Job Approved' }

    let(:access_token) { 'Token: 12345' }
    let(:callback_token) { 'Token: CALLBACK12345' }

    let!(:api_access_token) do
      create(
        :api_access_token,
        access_token: access_token,
        callback_token: callback_token,
        vendorid: vendor_id
      )
    end

    before do
      allow(Agero::Rsc::JobApprovedWorker).to receive(:perform_async)
    end

    it 'schedules Agero::Rsc::JobApprovedWorker with the given event' do
      handle_job_approved

      expect(Agero::Rsc::JobApprovedWorker).to have_received(:perform_async).with(
        callback_token, event
      )
    end
  end
end
