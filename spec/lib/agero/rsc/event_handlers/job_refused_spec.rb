# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::EventHandlers::JobRefused do
  describe '#handle!' do
    let(:vendor_id) { '50230' }
    let(:event) do
      {
        notificationEventId: 123456789,
        notificationCode: 2,
        vendorId: vendor_id,
        facilityId: '1',
        dispatchSource: 'eDispatch',
        dispatchRequestNumber: 123456789,
        title: 'Job Refused',
      }
    end
    let(:access_token) { 'Token: 12345' }
    let(:callback_token) { 'Token: CALLBACK12345' }

    let!(:api_access_token) do
      create(
        :api_access_token,
        access_token: access_token,
        callback_token: callback_token,
        vendorid: vendor_id
      )
    end

    it 'schedules Agero::Rsc::JobRefusedWorker with the given event' do
      Agero::Rsc::EventHandlers::JobRefused.new(
        event: event, callback_token: callback_token
      ).handle!

      expect(Agero::Rsc::JobRefusedWorker).to have_enqueued_sidekiq_job(callback_token, event)
    end
  end
end
