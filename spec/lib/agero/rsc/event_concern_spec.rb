# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::EventConcern do
  before do
    stub_class 'HelperImpl' do
      include Agero::Rsc::EventConcern

      def initialize(context, callback_token = nil, event = nil)
        @callback_token = callback_token
        @event = event
        super(context)
      end

      def call
        context[:output] = { stuff: 1 }
      end
    end
  end

  let(:callback_token) { 'TOKEN 123123123' }
  let(:event) do
    {
      vendorId: '998877',
      notificationEventId: '3V3N7',
    }
  end

  let!(:api_access_token_db) do
    create(:api_access_token, vendorid: event[:vendorId], callback_token: callback_token)
  end

  describe "output" do
    it "includes the callback token and the event" do
      context = HelperImpl.call(input: { callback_token: "123", event: { "foo" => "bar" } })
      expect(context[:output]).to eq(stuff: 1, callback_token: "123", event: { "foo" => "bar" })
    end
  end

  describe '#api_access_token' do
    subject(:api_access_token) do
      HelperImpl.new(nil, callback_token, event).api_access_token
    end

    it 'finds the access token given the vendorid and callback_token' do
      expect(api_access_token.id).to eq api_access_token_db.id
      expect(api_access_token.vendorid).to eq api_access_token_db.vendorid
      expect(api_access_token.callback_token).to eq api_access_token_db.callback_token
    end
  end

  describe '#rsc_api' do
    subject(:rsc_api) do
      HelperImpl.new(nil, callback_token, event).rsc_api
    end

    it 'instantiates a Rsc::API with the api_access_token.access_token' do
      expect(Agero::Rsc::API).to receive(:new).with(api_access_token_db.access_token)

      rsc_api
    end
  end

  describe '#notification_event_id' do
    subject(:notification_event_id) do
      HelperImpl.new(nil, callback_token, event).notification_event_id
    end

    it { is_expected.to eq '3V3N7' }
  end

  describe "dispatch_details" do
    let(:event) do
      {
        notificationEventId: '123456789',
        notificationCode: 1,
        vendorId: '50230',
        facilityId: '1',
        dispatchSource: 'eDispatch',
        dispatchRequestNumber: 123,
        title: 'New Offer',
      }
    end

    let(:api_access_token) { create(:api_access_token, vendorid: vendor_id) }

    before do
      stub_request(:get, Agero::Rsc::Client.url("/dispatches/123/detail"))
        .to_return(
          status: 200,
          headers: { "Content-Type" => "application/json" },
          body: '{"foo":"bar"}'
        )
    end

    it 'requires job details from RSC API' do
      details = HelperImpl.new(nil, callback_token, event).dispatch_details
      expect(details).to eq({ "foo" => "bar" })
    end
  end
end
