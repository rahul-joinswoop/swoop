# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::Data do
  describe "code lookups" do
    it "lookups notification types by id" do
      expect(Agero::Rsc::Data.notification_type(1)).to eq "new_job_offered"
      expect { Agero::Rsc::Data.notification_type(-1) }.to raise_error(ArgumentError)
    end

    it "lookups notification type ids by symbol" do
      expect(Agero::Rsc::Data.notification_type_id(:new_job_offered)).to eq 1
      expect { Agero::Rsc::Data.notification_type_id(:not_exist) }.to raise_error(ArgumentError)
    end

    it "lookups status codes by id" do
      expect(Agero::Rsc::Data.status_code(1)).to eq "new"
      expect { Agero::Rsc::Data.status_code(-1) }.to raise_error(ArgumentError)
    end

    it "lookups status code ids by symbol" do
      expect(Agero::Rsc::Data.status_code_id(:new)).to eq 1
      expect { Agero::Rsc::Data.status_code_id(:not_exist) }.to raise_error(ArgumentError)
    end

    it "lookups ETA reason codes by id" do
      expect(Agero::Rsc::Data.eta_reason_code(401)).to eq "weather"
      expect { Agero::Rsc::Data.eta_reason_code(-1) }.to raise_error(ArgumentError)
    end

    it "lookups ETA reason code ids by symbol" do
      expect(Agero::Rsc::Data.eta_reason_code_id(:weather)).to eq 401
      expect { Agero::Rsc::Data.eta_reason_code_id(:not_exist) }.to raise_error(ArgumentError)
    end

    it "lookups ETA reason descriptions ids by symbol" do
      expect(Agero::Rsc::Data.eta_reason_description(:weather)).to eq "Weather Emergency"
      expect(Agero::Rsc::Data.eta_reason_description(:traffic)).to eq "Extreme traffic in the area"
      expect(Agero::Rsc::Data.eta_reason_description(:other)).to eq "None"
    end

    it "lookups refusal reason codes by id" do
      expect(Agero::Rsc::Data.refusal_reason_code(400)).to eq "payment_type"
      expect { Agero::Rsc::Data.refusal_reason_code(-1) }.to raise_error(ArgumentError)
    end

    it "lookups refusal reason code ids by symbol" do
      expect(Agero::Rsc::Data.refusal_reason_code_id(:payment_type)).to eq 400
      expect { Agero::Rsc::Data.refusal_reason_code_id(:not_exist) }.to raise_error(ArgumentError)
    end

    it "lookups cancel reason codes by id" do
      expect(Agero::Rsc::Data.cancel_reason_code(201)).to eq "prior_job_delayed"
      expect { Agero::Rsc::Data.cancel_reason_code(-1) }.to raise_error(ArgumentError)
    end

    it "lookups cancel reason code ids by symbol" do
      expect(Agero::Rsc::Data.cancel_reason_code_id(:prior_job_delayed)).to eq 201
      expect { Agero::Rsc::Data.cancel_reason_code_id(:not_exist) }.to raise_error(ArgumentError)
    end

    it "matches a call method type to a club job type" do
      expect(Agero::Rsc::Data.club_job_type("eDispatch")).to eq "Digital"
      expect(Agero::Rsc::Data.club_job_type("LivePhone")).to eq "Phone Transcript"
      expect(Agero::Rsc::Data.club_job_type("IVR")).to eq "Phone Transcript"
    end
  end

  describe "event_handler" do
    it "looks up an event handler by notification code" do
      expect(Agero::Rsc::Data.event_handler(1)).to eq Agero::Rsc::EventHandlers::NewJobOffered
      expect(Agero::Rsc::Data.event_handler(7)).to eq Agero::Rsc::EventHandlers::EtaRejected
      expect(Agero::Rsc::Data.event_handler(8)).to eq Agero::Rsc::EventHandlers::JobApproved
      expect(Agero::Rsc::Data.event_handler(5)).to eq Agero::Rsc::EventHandlers::AgeroCanceled
      expect(Agero::Rsc::Data.event_handler(23)).to eq Agero::Rsc::EventHandlers::AgeroCanceled
    end

    it "returns nil if the code is not defined" do
      expect(Agero::Rsc::Data.event_handler(-1)).to eq nil
    end
  end

  describe "location" do
    it "serializes a location with a geocoded address" do
      loc = Location.new(street: "111 S. Michigan Ave.", city: "Chicago", state: "IL", zip: "60604", lat: 41.88, lng: -87.62)
      expect(Agero::Rsc::Data.location(loc)).to eq({
        address1: "111 S. Michigan Ave.",
        city: "Chicago",
        state: "IL",
        postalCode: "60604",
        lat: 41.88,
        lng: -87.62,
      })
    end

    it "serializes a location with a geocoded address as address1" do
      loc = Location.new(address: "435 N. Michigan Ave., Chicago, IL, 60611", lat: 41.88, lng: -87.62)
      expect(Agero::Rsc::Data.location(loc)).to eq({
        address1: "435 N. Michigan Ave., Chicago, IL, 60611",
        lat: 41.88,
        lng: -87.62,
      })
    end
  end

  describe "driver_profile" do
    let(:company) { create(:rescue_company, phone: "+13125551212", dispatch_email: "dispatch@example.com") }
    let(:driver) { create(:driver_on_duty, company: company, phone: "+16165551212") }
    let(:swoop) { create(:super_company, :swoop) }

    it "serializes just a driver" do
      expect(Agero::Rsc::Data.driver_profile(driver)).to eq({
        externalDriverId: driver.id.to_s,
        name: driver.name,
        cellPhone: "+16165551212",
        officePhone: "+13125551212",
        officeEmail: "dispatch@example.com",
        onDuty: true,
        gpsTracking: false,
      })
    end

    it "gets contact info from the HQ rescue provider" do
      site = create(:site, name: "HQ", company: company)
      create(:rescue_provider, site: site, phone: "+14145551212", dispatch_email: "site@example.com", company: swoop)
      expect(Agero::Rsc::Data.driver_profile(driver)).to eq({
        externalDriverId: driver.id.to_s,
        name: driver.name,
        cellPhone: "+16165551212",
        officePhone: "+14145551212",
        officeEmail: "site@example.com",
        onDuty: true,
        gpsTracking: false,
      })
    end

    it "gets the contact info from site the rescue provider" do
      hq = create(:site, name: "HQ", company: company)
      create(:rescue_provider, site: hq, phone: "+14145551212", dispatch_email: "hq@example.com", company: swoop)
      site = create(:site, name: "Site 1", company: company)
      create(:rescue_provider, site: site, phone: "+16085551212", dispatch_email: "site@example.com", company: swoop)
      expect(Agero::Rsc::Data.driver_profile(driver, site: site)).to eq({
        externalDriverId: driver.id.to_s,
        name: driver.name,
        cellPhone: "+16165551212",
        officePhone: "+16085551212",
        officeEmail: "site@example.com",
        onDuty: true,
        gpsTracking: false,
      })
    end

    it "serializes a driver with a vehicle location" do
      vehicle = create(:rescue_vehicle)
      vehicle.update_location!(lat: 48.1, lng: -90.5, timestamp: Time.current)
      expect(Agero::Rsc::Data.driver_profile(driver, vehicle: vehicle)).to eq({
        externalDriverId: driver.id.to_s,
        name: driver.name,
        cellPhone: "+16165551212",
        officePhone: "+13125551212",
        officeEmail: "dispatch@example.com",
        onDuty: true,
        gpsTracking: true,
        currentLocation: {
          lat: 48.1,
          lng: -90.5,
        },
      })
    end
  end

  describe '.current_job_status_code' do
    subject(:current_job_status_code) { Agero::Rsc::Data.current_job_status_code(job) }

    let(:job) { create(:fleet_motor_club_job, status: status) }

    context 'when job.status is accepted' do
      let(:status) { Job::STATUS_ACCEPTED }

      it { is_expected.to eq(:unassigned) }
    end
  end

  describe '.job_status_code' do
    subject(:job_status_code) { Agero::Rsc::Data.job_status_code(job_status_id) }

    context 'when job_status_id maps to accepted' do
      let(:job_status_id) { create(:job_status, :accepted).id }

      it { is_expected.to eq(:unassigned) }
    end
  end

  describe '.job_eta_explanation_code' do
    subject(:job_eta_explanation_code) do
      Agero::Rsc::Data.job_eta_explanation_code(job_eta_explanation_id)
    end

    context 'when job_eta_explanation_id maps to None' do
      let(:job_eta_explanation_id) { create(:job_eta_explanation, text: 'None').id }

      it { is_expected.to eq(:other) }
    end

    context 'when job_eta_explanation_id maps to Traffic' do
      let(:job_eta_explanation_id) { create(:job_eta_explanation, text: 'Traffic').id }

      it { is_expected.to eq(:traffic) }
    end

    context 'when job_eta_explanation_id maps to Weather' do
      let(:job_eta_explanation_id) { create(:job_eta_explanation, text: 'Weather').id }

      it { is_expected.to eq(:weather) }
    end

    context 'when job_eta_explanation_id is nil' do
      let(:job_eta_explanation_id) { nil }

      it { is_expected.to eq(:other) }
    end

    context 'when job_eta_explanation_id does not match an Agero one' do
      let(:job_eta_explanation_id) { create(:job_eta_explanation, text: :whatever).id }

      it { is_expected.to eq(:other) }
    end
  end

  describe '.job_refusal_explanation_code' do
    subject(:job_refusal_explanation_code) do
      Agero::Rsc::Data.job_refusal_explanation_code(job_reject_reason_id)
    end

    context "when job_reject_reason_id maps to 'Do not accept payment type'" do
      let(:job_reject_reason_id) do
        create(:job_reject_reason, text: 'Do not accept payment type').id
      end

      it { is_expected.to eq(:payment_type) }
    end

    context "when job_reject_reason_id maps to 'No longer offer service'" do
      let(:job_reject_reason_id) { create(:job_reject_reason, text: 'No longer offer service').id }

      it { is_expected.to eq(:no_longer_offering_service) }
    end

    context "when job_reject_reason_id maps to 'Out of my coverage area'" do
      let(:job_reject_reason_id) { create(:job_reject_reason, text: 'Out of my coverage area').id }

      it { is_expected.to eq(:out_of_coverage_area) }
    end

    context 'when job_reject_reason_id maps to Other' do
      let(:job_reject_reason_id) { create(:job_reject_reason, text: 'Other').id }

      it { is_expected.to eq(:other) }
    end

    context "when job_reject_reason_id maps to 'Proper equipment not available'" do
      let(:job_reject_reason_id) do
        create(:job_reject_reason, text: 'Proper equipment not available').id
      end

      it { is_expected.to eq(:equipment_not_available) }
    end

    context 'when job_reject_reason_id is nil' do
      let(:job_reject_reason_id) { nil }

      it { is_expected.to eq :other }
    end

    context 'when job_reject_reason_id does not match an Agero one' do
      let(:job_reject_reason_id) { create(:job_reject_reason, text: :whatever).id }

      it { is_expected.to eq(:other) }
    end
  end

  describe '.rsc_status_and_reason_code_hash' do
    subject(:rsc_status_and_reason_code_hash) do
      Agero::Rsc::Data.rsc_status_and_reason_code_hash(job_reason_key, job_status)
    end

    context 'when job_status maps to JobStatus::CANCELED' do
      let(:job_status) { JobStatus::CANCELED }

      context "and job_reason_key is cancel_prior_job_delayed" do
        let(:job_reason_key) { 'cancel_prior_job_delayed' }

        it { is_expected.to match({ reason_code: :prior_job_delayed, status_code: :service_provider_cancel }) }
      end

      context "and job_reason_key is cancel_traffic_service_vehicle_problem" do
        let(:job_reason_key) { 'cancel_traffic_service_vehicle_problem' }

        it { is_expected.to match({ reason_code: :traffic_vehicle_problem, status_code: :service_provider_cancel }) }
      end

      context "and job_reason_key is cancel_out_of_area" do
        let(:job_reason_key) { 'cancel_out_of_area' }

        it { is_expected.to match({ reason_code: :out_of_area, status_code: :service_provider_cancel }) }
      end

      context "and job_reason_key is cancel_another_job_priority" do
        let(:job_reason_key) { 'cancel_another_job_priority' }

        it { is_expected.to match({ reason_code: :another_job_priority, status_code: :service_provider_cancel }) }
      end

      context "and job_reason_key is cancel_no_reason_given" do
        let(:job_reason_key) { 'cancel_no_reason_given' }

        it { is_expected.to match({ reason_code: :no_reason_given, status_code: :service_provider_cancel }) }
      end

      context "and job_reason_key is cancel_customer_found_alternate_solution" do
        let(:job_reason_key) { 'cancel_customer_found_alternate_solution' }

        it { is_expected.to match({ reason_code: :found_alternate_solution, status_code: :customer_cancel }) }
      end
    end

    context 'when job_status maps to JobStatus::GOA' do
      let(:job_status) { JobStatus::GOA }

      context "and job_reason_key is goa_customer_cancel_after_deadline" do
        let(:job_reason_key) { 'goa_customer_cancel_after_deadline' }

        it { is_expected.to match({ reason_code: :customer_cancel, status_code: :customer_cancel }) }
      end

      context "and job_reason_key is goa_wrong_location_given" do
        let(:job_reason_key) { 'goa_wrong_location_given' }

        it { is_expected.to match({ reason_code: :wrong_location_given, status_code: :unsuccessful_no_vehicle }) }
      end

      context "and job_reason_key is goa_customer_not_with_vehicle" do
        let(:job_reason_key) { 'goa_customer_not_with_vehicle' }

        it { is_expected.to match({ reason_code: :customer_not_with_vehicle, status_code: :unsuccessful_no_customer }) }
      end

      context "and job_reason_key is goa_incorrect_service" do
        let(:job_reason_key) { 'goa_incorrect_service' }

        it { is_expected.to match({ reason_code: :incorrect_service, status_code: :service_attempt_failed }) }
      end

      context "and job_reason_key is goa_incorrect_equipment" do
        let(:job_reason_key) { 'goa_incorrect_equipment' }

        it { is_expected.to match({ reason_code: :incorrect_equipment, status_code: :service_attempt_failed }) }
      end

      context "and job_reason_key is goa_unsuccessful_service_attempt" do
        let(:job_reason_key) { 'goa_unsuccessful_service_attempt' }

        it { is_expected.to match({ reason_code: :need_additional_equipment, status_code: :service_attempt_failed }) }
      end
    end
  end

  describe "location_type" do
    it "translates an Agero location type into a Swoop location type name" do
      expect(Agero::Rsc::Data.location_type("Driveway")).to eq LocationType::RESIDENCE
    end
  end

  describe "time formats" do
    let(:time) { Time.parse("2018-08-21T05:56:22.123-0800") }

    it "formats a time with the Agero format" do
      expect(Agero::Rsc::Data.format_time(time)).to eq "20180821T13: 56: 22Z"
    end

    it "formats a time as a UTC timestamp" do
      expect(Agero::Rsc::Data.utc_timestamp(time)).to eq "2018-08-21 01:56 PM"
    end
  end
end
