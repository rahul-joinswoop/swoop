# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::Response do
  it "has a status code" do
    stub_request(:get, "http://example.com/").to_return(status: 200)
    response = Agero::Rsc::Response.new(HTTP.get("http://example.com"))
    expect(response.code).to eq 200
    expect(response.success?).to eq true
  end

  it "has a content_type" do
    stub_request(:get, "http://example.com/").to_return(headers: { "Content-Type" => "application/json; charset=utf-8" })
    response = Agero::Rsc::Response.new(HTTP.get("http://example.com"))
    expect(response.content_type).to eq "application/json"
  end

  it "has the response headers" do
    stub_request(:get, "http://example.com/").to_return(headers: { "X-Foo" => "bar" })
    response = Agero::Rsc::Response.new(HTTP.get("http://example.com"))
    expect(response.headers["X-Foo"]).to eq "bar"
    expect(response.headers["x-foo"]).to eq "bar"
  end

  it "has a body" do
    stub_request(:get, "http://example.com/").to_return(body: "foobar")
    response = Agero::Rsc::Response.new(HTTP.get("http://example.com"))
    expect(response.body).to eq "foobar"
  end

  it "parses a json response to the payload" do
    stub_request(:get, "http://example.com/").to_return(
      headers: { "Content-Type" => "application/json; charset=utf-8" },
      body: '{"foo":"bar"}',
    )
    response = Agero::Rsc::Response.new(HTTP.get("http://example.com"))
    expect(response.payload).to eq("foo" => "bar")
  end

  it "does not have a payload if the response is not JSON" do
    stub_request(:get, "http://example.com/").to_return(headers: { "Content-Type" => "tex/html" }, body: "foobar")
    response = Agero::Rsc::Response.new(HTTP.get("http://example.com"))
    expect(response.payload).to eq nil
  end

  it "tries to parse the payload if the response is an error" do
    stub_request(:get, "http://example.com/").to_return(status: 503, body: '{"error": "server busy"}')
    response = Agero::Rsc::Response.new(HTTP.get("http://example.com"))
    expect(response.payload).to eq("error" => "server busy")
  end
end
