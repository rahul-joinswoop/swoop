# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc::Status do
  it "serializes a job with the current status" do
    job = create(:job, status: "On Site")
    status = Agero::Rsc::Status.new(job)

    expect(status.to_hash).to eq({
      code: "30",
      source: "Swoop",
      extJobId: job.id.to_s,
      statusTime: Agero::Rsc::Data.utc_timestamp(job.updated_at),
    }.with_indifferent_access)
  end

  it "serializes a job a different name for the timestamp if used for GPS updates" do
    job = create(:job, status: "On Site")
    status = Agero::Rsc::Status.new(job, gps: true)

    expect(status.to_hash).to eq({
      code: "30",
      source: "Swoop",
      extJobId: job.id.to_s,
      timestamp: Agero::Rsc::Data.utc_timestamp(job.updated_at),
    }.with_indifferent_access)
  end

  it "serializes a job information from the rescue driver and vehicle" do
    driver = create(:driver_on_duty)
    vehicle = create(:rescue_vehicle, lat: 45.0, lng: -118.0)
    job = create(:job, status: "On Site", rescue_driver: driver, rescue_vehicle: vehicle)
    status = Agero::Rsc::Status.new(job)

    expect(status.to_hash).to eq({
      code: "30",
      source: "Swoop",
      extJobId: job.id.to_s,
      statusTime: Agero::Rsc::Data.utc_timestamp(job.updated_at),
      externalDriverId: driver.id.to_s,
      latitude: 45.0,
      longitude: -118.0,
    }.with_indifferent_access)
  end

  it "serializes a job with a specific status" do
    job = create(:job, status: "On Site")

    expect(Agero::Rsc::Status.new(job, code: :po_needed).to_hash[:code]).to eq "2"
    expect(Agero::Rsc::Status.new(job, code: 3).to_hash[:code]).to eq "3"
  end

  it "serializes a job with a specific location" do
    job = create(:job, status: "On Site")
    hash = Agero::Rsc::Status.new(job, latitude: 48.8, longitude: -110.5).to_hash

    expect(hash[:latitude]).to eq 48.8
    expect(hash[:longitude]).to eq(-110.5)
  end

  it "serializes a job with an ETA" do
    job = create(:job, status: "On Site")

    expect(Agero::Rsc::Status.new(job, eta: 30).to_hash[:eta]).to eq 30
  end
end
