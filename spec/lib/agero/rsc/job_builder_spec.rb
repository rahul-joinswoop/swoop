# frozen_string_literal: true

require 'rsc_helper'

describe Agero::Rsc::JobBuilder do
  let(:dispatch) { JSON.load(rsc_api_response(:get_dispatch)) }
  let(:fleet_company) { create(:fleet_company, :ago) }

  describe "build" do
    let(:job) { FleetMotorClubJob.new(fleet_company: fleet_company) }

    it "updates the po_number" do
      dispatch["poNumber"] = "123456"
      Agero::Rsc::JobBuilder.new(job).build(dispatch)
      expect(job.po_number).to eq "123456"
    end

    it "updates the odometer" do
      dispatch["vehicle"]["mileage"] = "123456"
      Agero::Rsc::JobBuilder.new(job).build(dispatch)
      expect(job.odometer).to eq 123456
    end

    it "updates the ref_number" do
      dispatch["referenceNumber"] = "123456"
      Agero::Rsc::JobBuilder.new(job).build(dispatch)
      expect(job.ref_number).to eq "123456"
    end

    it "updates the service_code_id" do
      dispatch["serviceType"] = "Door Unlock"
      Agero::Rsc::JobBuilder.new(job).build(dispatch)
      expect(job.service_code.name).to eq ServiceCode::LOCK_OUT
    end

    it "updates the received time" do
      time = Time.parse("2018-08-31T13:52:01")
      dispatch["receivedTime"] = time
      Agero::Rsc::JobBuilder.new(job).build(dispatch)
      expect(job.adjusted_created_at).to eq time
    end

    it "sets the dispatcher" do
      dispatch["disablementLocation"]["dispatcherContactNumber"] = "312-555-1212"
      dispatcher = create(:user, phone: "+13125551212")
      Agero::Rsc::JobBuilder.new(job).build(dispatch)
      expect(job.fleet_dispatcher).to eq dispatcher
    end

    it "sets the driver" do
      vehicle = dispatch["vehicle"]
      vehicle["vin"] = "12345678"
      vehicle["year"] = "2005"
      vehicle["make"] = "Ford"
      vehicle["model"] = "Mondeo"
      vehicle["color"] = "Red"
      vehicle["plate"] = "ABC123"

      location = dispatch["disablementLocation"]
      location["contactInfo"] = { "name" => "Joey The Lip", "callbackNumber" => "312-5553344" }

      Agero::Rsc::JobBuilder.new(job).build(dispatch)

      driver = job.driver
      expect(driver.user.full_name).to eq "Joey The Lip"
      expect(driver.user.phone).to eq "+13125553344"
      expect(driver.vehicle.vin).to eq "12345678"
      expect(driver.vehicle.year).to eq 2005
      expect(driver.vehicle.make).to eq "Ford"
      expect(driver.vehicle.model).to eq "Mondeo"
      expect(driver.vehicle.color).to eq "Red"
      expect(driver.vehicle.license).to eq "ABC123"
    end

    it "thes service location" do
      location = dispatch["disablementLocation"]
      location["address1"] = "435 N. Michigan Ave."
      location["city"] = "Chicago"
      location["state"] = "IL"
      location["postalCode"] = "60603"
      location["latitude"] = 45.1
      location["longitude"] = -90.5

      Agero::Rsc::JobBuilder.new(job).build(dispatch)

      expect(job.service_location.address).to eq "435 N. Michigan Ave., Chicago, IL 60603"
      expect(job.service_location.street).to eq "435 N. Michigan Ave."
      expect(job.service_location.city).to eq "Chicago"
      expect(job.service_location.state).to eq "IL"
      expect(job.service_location.zip).to eq "60603"
      expect(job.service_location.lat).to eq 45.1
      expect(job.service_location.lng).to eq(-90.5)
    end

    it "sets the drop off location" do
      location = dispatch["towDestination"]
      location["address1"] = "435 N. Michigan Ave."
      location["city"] = "Chicago"
      location["state"] = "IL"
      location["postalCode"] = "60603"
      location["latitude"] = 45.1
      location["longitude"] = -90.5

      Agero::Rsc::JobBuilder.new(job).build(dispatch)

      expect(job.drop_location.address).to eq "435 N. Michigan Ave., Chicago, IL 60603"
      expect(job.drop_location.street).to eq "435 N. Michigan Ave."
      expect(job.drop_location.city).to eq "Chicago"
      expect(job.drop_location.state).to eq "IL"
      expect(job.drop_location.zip).to eq "60603"
      expect(job.drop_location.lat).to eq 45.1
      expect(job.drop_location.lng).to eq(-90.5)
    end

    it "sets the drop off contact" do
      location = dispatch["towDestination"]
      location["contactInfo"] = { "name" => "Joey The Lip", "callbackNumber" => "312-5553344" }
      Agero::Rsc::JobBuilder.new(job).build(dispatch)
      expect(job.dropoff_contact.full_name).to eq "Joey The Lip"
      expect(job.dropoff_contact.phone).to eq "+13125553344"
    end

    describe "notes" do
      subject { Agero::Rsc::JobBuilder.new(job).build(dispatch).notes }

      let(:rsc_problem) { 'Tow' }
      let(:coverage) { "" }
      let(:urgency) { 'STANDARD' }
      let(:rsc_equipment_type) { 'LightDutyFlatbed' }
      let(:rsc_equipment_id) { nil }
      let(:customer_phone) { nil }

      let(:dispatch) do
        dispatch = JSON.load(rsc_api_response(:get_dispatch))

        dispatch["problem"] = rsc_problem
        dispatch["urgency"] = urgency
        dispatch["coverage"] = coverage
        dispatch["referenceNumber"] = "REF_NUMBER_123"

        dispatch["equipment"]["type"] = rsc_equipment_type
        dispatch["equipment"]["equipmentId"] = rsc_equipment_id

        dispatch["disablementLocation"]["contactInfo"]["name"] = "Customer name"
        dispatch["disablementLocation"]["contactInfo"]["callbackNumber"] = "4059876543"
        dispatch["disablementLocation"]["contactInfo"]["phone"] = customer_phone
        dispatch["disablementLocation"]["crossStreet"] = nil
        dispatch["disablementLocation"]["isDriverWithVehicle"] = nil

        dispatch["vehicle"]["fuelType"] = nil

        dispatch
      end

      let(:default_notes_text) do
        <<~HEREDOC
          Service Comments: be careful with dog in the back yard.
          Engine Type: LightDutyFlatbed
          Vehicle State: New
          Vehicle Type: Vehicle Type description
          Vehicle Drive Train Type: Drive Train of the vehicle
        HEREDOC
      end

      def adds_notes_text(text)
        expect(default_notes_text).not_to include(text)
        expect(subject).to include(text)
      end

      it 'adds expected notes' do
        expect(subject).to eq(default_notes_text)
      end

      context 'when customer has a phone in addition to a callbackNumber' do
        let(:customer_phone) { '1234567890' }

        it { adds_notes_text("Additional Phone Number: 1234567890") }
      end

      context 'when rsc_api_response.urgency is != from STANDARD' do
        let(:urgency) { 'URGENT' }

        it { adds_notes_text("URGENT") }
      end

      context 'when rsc_api_response.coverage is not blank' do
        let(:coverage) { "Covered for specific service." }

        it { adds_notes_text("Coverage: Covered for specific service.") }
      end

      context 'when rsc_api_response.vehicle.fuelType is not blank' do
        before do
          dispatch["vehicle"]["fuelType"] = "Gasoline"
        end

        it { adds_notes_text("Vehicle Fuel Type: Gasoline") }
      end

      context 'when rsc_api_response.disablementLocation.garageLevel is not blank' do
        before do
          dispatch["disablementLocation"]["garageLevel"] = "5A"
        end

        it { adds_notes_text("Garage Level: 5A") }
      end

      context 'when rsc_api_response.disablementLocation.garageClearanceHeight is not blank' do
        before do
          dispatch["disablementLocation"]["garageClearanceHeight"] = "100"
        end

        it { adds_notes_text("Garage Clearance: 100") }
      end

      context 'when rsc_api_response.disablementLocation.isDriverWithVehicle is not blank' do
        before do
          dispatch["disablementLocation"]["isDriverWithVehicle"] = true
        end

        it { adds_notes_text("Driver With Vehicle: Yes") }
      end

      context 'when rsc_api_response.disablementLocation.dropInstructions is not blank' do
        before do
          dispatch["disablementLocation"]["dropInstructions"] = "Be gentle with car."
        end

        it { adds_notes_text("Drop Instructions: Be gentle with car.") }
      end

      context 'when rsc_api_response.disablementLocation.nightDropOff is not blank' do
        before do
          dispatch["disablementLocation"]["nightDropOff"] = "Yes"
        end

        it { adds_notes_text("Night drop off: Yes") }
      end

      context 'when rsc_api_response.disablementLocation.parked is not blank' do
        before do
          dispatch["disablementLocation"]["parked"] = "Yes"
        end

        it { adds_notes_text("Parked: Yes") }
      end

      context 'when rsc_api_response.disablementLocation.crossStreet is not blank' do
        before do
          dispatch["disablementLocation"]["crossStreet"] = "123 Street Ave."
        end

        it { adds_notes_text("Pickup Cross Street: 123 Street Ave.") }
      end

      context 'when rsc_api_response.disablementLocation.landmark is not blank' do
        before do
          dispatch["disablementLocation"]["landmark"] = "A Tall Building"
        end

        it { adds_notes_text("Pickup Landmark: A Tall Building") }
      end

      context 'when rsc_api_response.disablementLocation.poi1 is not blank' do
        before do
          dispatch["disablementLocation"]["poi1"] = "McDonalds"
        end

        it { adds_notes_text("Pickup Point of Interest: McDonalds") }
      end

      context 'when rsc_api_response.disablementLocation.poi2 is not blank' do
        before do
          dispatch["disablementLocation"]["poi2"] = "Pizza Hut"
        end

        it { adds_notes_text("Pickup Point of Interest: Pizza Hut") }
      end

      context 'when rsc_api_response.towDestination.nightDropOff is not blank' do
        before do
          dispatch["towDestination"]["nightDropOff"] = "Yes"
        end

        it { adds_notes_text("Night drop off: Yes") }
      end

      context 'when rsc_api_response.towDestination.crossStreet is not blank' do
        before do
          dispatch["towDestination"]["crossStreet"] = "123 Street Ave."
        end

        it { adds_notes_text("Drop-Off Cross Street: 123 Street Ave.") }
      end

      context 'when rsc_api_response.towDestination.landmark is not blank' do
        before do
          dispatch["towDestination"]["landmark"] = "A Tall Building"
        end

        it { adds_notes_text("Drop-Off Landmark: A Tall Building") }
      end

      context 'when rsc_api_response.towDestination.poi1 is not blank' do
        before do
          dispatch["towDestination"]["poi1"] = "McDonalds"
        end

        it { adds_notes_text("Drop-Off Point of Interest: McDonalds") }
      end

      context 'when rsc_api_response.towDestination.poi2 is not blank' do
        before do
          dispatch["towDestination"]["poi2"] = "Pizza Hut"
        end

        it { adds_notes_text("Drop-Off Point of Interest: Pizza Hut") }
      end

      context 'when rsc_api_response.equipment.type is nil' do
        let(:rsc_equipment_type) { nil }

        it 'adds expected notes without Engine Type appended' do
          expect(subject).to eq(
            <<~HEREDOC
              Service Comments: be careful with dog in the back yard.
              Vehicle State: New
              Vehicle Type: Vehicle Type description
              Vehicle Drive Train Type: Drive Train of the vehicle
            HEREDOC
          )
        end
      end

      context 'when rsc_api_response.equipment.equipmentId is not nil' do
        let(:rsc_equipment_id) { 'XYZ123' }

        it { adds_notes_text("Engine ID: XYZ123") }
      end

      it "does not lose or duplicate notes" do
        dispatch["urgency"] = "URGENT"
        dispatch["equipment"]["type"] = "Hammer"
        job.notes = "URGENT\n\nMy Notes"
        Agero::Rsc::JobBuilder.new(job).build(dispatch)
        expect(job.notes).to include "Hammer"
        expect(job.notes).to start_with "URGENT"
        expect(job.notes[1, job.notes.length]).not_to include "URGENT"
      end
    end
  end

  describe "update!" do
    it "updates a new job and save it with all it's associations" do
      job = FleetMotorClubJob.new(fleet_company: fleet_company)
      Agero::Rsc::JobBuilder.new(job).update!(dispatch)
      expect(job).to be_persisted
      expect(job.driver).to be_persisted
      expect(job.driver.vehicle).to be_persisted
      expect(job.driver.user).to be_persisted
      expect(job.drop_location).to be_persisted
      expect(job.service_location).to be_persisted
      expect(job.dropoff_contact).to be_persisted
    end

    it "updates an existing job and save it with all it's associations" do
      driver = create(:drive)
      driver_user = driver.user
      driver_vehicle = driver.vehicle
      drop_location = create(:location)
      service_location = create(:location)
      dropoff_contact = create(:user)
      job = create(:fleet_motor_club_job, driver: driver, drop_location: drop_location, service_location: service_location, dropoff_contact: dropoff_contact)
      Agero::Rsc::JobBuilder.new(job).update!(dispatch)
      job.reload

      expect(job.driver.id).to eq driver.id
      expect(job.driver.vehicle_id).to eq driver_vehicle.id
      expect(job.driver.user_id).to eq driver_user.id
      expect(job.drop_location.id).to eq drop_location.id
      expect(job.service_location.id).to eq service_location.id
      expect(job.dropoff_contact.id).to eq dropoff_contact.id

      expect(job.po_number).to eq dispatch["poNumber"]
      expect(job.driver.user.name).to eq dispatch.dig("disablementLocation", "contactInfo", "name")
      expect(job.driver.vehicle.make).to eq dispatch.dig("vehicle", "make")
      expect(job.service_location.street).to eq dispatch.dig("disablementLocation", "address1")
      expect(job.drop_location.street).to eq dispatch.dig("towDestination", "address1")
      expect(job.dropoff_contact.name).to eq dispatch.dig("towDestination", "contactInfo", "name")
    end
  end
end
