# frozen_string_literal: true

require 'rsc_helper'

describe Agero::Rsc::API do
  let(:api) { Agero::Rsc::API.new("mytoken") }
  let(:client_id) { Agero::Rsc::Client.client_id }
  let(:client_secret) { Agero::Rsc::Client.client_secret }
  let(:empty_response) { { status: 200, body: nil } }
  let(:error_response) { { status: 500, body: '{"error":"boom"}' } }

  describe Agero::Rsc::API::ResponsePayload do
    it "returns a nested wrapped hash" do
      obj = Agero::Rsc::API::ResponsePayload.new("foo" => [{ "bar" => 1 }, { "baz" => 2 }], :other => "thing")
      expect(obj).to eq("foo" => [{ "bar" => 1 }, { "baz" => 2 }], "other" => "thing")
      expect(obj.other).to eq "thing"
      expect(obj[:other]).to eq "thing"
      expect(obj["other"]).to eq "thing"
      expect(obj.foo[0].bar).to eq 1
      expect(obj.foo[1].baz).to eq 2
    end

    it "parses time fields as UTC times" do
      attributes = {
        "foo" => "20150121T14: 55: 06Z",
        "bar" => [{ "baz" => "20160121T14: 55: 06", "other" => "20170121T14: 55: 06" }],
      }
      Time.use_zone("America/Los_Angeles") do
        obj = Agero::Rsc::API::ResponsePayload.new(attributes, time_fields: { foo: true, bar: { baz: true } })
        expect(obj.foo).to eq Time.utc(2015, 1, 21, 14, 55, 6)
        expect(obj.bar[0].baz).to eq Time.utc(2016, 1, 21, 14, 55, 6)
        expect(obj.bar[0].other).to eq "20170121T14: 55: 06"
      end
    end
  end

  describe '.for_job' do
    subject(:rsc_api_for_job) do
      Agero::Rsc::API.for_job(job_id)
    end

    let(:job_id) { job.id }
    let(:job) do
      create(
        :fleet_motor_club_job,
        fleet_company: fleet_company,
        rescue_company: rescue_company,
        issc_dispatch_request: issc_dispatch_request,
      )
    end

    let(:issc_dispatch_request) do
      create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", max_eta: 90.minutes)
    end
    let(:issc) do
      create(
        :ago_issc,
        :with_api_access_token,
        api_access_token: api_access_token,
        company: rescue_company,
      )
    end
    let(:api_access_token) { create(:api_access_token) }

    let(:rescue_company) { create(:rescue_company) }
    let(:fleet_company) { create(:fleet_company, :ago) }

    it { is_expected.to be_present }

    it 'instantiates Agero::Rsc::Client with expected access_token' do
      rsc_api = rsc_api_for_job

      rsc_client = rsc_api.send(:client)

      expect(rsc_client.instance_variable_get(:@access_token)).to eq api_access_token.access_token
    end

    context 'when job does not have a issc_dispatch_request' do
      let(:issc_dispatch_request) { nil }

      it 'raises ArgumentError' do
        expect { rsc_api_for_job }.to raise_error(Agero::Rsc::API::MissingAccessTokenError)
      end
    end

    context 'when issc does not have an api_access_token' do
      let(:api_access_token) { nil }

      it 'raises ArgumentError' do
        expect { rsc_api_for_job }.to raise_error(Agero::Rsc::API::MissingAccessTokenError)
      end
    end

    context 'when access token has been deleted' do
      it 'raises ArgumentError' do
        api_access_token.update!(deleted_at: Time.now)
        expect { rsc_api_for_job }.to raise_error(Agero::Rsc::API::MissingAccessTokenError)
      end
    end
  end

  describe "dispatch_request_number" do
    let(:job) do
      create(
        :fleet_motor_club_job,
        fleet_company: fleet_company,
        rescue_company: rescue_company,
        issc_dispatch_request: issc_dispatch_request,
      )
    end

    let(:rescue_company) { create(:rescue_company) }
    let(:fleet_company) { create(:fleet_company, :ago) }
    let(:api_access_token) { create(:api_access_token) }

    let(:issc_dispatch_request) do
      create(:issc_dispatch_request, issc: issc, jobid: "ISSC_JOB_123", dispatchid: "1234", max_eta: 90.minutes)
    end

    let(:issc) { create(:ago_issc, :with_api_access_token, system: "rsc", api_access_token: api_access_token, company: rescue_company) }

    it "returns the Agero dispatch request number from the issc request" do
      expect(Agero::Rsc::API.dispatch_request_number(job)).to eq "1234"
    end
  end

  describe "Dispatch API" do
    describe "get_dispatch" do
      it "returns the list of facilities" do
        payload = rsc_api_response(:get_dispatch)
        stub_request(:get, Agero::Rsc::Client.url("/dispatches/123/detail"))
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: payload)
        result = api.get_dispatch("123")
        expect(result).to eq JSON.load(payload)
      end

      it "raises error if the endpoint returns an error code" do
        stub_request(:get, Agero::Rsc::Client.url("/dispatches/123/detail"))
          .to_return(error_response)
        expect { api.get_dispatch("123") }.to raise_error(Agero::Rsc::Error)
      end
    end

    describe "accept_dispatch" do
      it "sends the eta to accept the dispatch" do
        stub_request(:post, Agero::Rsc::Client.url("/dispatches/123/acceptDispatchRequest"))
          .with(body: { eta: 30 })
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: nil)
        result = api.accept_dispatch("123", eta: 30)
        expect(result).to eq nil
      end

      it "includes a reason if one is given" do
        stub_request(:post, Agero::Rsc::Client.url("/dispatches/123/acceptDispatchRequest"))
          .with(body: { eta: 30, reasonCode: 403, reason: "Extreme traffic in the area" })
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: nil)
        result = api.accept_dispatch("123", eta: 30, reason: "traffic")
        expect(result).to eq nil
      end

      it "raises error if the endpoint returns an error code" do
        stub_request(:post, Agero::Rsc::Client.url("/dispatches/123/acceptDispatchRequest"))
          .to_return(error_response)
        expect { api.accept_dispatch("123", eta: 30) }.to raise_error(Agero::Rsc::Error)
      end
    end

    describe "refuse_dispatch" do
      it "refuses the dispatch" do
        stub_request(:post, Agero::Rsc::Client.url("/dispatches/123/refuseDispatchRequest"))
          .with(body: { reasonCode: 400 })
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: nil)
        result = api.refuse_dispatch("123", reason: "payment_type")
        expect(result).to eq nil
      end

      it "raises error if the endpoint returns an error code" do
        stub_request(:post, Agero::Rsc::Client.url("/dispatches/123/refuseDispatchRequest"))
          .to_return(error_response)
        expect { api.refuse_dispatch("123", reason: "payment_type") }.to raise_error(Agero::Rsc::Error)
      end
    end

    describe "request_more_time" do
      it "requests more time for the dispatch" do
        payload = rsc_api_response(:request_more_time)
        stub_request(:post, Agero::Rsc::Client.url("/dispatches/123/requestExtension"))
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: payload)
        result = api.request_more_time("123")
        expect(result).to eq JSON.load(payload)
      end

      it "raises error if the endpoint returns an error code" do
        stub_request(:post, Agero::Rsc::Client.url("/dispatches/123/requestExtension"))
          .to_return(error_response)
        expect { api.request_more_time("123") }.to raise_error(Agero::Rsc::Error)
      end
    end

    describe "assign_external_driver" do
      let(:company) { create(:rescue_company, phone: "+13125551212", dispatch_email: "dispatch@example.com") }
      let(:driver) { create(:driver_on_duty, company: company, phone: "+16165551212") }

      it "assigns a driver to the dispatch" do
        driver_info = {
          externalDriverId: Agero::Rsc::Data.swoop_driver_id(driver.id),
          driverProfile: Agero::Rsc::Data.driver_profile(driver),
        }
        stub_request(:put, Agero::Rsc::Client.url("/dispatches/123/assignExternalDriver"))
          .with(body: driver_info)
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: nil)
        result = api.assign_external_driver("123", driver: driver)
        expect(result).to eq nil
      end

      it "raises error if the endpoint returns an error code" do
        stub_request(:put, Agero::Rsc::Client.url("/dispatches/123/assignExternalDriver"))
          .to_return(error_response)
        expect do
          api.assign_external_driver("123", driver: driver)
        end.to raise_error(Agero::Rsc::Error)
      end
    end

    describe "cancel_dispatch" do
      it "cancels the dispatch" do
        stub_request(:post, Agero::Rsc::Client.url("/dispatches/123/cancel"))
          .with(body: { statusCode: "70", reasonCode: 203 })
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: nil)
        result = api.cancel_dispatch("123", status: "service_provider_cancel", reason: "out_of_area")
        expect(result).to eq nil
      end

      it "raises error if the endpoint returns an error code" do
        stub_request(:post, Agero::Rsc::Client.url("/dispatches/123/cancel"))
          .to_return(error_response)
        expect do
          api.cancel_dispatch("123", status: "service_provider_cancel", reason: "out_of_area")
        end.to raise_error(Agero::Rsc::Error)
      end
    end

    describe "request_phone_call" do
      it "requests a phone call" do
        stub_request(:put, Agero::Rsc::Client.url("/dispatches/123/requestPhoneCall"))
          .with(body: { callbackNumber: "+17085551212", name: "Bob" })
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: nil)
        result = api.request_phone_call("123", callback_phone_number: "+17085551212", name: "Bob")
        expect(result).to eq nil
      end

      it "includes an alternate phone number if one was given" do
        stub_request(:put, Agero::Rsc::Client.url("/dispatches/123/requestPhoneCall"))
          .with(body: { callbackNumber: "+17085551212", name: "Bob", phone: "+13125551212" })
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: nil)
        result = api.request_phone_call("123", callback_phone_number: "+17085551212", name: "Bob", alternate_phone: "+13125551212")
        expect(result).to eq nil
      end

      it "raises error if the endpoint returns an error code" do
        stub_request(:put, Agero::Rsc::Client.url("/dispatches/123/requestPhoneCall"))
          .to_return(error_response)
        expect do
          api.request_phone_call("123", callback_phone_number: "+17085551212", name: "Bob")
        end.to raise_error(Agero::Rsc::Error)
      end
    end

    describe "change_status" do
      let(:job) { create(:job, status: "On Site") }
      let(:status) { Agero::Rsc::Status.new(job) }

      it "changes the status" do
        payload = rsc_api_response(:change_status)
        stub_request(:post, Agero::Rsc::Client.url("/dispatches/123/status"))
          .with(body: status.to_hash.to_json)
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: payload)
        result = api.change_status("123", status: status)
        expect(result).to eq JSON.load(payload)
      end

      it "raises error if the endpoint returns an error code" do
        stub_request(:post, Agero::Rsc::Client.url("/dispatches/123/status"))
          .to_return(error_response)
        expect do
          api.change_status("123", status: status)
        end.to raise_error(Agero::Rsc::Error)
      end
    end

    describe "undo_status" do
      it "changes the status" do
        payload = rsc_api_response(:undo_status)
        stub_request(:post, Agero::Rsc::Client.url("/dispatches/123/status/undo"))
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: payload)
        result = api.undo_status("123")
        expect(result).to eq JSON.load(payload)
      end

      it "raises error if the endpoint returns an error code" do
        stub_request(:post, Agero::Rsc::Client.url("/dispatches/123/status/undo"))
          .to_return(error_response)
        expect do
          api.undo_status("123")
        end.to raise_error(Agero::Rsc::Error)
      end
    end
  end

  describe "Vendor API" do
    describe "facility_list" do
      it "returns the list of facilities" do
        payload = rsc_api_response(:facility_list)
        stub_request(:get, Agero::Rsc::Client.url("/vendors/123/facilities"))
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: payload)
        result = api.facility_list("123")

        expect(result).to eq JSON.load(payload)
      end

      it "raises error if the endpoint returns an error code" do
        stub_request(:get, Agero::Rsc::Client.url("/vendors/123/facilities"))
          .to_return(error_response)
        expect { api.facility_list("123") }.to raise_error(Agero::Rsc::Error)
      end
    end
  end

  describe "Enumerations API" do
    describe "enumeration" do
      it "returns an enumeration list" do
        payload = ["foo"].to_json
        stub_request(:get, Agero::Rsc::Client.url("/enumerations/serviceTypes"))
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: payload)
        result = api.enumeration("serviceTypes")

        expect(result).to eq JSON.load(payload)
      end
    end
  end

  describe "Tracking API" do
    describe "update_driver_locations" do
      it "updates locations" do
        timestamp = Time.at(Time.now.to_i)
        locations = [{ latitude: 45.1, longitude: -110.3, externalDriverId: "123", locationDateTime: timestamp.utc }]
        stub_request(:post, Agero::Rsc::Client.url("/drivers/currentLocations"))
          .with(body: locations.to_json)
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: "")
        result = api.update_driver_locations(driver_id: 123, latitude: 45.1, longitude: -110.3, timestamp: timestamp)
        expect(result).to eq nil
      end

      it "raises error if the endpoint returns an error code" do
        stub_request(:post, Agero::Rsc::Client.url("/drivers/currentLocations"))
          .to_return(error_response)
        expect do
          api.update_driver_locations(driver_id: 123, latitude: 45.1, longitude: -110.3, timestamp: Time.now)
        end.to raise_error(Agero::Rsc::Error)
      end
    end
  end

  describe "Authorization" do
    describe "authorize_url" do
      it "returns the redirect location for the login screen" do
        api = Agero::Rsc::API.new
        params = { client_id: client_id, state: "foobar", scope: "READ", response_type: "code", external_Driver_Mgmt: "false" }
        stub_request(:get, Agero::Rsc::Client.url("/oauth/authorize"))
          .with(query: params, basic_auth: [client_id, client_secret])
          .to_return(status: 302, headers: { "Location" => "https://agero.com/auth/login" })
        result = api.authorize_url("foobar")
        expect(result).to eq "https://agero.com/auth/login"
      end

      it "raises error if the endpoint returns an error code" do
        api = Agero::Rsc::API.new
        params = { client_id: client_id, state: "foobar", scope: "READ", response_type: "code", external_Driver_Mgmt: "false" }
        stub_request(:get, Agero::Rsc::Client.url("/oauth/authorize"))
          .with(query: params, basic_auth: [client_id, client_secret])
          .to_return(error_response)
        expect { api.authorize_url("foobar") }.to raise_error(Agero::Rsc::Error)
      end
    end

    describe "get_access_token" do
      it "returns the response payload" do
        payload = rsc_api_response(:get_access_token)
        api = Agero::Rsc::API.new
        stub_request(:get, Agero::Rsc::Client.url("/oauth/accesstoken"))
          .with(query: { grant_type: "authorization_code", code: "ABC" }, basic_auth: [client_id, client_secret])
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: payload)
        result = api.get_access_token("ABC")
        expect(result).to eq JSON.load(payload)
      end

      it "raises error if the endpoint returns an error code" do
        api = Agero::Rsc::API.new
        stub_request(:get, Agero::Rsc::Client.url("/oauth/accesstoken"))
          .with(query: { grant_type: "authorization_code", code: "ABC" }, basic_auth: [client_id, client_secret])
          .to_return(error_response)
        expect { api.get_access_token("ABC") }.to raise_error(Agero::Rsc::Error)
      end
    end

    describe "sign_in" do
      it "returns the response payload" do
        payload = rsc_api_response(:sign_in)
        stub_request(:post, Agero::Rsc::Client.url("/oauth/signIn"))
          .with(body: { clientId: "1" })
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: payload)
        result = api.sign_in(1)
        expect(result).to eq JSON.load(payload)
      end

      it "raises error if the endpoint returns an error code" do
        stub_request(:post, Agero::Rsc::Client.url("/oauth/signIn"))
          .to_return(error_response)
        expect { api.sign_in(1) }.to raise_error(Agero::Rsc::Error)
      end
    end

    describe "sign_out" do
      it "returns the response payload" do
        stub_request(:post, Agero::Rsc::Client.url("/oauth/signOut"))
          .with(body: { clientId: "1" })
          .to_return(empty_response)
        result = api.sign_out(1)
        expect(result).to eq nil
      end

      it "raises error if the endpoint returns an error code" do
        stub_request(:post, Agero::Rsc::Client.url("/oauth/signOut"))
          .to_return(error_response)
        expect { api.sign_out(1) }.to raise_error(Agero::Rsc::Error)
      end
    end
  end

  describe "Server to server notifications" do
    describe "subscribe_to_server_notifications" do
      it "returns the response payload" do
        payload = rsc_api_response(:subscribe_to_server_notifications)
        notification_type_ids = Agero::Rsc::Data.notification_type_ids
        stub_request(:post, Configuration.rsc_api.base_url + "/serverNotifications/subscribe")
          .with(body: { callBackUrl: "http://test.host/api/digital_dispatch/handle_rsc_event", notificationToken: "GAHH", notificationEvents: notification_type_ids.join(',') })
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: payload)
        result = api.subscribe_to_server_notifications("GAHH")
        expect(result).to eq JSON.load(payload)
      end

      it "raises error if the endpoint returns an error code" do
        stub_request(:post, Agero::Rsc::Client.url("/serverNotifications/subscribe"))
          .to_return(error_response)
        expect { api.subscribe_to_server_notifications("GAHH") }.to raise_error(Agero::Rsc::Error)
      end
    end

    describe "unsubscribe_from_server_notifications" do
      it "returns the response payload" do
        stub_request(:delete, Agero::Rsc::Client.url("/serverNotifications/subscriptions/SUBID"))
          .to_return(empty_response)
        result = api.unsubscribe_from_server_notifications("SUBID")
        expect(result).to eq nil
      end

      it "raises error if the endpoint returns an error code" do
        stub_request(:delete, Agero::Rsc::Client.url("/serverNotifications/subscriptions/SUBID"))
          .to_return(error_response)
        expect { api.unsubscribe_from_server_notifications("SUBID") }.to raise_error(Agero::Rsc::Error)
      end
    end

    describe "subscription_info" do
      it "returns the response payload" do
        payload = rsc_api_response(:subscription_info)
        stub_request(:get, Agero::Rsc::Client.url("/serverNotifications/subscriptions/SUBID"))
          .to_return(status: 200, headers: { "Content-Type" => "application/json" }, body: payload)
        result = api.subscription_info("SUBID")
        expect(result).to eq JSON.load(payload)
      end

      it "raises error if the endpoint returns an error code" do
        stub_request(:get, Agero::Rsc::Client.url("/serverNotifications/subscriptions/SUBID"))
          .to_return(error_response)
        expect { api.subscription_info("SUBID") }.to raise_error(Agero::Rsc::Error)
      end
    end
  end
end
