# frozen_string_literal: true

require 'rails_helper'

describe Agero::Rsc do
  describe '.clean_authorization_header' do
    subject(:clean_authorization_header) do
      Agero::Rsc.clean_authorization_header(authorization_header)
    end

    let(:authorization_header) { 'Bearer 123123123' }

    it { is_expected.to eq '123123123' }
  end
end
