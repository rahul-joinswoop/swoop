# frozen_string_literal: true

require "rails_helper"

describe WebpackAssets do
  let(:manifest) { { application: { js: ["/application.js"], css: ["/application.css"] }, foo: { js: ["/foo.js"], css: ["/foo.css"] } } }
  let(:mock_assets_path) { double(:path, exist?: true, read: manifest.to_json) }

  it "reads the webpack-assets.json from the public assets directory" do
    expect(WebpackAssets::WEBPACK_ASSETS_PATH).to eq Rails.root + 'public' + 'assets' + 'webpack-assets.json'
    expect_any_instance_of(WebpackAssets).to receive(:manifest_json).and_call_original
    stub_const("WebpackAssets::WEBPACK_ASSETS_PATH", mock_assets_path)
    WebpackAssets.new
  end

  it "returns the javascript from the assets" do
    stub_const("WebpackAssets::WEBPACK_ASSETS_PATH", mock_assets_path)
    expect(WebpackAssets.new.js("application")).to eq ["/application.js"]
  end

  it "returns the css from the assets" do
    stub_const("WebpackAssets::WEBPACK_ASSETS_PATH", mock_assets_path)
    expect(WebpackAssets.new.css("application")).to eq ["/application.css"]
  end
end
