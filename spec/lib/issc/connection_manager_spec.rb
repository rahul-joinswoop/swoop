# frozen_string_literal: true

require "rails_helper"

describe ISSC::ConnectionManager do
  subject do
    ISSC::ConnectionManager.new
  end

  before do
    subject._client.del('heartbeat_time')
    subject._client.del('heartbeat_sequence')
    subject.disable_issc
    subject._clear_status
  end

  it 'is dissconnected' do
    expect(subject.connected?).to eql(false)
  end

  it 'is not connecting' do
    expect(subject.connecting?).to eql(false)
  end

  context 'issc enabled' do
    before do
      subject.enable_issc
    end

    it 'Drops message when ISSC_AUTOCONNECT not set' do
      ClimateControl.modify ISSC_AUTOCONNECT: nil do
        expect(subject.connecting?).to eql(false)
        subject.ensure_connected do
          raise Exception, 'yielded unexpectedly'
        end
        expect(subject.connecting?).to eql(false)
      end
    end

    it 'Kicks off connecting when ISSC_AUTOCONNECT set' do
      ClimateControl.modify ISSC_AUTOCONNECT: 'true' do
        expect(subject.connecting?).to eql(false)

        expect_any_instance_of(IsscConnect).to receive(:perform)

        expect do
          subject.ensure_connected do
            raise Exception, 'yielded unexpectedly'
          end
        end.to raise_error(ISSC::NotConnected)

        expect(subject.connecting?).to eql(true)
      end
    end

    context 'Already Connected' do
      before do
        subject._set_status(ISSC::ConnectionManager::CONNECTED, 60)
      end

      # When no heartbeats it tries to connect
      it 'Kicks off connecting when ISSC_AUTOCONNECT set' do
        ClimateControl.modify ISSC_AUTOCONNECT: "true" do
          expect(subject.connecting?).to eql(false)

          expect_any_instance_of(IsscConnect).to receive(:perform)

          expect do
            subject.ensure_connected do
              raise Exception, 'yielded unexpectedly'
            end
          end.to raise_error(ISSC::NotConnected)

          expect(subject.connecting?).to eql(true)
        end
      end

      context 'heartbeat set' do
        before do
          subject.update_heartbeat(1, Time.now.utc)
        end

        let(:mock) { double(Object) }

        it 'yeilds' do
          expect(subject).to receive(:issc_client)
          expect(mock).to receive(:yeilded)

          subject.ensure_connected do
            mock.yeilded
          end
        end
      end
    end
  end
end
