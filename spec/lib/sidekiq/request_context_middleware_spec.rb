# frozen_string_literal: true

require "rails_helper"

describe Sidekiq::RequestContextMiddleware do
  let(:worker_class) { Object }
  let(:worker) { double(:worker) }
  let(:job) { { "jid" => "1234", "args" => [] } }
  let(:queue) { double(:queue) }

  it "does not blow up the client if there is no context" do
    called = false
    middleware = Sidekiq::RequestContextMiddleware::Client.new
    middleware.call(worker_class, job, queue) do
      called = true
    end
    expect(called).to eq true
  end

  it "does not blow up the server if there is no context" do
    called = false
    middleware = Sidekiq::RequestContextMiddleware::Server.new
    middleware.call(worker, job, queue) do
      called = true
    end
    expect(called).to eq true
  end

  it "saves the context in the client middleware and restores it for the worker on the server" do
    client_context = RequestContext.new(uuid: "abcd", user_id: 1, source_application_id: 2)
    client_middleware = Sidekiq::RequestContextMiddleware::Client.new
    RequestContext.use(client_context) do
      client_middleware.call(worker_class, job, queue) do
        true
      end
    end

    server_context = nil
    server_middleware = Sidekiq::RequestContextMiddleware::Server.new
    server_middleware.call(worker, job, queue) do
      server_context = RequestContext.current
    end

    expect(server_context.uuid).to eq "abcd"
    expect(server_context.user_id).to eq 1
    expect(server_context.source_application_id).to eq 2
  end
end
