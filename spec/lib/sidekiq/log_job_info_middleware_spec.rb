# frozen_string_literal: true

require 'rails_helper'

describe Sidekiq::LogJobInfoMiddleware do
  before do
    stub_class 'TestLoggingWorker' do
      include Sidekiq::Worker

      def perform
      end
    end

    stub_class 'TestNoArgsLoggingWorker' do
      include Sidekiq::Worker
      sidekiq_options logging: :without_args

      def perform
      end
    end

    stub_class 'TestNoLoggingWorker' do
      include Sidekiq::Worker
      sidekiq_options logging: false

      def perform
      end
    end

    allow(::Rails).to receive(:logger).and_return(logger)
  end

  let(:out) { StringIO.new }
  let(:logger) { Logger.new(out) }
  let(:job) { { "class" => worker.class.name, "args" => ["foo", 1], "retry_count" => 0 } }
  let(:middleware) { Sidekiq::LogJobInfoMiddleware.new }

  context "default logging options" do
    let(:worker) { TestLoggingWorker.new }

    it "yields" do
      yielded = false
      middleware.call(worker, job, "test") { yielded = true }
      expect(yielded).to eq true
    end

    it "logs info about a sidekiq job" do
      middleware.call(worker, job, "test") { true }
      expect(out.string).to include('Worker start: TestLoggingWorker.perform("foo", 1)')
      expect(out.string).to include('Worker finish: TestLoggingWorker.perform("foo", 1)')
    end

    it "logs retry counts" do
      job["retry_count"] = 1
      middleware.call(worker, job, "test") { true }
      expect(out.string).to include('retry count: 1')
    end

    it "logs and re-raise errors" do
      expect { middleware.call(worker, job, "test") { raise("boom") } }.to raise_error("boom")
      expect(out.string).to include('boom')
      expect(out.string).to include('Worker finish: TestLoggingWorker.perform("foo", 1)')
    end
  end

  context "with logging: :without_args" do
    let(:worker) { TestNoArgsLoggingWorker.new }

    it "yields" do
      yielded = false
      middleware.call(worker, job, "test") { yielded = true }
      expect(yielded).to eq true
    end

    it "logs only the class name" do
      middleware.call(worker, job, "test") { true }
      expect(out.string).to include('Worker start: TestNoArgsLoggingWorker.perform(---)')
      expect(out.string).to include('Worker finish: TestNoArgsLoggingWorker.perform(---)')
      expect(out.string).not_to include("foo")
    end

    it "logs and re-raise errors" do
      expect { middleware.call(worker, job, "test") { raise("boom") } }.to raise_error("boom")
      expect(out.string).to include('boom')
      expect(out.string).not_to include("foo")
    end
  end

  context "with logging: false" do
    let(:worker) { TestNoLoggingWorker.new }

    it "yields" do
      yielded = false
      middleware.call(worker, job, "test") { yielded = true }
      expect(yielded).to eq true
    end

    it "does not log anything" do
      middleware.call(worker, job, "test") { true }
      expect(out.string).to eq ""
    end

    it "re-raises errors" do
      expect { middleware.call(worker, job, "test") { raise("boom") } }.to raise_error("boom")
      expect(out.string).to eq ""
    end
  end
end
