# frozen_string_literal: true

require 'rails_helper'
require "active_support/core_ext/digest/uuid"

describe Sidekiq::WorkerUuidMiddleware do
  before do
    stub_class 'GenericWorker' do
      include Sidekiq::Worker

      def perform
        ::Rails.logger.info('perform')
      end
    end
  end

  let(:middleware) { Sidekiq::WorkerUuidMiddleware.new }

  it "adds the tag to Rails.logger" do
    uuid = Digest::UUID.uuid_v5("GenericWorker", "my_job_id")
    middleware.call(GenericWorker.new, { "jid" => "my_job_id" }, "low") do
      expect(::Rails.logger.formatter.current_tags).to include(uuid)
    end
  end
end
