# frozen_string_literal: true

require "rails_helper"

describe LogDeadLockTransactions do
  context 'when there is a deadlock issue on an ActiveRecord call' do
    let!(:user) { create(:user) }

    before do
      allow(Rollbar).to receive(:error)
      allow(Rails.logger).to receive(:error)
    end

    context 'when it is a find call' do
      before do
        allow(ActiveRecord::Base).to receive(:find).and_raise(ActiveRecord::Deadlocked)
      end

      it "logs dead lock error for a find within transaction and pass to Rollbar,LogDNA" do
        expect { User.transaction { User.find(user.id) } }.to raise_error(ActiveRecord::Deadlocked)
        expect(Rollbar).to have_received(:error).with(instance_of(String))
        expect(Rails.logger).to have_received(:error).with(instance_of(String))
      end
    end

    context 'when it is an update! call' do
      before do
        allow_any_instance_of(ActiveRecord::Base).to receive(:update!).and_raise(ActiveRecord::Deadlocked)
      end

      it "logs dead lock error for a find within transaction and pass to Rollbar,LogDNA" do
        expect { User.transaction { user.update!(first_name: 'Any') } }.to raise_error(ActiveRecord::Deadlocked)
        expect(Rollbar).to have_received(:error).with(instance_of(String))
        expect(Rails.logger).to have_received(:error).with(instance_of(String))
      end
    end

    context 'when it is an where call' do
      before do
        allow(ActiveRecord::Base).to receive(:where).and_raise(ActiveRecord::Deadlocked)
      end

      it "logs dead lock error for a find within transaction and pass to Rollbar,LogDNA" do
        expect { User.transaction { User.where(id: user.id).update(first_name: 'Deadlock') } }.to raise_error(ActiveRecord::Deadlocked)
        expect(Rollbar).to have_received(:error).with(instance_of(String))
        expect(Rails.logger).to have_received(:error).with(instance_of(String))
      end
    end
  end
end
