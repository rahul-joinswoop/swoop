# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Notifiable::JobReassigned do
  subject { Notifiable.trigger_job_reassigned job, rescue_drivers.first.id }

  let(:rescue_company) { create :rescue_company }
  let(:rescue_drivers) { create_list :user, 2, :rescue, :driver, company: rescue_company }

  shared_examples "it works" do
    it "calls Notification::JobReassignedWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::JobReassignedWorker)
        .to receive(:perform_async)
        .with(job.id, rescue_drivers.first.id)
        .once
      subject
    end
  end

  context "with a job and a rescue driver" do
    let(:job) do
      create :rescue_job,
             :with_account,
             rescue_driver: rescue_drivers.last,
             rescue_company: rescue_company
    end

    it_behaves_like "it works"
  end
end
