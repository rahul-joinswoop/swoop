# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Notifiable::JobDispatched do
  subject { Notifiable.trigger_job_dispatched job }

  shared_examples "it works" do
    it "calls Notification::JobDispatchedWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::JobDispatchedWorker)
        .to receive(:perform_async)
        .with(job.id)
        .once
      subject
    end
  end

  shared_examples "it does nothing" do
    it "doesn't call Notification::JobDispatchedWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::JobDispatchedWorker)
        .not_to receive(:perform_async)
      subject
    end
  end

  context "with a dispatched job and a driver" do
    let(:job) { create :rescue_job, :with_rescue_driver, status: :dispatched }

    it_behaves_like "it works"
  end

  context "without a driver" do
    let(:job) { create :rescue_job, status: :dispatched }

    it_behaves_like "it does nothing"
  end

  context "with an enroute job" do
    let(:job) { create :rescue_job, :with_rescue_driver, status: :enroute }

    it_behaves_like "it does nothing"
  end
end
