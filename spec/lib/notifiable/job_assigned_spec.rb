# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Notifiable::JobAssigned do
  let(:job) do
    create :fleet_motor_club_job,
           :with_account,
           :with_invoice,
           rescue_company: rescue_company,
           status: status
  end

  describe 'Notifiable.trigger_job_assigned(job)' do
    shared_examples 'schedules Notification::JobAssignedWorker' do
      it 'calls Notification::JobAssignedWorker.perform_async(job.id)' do
        expect(Notification::JobAssignedWorker).to receive(:perform_async).with(job.id)

        Notifiable.trigger_job_assigned job
      end
    end

    shared_examples 'does not schedule Notification::JobAssignedWorker' do
      it 'does not call Notification::JobAssignedWorker.perform_async' do
        expect(Notification::JobAssignedWorker).not_to receive(:perform_async)

        Notifiable.trigger_job_assigned job
      end
    end

    context 'when job.rescue_company is present' do
      let(:rescue_company) { create(:rescue_company) }

      context 'and job.status is assigned' do
        let(:status) { :assigned }

        it 'schedules Notification::JobAssignedWorker' do
          expect(Notification::JobAssignedWorker).to receive(:perform_async).with(job.id)

          Notifiable.trigger_job_assigned job
        end
      end

      context 'and job.status is not assigned' do
        let(:status) { :pending }

        it_behaves_like 'does not schedule Notification::JobAssignedWorker'
      end
    end

    context 'when job.rescue_company is not present' do
      let(:rescue_company) { nil }

      context 'and job.status is assigned' do
        let(:status) { :assigned }

        it_behaves_like 'does not schedule Notification::JobAssignedWorker'
      end

      context 'and job.status is not assigned' do
        let(:status) { :pending }

        it_behaves_like 'does not schedule Notification::JobAssignedWorker'
      end
    end
  end

  describe '.build_sms_msg(job)' do
    let(:job) { create :fleet_motor_club_job, service_code: service_code, account: account }
    let(:service_code) { create(:service_code, name: ServiceCode::BATTERY_JUMP) }
    let(:account) { create(:account, name: 'MotorClub Demo') }

    it 'builds the sms msg accordingly' do
      expect(Notifiable::JobAssigned.build_sms_msg(job)).to eq(
        "Swoop (#{job.id}): New Job • #{job.service_code.name} • #{job.account.name} • Review job details and submit an ETA."
      )
    end
  end
end
