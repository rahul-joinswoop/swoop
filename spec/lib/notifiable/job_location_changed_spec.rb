# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Notifiable::JobLocationChanged do
  subject { Notifiable.trigger_job_location_changed job }

  shared_examples "it works" do
    it "calls Notification::JobLocationChangedWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::JobLocationChangedWorker)
        .to receive(:perform_async)
        .with(job.id)
        .once
      subject
    end
  end

  shared_examples "it does nothing" do
    it "does not call Notification::JobLocationChangedWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::JobLocationChangedWorker)
        .not_to receive(:perform_async)
      subject
    end
  end

  context "with a job with a location" do
    let(:job) do
      create :rescue_job,
             :with_rescue_driver,
             :with_account,
             service_location: create(:location)
    end

    it_behaves_like "it works"

    context "without a location" do
      let(:job) do
        create :rescue_job,
               :with_rescue_driver,
               :with_account,
               service_location: nil
      end

      it_behaves_like "it does nothing"
    end
  end
end
