# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Notifiable::JobServiceTypeChanged do
  subject { Notifiable.trigger_job_service_type_changed job }

  let(:service_code) { create :service_code, name: ServiceCode::DOLLY }
  let(:user_ids) { [job.rescue_driver.to_ssid] }
  let(:title) { Notifiable::JobServiceTypeChanged.build_title(job) }
  let(:msg) { Notifiable::JobServiceTypeChanged.build_msg }
  let(:link) { Notifiable::JobServiceTypeChanged.build_link(job) }

  shared_examples "it works" do
    it "calls Notification::JobServiceTypeChangedWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::JobServiceTypeChangedWorker)
        .to receive(:perform_async)
        .with(job.id)
        .once
      subject
    end
  end

  context "with a job and a service code" do
    let(:job) do
      create :rescue_job,
             :with_account,
             :with_rescue_driver,
             service_code: service_code
    end

    it_behaves_like "it works"
  end
end
