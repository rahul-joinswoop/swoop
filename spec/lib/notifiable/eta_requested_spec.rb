# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Notifiable::EtaRequested do
  let(:rescue_company) { create :rescue_company }

  shared_examples "it works" do
    it "calls Notification::EtaRequestedWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::EtaRequestedWorker)
        .to receive(:perform_async)
        .with(job.id)
        .once
      subject
    end
  end

  shared_examples "it does nothing" do
    it "doesn't call Notification::EtaRequestedWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::EtaRequestedWorker)
        .not_to receive(:perform_async)
      subject
    end
  end

  context "with a job" do
    subject { Notifiable.trigger_eta_requested job }

    context "with an assigned job and a rescue company" do
      let(:job) do
        create :fleet_motor_club_job,
               :with_account,
               :with_invoice,
               status: :assigned,
               rescue_company: rescue_company
      end

      it_behaves_like "it works"
    end

    context "with a pending job and a rescue company" do
      let(:job) do
        create :fleet_motor_club_job,
               :with_account,
               :with_invoice,
               status: :pending,
               rescue_company: rescue_company
      end

      it_behaves_like "it does nothing"
    end

    context "with an assigned job and no rescue company" do
      let(:job) do
        create :fleet_motor_club_job,
               :with_account,
               :with_invoice,
               status: :assigned,
               rescue_company: nil
      end

      it_behaves_like "it does nothing"
    end
  end

  context "with a bid" do
    subject { Notifiable.trigger_eta_requested bid }

    let(:job) do
      create :fleet_managed_job,
             :with_auction,
             :with_account,
             :with_invoice,
             status: :assigned,
             rescue_company: rescue_company
    end

    context "with an requested bid and a rescue company" do
      let!(:bid) do
        bid = create :bid, job: job, company: rescue_company, status: Auction::Bid::REQUESTED
        job.auction.bids << bid
        bid
      end

      it "calls Notification::EtaRequestedWorker" do
        expect(Rollbar).not_to receive(:error)
        expect(Notification::EtaRequestedWorker)
          .to receive(:perform_async)
          .with(nil, bid.id)
          .once
        subject
      end
    end

    context "with a new bid and a rescue company" do
      let!(:bid) do
        bid = create :bid, job: job, company: rescue_company, status: Auction::Bid::NEW
        job.auction.bids << bid
        bid
      end

      it_behaves_like "it does nothing"
    end
  end
end
