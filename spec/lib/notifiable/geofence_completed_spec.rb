# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Notifiable::GeofenceCompleted do
  shared_examples "it works" do
    it "calls Notification::GeofenceCompletedWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::GeofenceCompletedWorker).to receive(:perform_async)
        .once
        .with(job.id, location_departed)
      subject
    end
  end

  shared_examples "it does nothing" do
    it "does not call Notification::GeofenceCompletedWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::GeofenceCompletedWorker).not_to receive(:perform_async)

      subject
    end
  end

  shared_context "has rescue driver" do
    let(:rescue_driver) { build_stubbed :user, :rescue_driver }
    let(:job) { build_stubbed :rescue_job, rescue_driver: rescue_driver }

    it_behaves_like "it works"
  end

  shared_context "missing rescue driver" do
    let(:job) { build_stubbed :rescue_job }

    it_behaves_like "it does nothing"
  end

  context 'without dropoff location' do
    subject { Notifiable.trigger_geofence_completed_without_dropoff job }

    let(:location_departed) { 'Pickup' }

    it_behaves_like 'has rescue driver'
    it_behaves_like 'missing rescue driver'
  end

  context 'with dropoff location' do
    subject { Notifiable.trigger_geofence_completed_with_dropoff job }

    let(:location_departed) { 'Drop Off' }

    it_behaves_like 'has rescue driver'
    it_behaves_like 'missing rescue driver'
  end

  context '#trigger_geofence_complete' do
    let(:rescue_driver) { build_stubbed :user, :rescue_driver }
    let(:job) { build_stubbed :rescue_job, rescue_driver: rescue_driver }

    before do
      allow(Notification::GeofenceCompletedWorker).to receive(:perform_async)
    end

    it 'accepts Pickup' do
      expect(Notification::GeofenceCompletedWorker).to receive(:perform_async).with(job.id, 'Pickup')
      Notifiable.send(:trigger_geofence_completed, job, 'Pickup')
    end

    it 'accepts Drop Off' do
      expect(Notification::GeofenceCompletedWorker).to receive(:perform_async).with(job.id, 'Drop Off')
      Notifiable.send(:trigger_geofence_completed, job, 'Drop Off')
    end

    it 'fails otherwise' do
      expect { Notifiable.send(:trigger_geofence_completed, job, 'Crash and Burn') }
        .to raise_error(ArgumentError)
    end
  end
end
