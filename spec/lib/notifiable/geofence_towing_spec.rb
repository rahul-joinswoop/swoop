# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Notifiable::GeofenceTowing do
  subject { Notifiable.trigger_geofence_towing job }

  shared_examples "it works" do
    it "calls Notification::GeofenceTowingWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::GeofenceTowingWorker)
        .to receive(:perform_async)
        .with(job.id)
        .once
      subject
    end
  end

  shared_examples "it does nothing" do
    it "does not call Notification::GeofenceTowingWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::GeofenceTowingWorker)
        .not_to receive(:perform_async)
      subject
    end
  end

  context "with a job that has rescue driver" do
    let(:job) do
      create :rescue_job,
             :with_rescue_driver,
             :with_account,
             service_location: create(:location)
    end

    it_behaves_like "it works"

    context "without a driver" do
      before do
        job.update_attributes!(rescue_driver_id: nil)
      end

      it_behaves_like "it does nothing"
    end
  end
end
