# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Notifiable::JobCanceled do
  subject { Notifiable.trigger_job_canceled job }

  let(:rescue_company) { create :rescue_company }
  let(:rescue_driver) { create :user, :rescue, :driver }

  shared_examples "it works" do
    it "calls Notification::JobCanceledWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::JobCanceledWorker)
        .to receive(:perform_async)
        .with(job.id)
        .once
      subject
    end
  end

  shared_examples "it does nothing" do
    it "does not call Notification::JobCanceledWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::JobCanceledWorker)
        .not_to receive(:perform_async)
      subject
    end
  end

  context "with a canceled job" do
    let(:job) do
      create :rescue_job,
             :with_account,
             rescue_driver: rescue_driver,
             rescue_company: rescue_company,
             status: :canceled
    end

    it_behaves_like "it works"
  end

  context "with an enroute job" do
    let(:job) do
      create :rescue_job,
             :with_account,
             rescue_driver: rescue_driver,
             rescue_company: rescue_company,
             status: :enroute
    end

    it_behaves_like "it does nothing"
  end
end
