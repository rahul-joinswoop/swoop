# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Notifiable::EtaAccepted do
  let(:rescue_company) { create :rescue_company }

  shared_examples "it works" do
    it "calls Notification::EtaAcceptedWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::EtaAcceptedWorker)
        .to receive(:perform_async)
        .with(job.id)
        .once
      subject
    end
  end

  shared_examples "it does nothing" do
    it "doesn't call Notification::EtaAcceptedWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::EtaAcceptedWorker)
        .not_to receive(:perform_async)
      subject
    end
  end

  context "with a job" do
    subject { Notifiable.trigger_eta_accepted job }

    context "with an accepted job and a rescue company" do
      let(:job) do
        create :fleet_motor_club_job,
               :with_account,
               :with_invoice,
               status: :accepted,
               rescue_company: rescue_company
      end

      it_behaves_like "it works"
    end

    context "with a pending job and a rescue company" do
      let(:job) do
        create :fleet_motor_club_job,
               :with_account,
               :with_invoice,
               status: :pending,
               rescue_company: rescue_company
      end

      it_behaves_like "it does nothing"
    end
  end

  context "with a bid" do
    subject { Notifiable.trigger_eta_accepted bid }

    let(:job) do
      create :fleet_managed_job,
             :with_auction,
             :with_account,
             :with_invoice,
             status: :assigned,
             rescue_company: rescue_company
    end

    context "with an winning bid and a rescue company" do
      let!(:bid) do
        bid = create :bid, job: job, company: rescue_company, status: Auction::Bid::WON
        job.auction.bids << bid
        bid
      end

      it_behaves_like "it works"
    end

    context "with a new bid and a rescue company" do
      let!(:bid) do
        bid = create :bid, job: job, company: rescue_company, status: Auction::Bid::NEW
        job.auction.bids << bid
        bid
      end

      it_behaves_like "it does nothing"
    end
  end
end
