# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Notifiable::FleetInHouseJobAssigned do
  subject { Notifiable.trigger_fleet_in_house_job_assigned job }

  let(:rescue_company) { create :rescue_company }

  shared_examples "it works" do
    it "calls Notification::FleetInHouseJobAssignedWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::FleetInHouseJobAssignedWorker)
        .to receive(:perform_async)
        .with(job.id)
        .once
      subject
    end
  end

  shared_examples "it does nothing" do
    it "doesn't call Notification::FleetInHouseJobAssignedWorker" do
      expect(Rollbar).not_to receive(:error)
      expect(Notification::FleetInHouseJobAssignedWorker)
        .not_to receive(:perform_async)
      subject
    end
  end

  context "with an assigned job and a rescue company" do
    let(:job) do
      create :fleet_in_house_job,
             :with_account,
             status: :assigned,
             rescue_company: rescue_company
    end

    it_behaves_like "it works"
  end

  context "with a pending job and a rescue company" do
    let(:job) do
      create :fleet_in_house_job,
             :with_account,
             status: :pending,
             rescue_company: rescue_company
    end

    it_behaves_like "it does nothing"
  end

  context "with an assigned job and no rescue company" do
    let(:job) do
      create :fleet_in_house_job,
             :with_account,
             status: :assigned,
             rescue_company: nil
    end

    it_behaves_like "it does nothing"
  end
end
