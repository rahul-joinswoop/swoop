# frozen_string_literal: true

require "rails_helper"

describe SwoopAuthenticator do
  let(:request) { ActionDispatch::TestRequest.create }

  describe "for(request)" do
    it "gets the same authenticator from a request" do
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator).to be_a(SwoopAuthenticator)
      expect(SwoopAuthenticator.for(request).object_id).to eq authenticator.object_id
    end

    it "can clear the memoized authenticator" do
      authenticator = SwoopAuthenticator.for(request)
      SwoopAuthenticator.clear(request)
      authenticator_2 = SwoopAuthenticator.for(request)
      expect(authenticator_2).to be_a(SwoopAuthenticator)
      expect(authenticator_2.object_id).not_to eq authenticator.object_id
    end
  end

  describe "from_context" do
    it "loads the authenticator from an access token on a GraphQL context" do
      company = create(:rescue_company)
      user = create(:user, company: company)
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: user.id)
      context = { api_access_token: access_token }
      authenticator = SwoopAuthenticator.from_context(context)
      expect(authenticator.access_token).to eq access_token
      expect(authenticator.user).to eq user
      expect(authenticator.company).to eq company
    end

    it "can set the user and company from the GraphQL context" do
      company = create(:rescue_company)
      user = create(:user)
      access_token = Doorkeeper::AccessToken.create!
      context = { api_access_token: access_token, api_company: company, api_user: user }
      authenticator = SwoopAuthenticator.from_context(context)
      expect(authenticator.access_token).to eq access_token
      expect(authenticator.user).to eq user
      expect(authenticator.company).to eq company
    end
  end

  describe "#scope_roles" do
    subject { SwoopAuthenticator.scope_roles(scopes, company) }

    context 'with a customer' do
      let(:scopes) { [SwoopAuthenticator::CUSTOMER_ROLE] }
      let(:company) { nil }

      it 'works' do
        expect(subject).to eq([SwoopAuthenticator::CUSTOMER_ROLE])
      end
    end

    context 'with SUPER_FLEET_MANAGED_ROLE' do
      let(:scopes) { [SwoopAuthenticator::SUPER_FLEET_MANAGED_ROLE] }
      let(:company) { nil }

      it 'works' do
        expect(subject).to eq(SwoopAuthenticator::ALL_SUPER_FLEET_MANAGED_ROLES)
      end
    end

    context 'with FLEET_ROLE' do
      let(:scopes) { [SwoopAuthenticator::FLEET_ROLE] }
      let(:company) { nil }

      it 'works' do
        expect(subject).to eq(SwoopAuthenticator::ALL_FLEET_ROLES)
      end
    end

    context 'with RESCUE_ROLE' do
      let(:scopes) { [SwoopAuthenticator::RESCUE_ROLE] }
      let(:company) { nil }

      it 'works' do
        expect(subject).to eq(SwoopAuthenticator::ALL_RESCUE_ROLES)
      end
    end
  end

  describe "authenticate_user" do
    let!(:user) { create(:user, username: "joe", email: "joe@example.com", password: "abcd1234") }

    describe "with password" do
      it "authenticates a user by email and password" do
        request.params["email"] = "joe@example.com"
        request.params["password"] = "abcd1234"
        expect(SwoopAuthenticator.authenticate_user(request)).to eq user
      end

      it "authenticates a user by username and password" do
        request.params["username"] = "joe"
        request.params["password"] = "abcd1234"
        expect(SwoopAuthenticator.authenticate_user(request)).to eq user
      end

      it "returns nil if the email is not found" do
        request.params["email"] = "jane@example.com"
        request.params["password"] = "abcd1234"
        expect(SwoopAuthenticator.authenticate_user(request)).to eq nil
      end

      it "returns nil if the username is not found" do
        request.params["username"] = "jane"
        request.params["password"] = "abcd1234"
        expect(SwoopAuthenticator.authenticate_user(request)).to eq nil
      end

      it "returns nil if the password is not correct" do
        request.params["email"] = "joe@example.com"
        request.params["password"] = "not the password"
        expect(SwoopAuthenticator.authenticate_user(request)).to eq nil
      end

      it "return nil if the password is not set" do
        create(:user, username: "jane", email: "jane@example.com", password: nil)
        request.params["username"] = "jane"
        request.params["password"] = nil
        expect(SwoopAuthenticator.authenticate_user(request)).to eq nil
      end
    end

    describe "with single user token" do
      it "authenticates a user with a valid token and marks the token as used" do
        token = SingleUseAccessToken.generate(user)
        token.save!
        request.params["token"] = token.jwt
        expect(SwoopAuthenticator.authenticate_user(request)).to eq user
        token.reload
        expect(token.used_at).to be_present
      end

      it "return nil if the token is not found" do
        request.params["token"] = "nosuchtoken"
        expect(SwoopAuthenticator.authenticate_user(request)).to eq nil
      end

      it "return nil if the token is expired" do
        token = nil
        Timecop.travel(1.week.ago) do
          token = SingleUseAccessToken.generate(user)
          token.save!
        end
        request.params["token"] = token.jwt
        expect(SwoopAuthenticator.authenticate_user(request)).to eq nil
      end

      it "return nil if the token has already been used" do
        token = SingleUseAccessToken.generate(user)
        token.used_at = Time.current
        token.save!
        request.params["token"] = token.jwt
        expect(SwoopAuthenticator.authenticate_user(request)).to eq nil
      end
    end
  end

  describe "access_token" do
    it "returns nil if there is no access token on the request" do
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.access_token).to eq nil
      expect(authenticator.authenticated?).to eq false
    end

    it "gets an access token from the request" do
      access_token = Doorkeeper::AccessToken.create!
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.access_token).to eq access_token
      expect(authenticator.authenticated?).to eq true
    end

    it "does not honor an expired token" do
      access_token = Doorkeeper::AccessToken.create!(expires_in: 30, created_at: 2.hours.ago)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.access_token).to eq nil
      expect(authenticator.authenticated?).to eq false
    end

    it "does not honor a revoked token" do
      access_token = Doorkeeper::AccessToken.create!(revoked_at: Time.current)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.access_token).to eq nil
      expect(authenticator.authenticated?).to eq false
    end
  end

  describe "user" do
    it "returns nil if there is no access token" do
      create(:user)
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.user).to eq nil
    end

    it "looks up the user associated with the access token" do
      user = create(:user)
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: user.id)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.user).to eq user
    end

    it "returns nil on an application level token" do
      create(:rescue_company)
      access_token = Doorkeeper::AccessToken.create!(scopes: "fleet")
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.user).to eq nil
    end

    it "returns nil on company scoped access token" do
      company = create(:rescue_company)
      create(:user, company: company, roles: "admin")
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.user).to eq nil
    end

    it "looks up the user from the X-Authenticated-User header for company scoped tokens using an ssid" do
      company = create(:rescue_company)
      user_1 = create(:user, company: company, roles: "admin")
      user_2 = create(:user, company: company)
      access_token = Doorkeeper::AccessToken.create!(scopes: "company", resource_owner_id: user_1.id)
      request.params[:access_token] = access_token.token
      request.env["HTTP_X_AUTHENTICATED_USER"] = user_2.to_ssid
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.user).to eq user_2
    end

    it "validates that the user from the X-Authenticated-User header belongs to the company" do
      company = create(:rescue_company)
      user_1 = create(:user, company: company, roles: "admin")
      user_2 = create(:user, company: create(:rescue_company))
      access_token = Doorkeeper::AccessToken.create!(scopes: "company", resource_owner_id: user_1.id)
      request.params[:access_token] = access_token.token
      request.env["HTTP_X_AUTHENTICATED_USER"] = user_2.to_ssid
      authenticator = SwoopAuthenticator.for(request)
      expect { authenticator.user }.to raise_error(SwoopAuthenticator::InvalidTokenError)
    end

    it "validates that company scoped tokens were created by an admin" do
      company = create(:rescue_company)
      user_1 = create(:user, company: company)
      user_2 = create(:user, company: company)
      access_token = Doorkeeper::AccessToken.create!(scopes: "company", resource_owner_id: user_1.id)
      request.params[:access_token] = access_token.token
      request.env["HTTP_X_AUTHENTICATED_USER"] = user_2.to_ssid
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.user).to eq nil
    end

    it "validates that company scoped tokens were created by an account that still exists" do
      company = create(:rescue_company)
      user_1 = create(:user, company: company, deleted_at: Time.now, roles: "admin")
      user_2 = create(:user, company: company)
      access_token = Doorkeeper::AccessToken.create!(scopes: "company", resource_owner_id: user_1.id)
      request.params[:access_token] = access_token.token
      request.env["HTTP_X_AUTHENTICATED_USER"] = user_2.to_ssid
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.user).to eq nil
    end
  end

  describe "company" do
    it "returns nil if there is no access token" do
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.user).to eq nil
    end

    it "returns the user's company if there is a user" do
      company = create(:company)
      user = create(:user, company: company)
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: user.id)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.company).to eq user.company
    end

    it "looks up the company associated with the application if there is no user" do
      company = create(:fleet_company)
      application = create(:oauth_application, owner: company, scopes: "fleet")
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: nil, application: application)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.company).to eq company
    end

    it "looks up the company associated with the user who created the token on company scope tokens" do
      company = create(:company)
      user = create(:user, company: company, roles: "admin")
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: user.id, scopes: "company")
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.company).to eq company
    end

    it "validates that company scoped tokens are created by admins" do
      company = create(:company)
      user = create(:user, company: company, roles: "dispatcher")
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: user.id, scopes: "company")
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.company).to eq nil
    end

    it "validates that company scoped tokens are not created by deleted accounts" do
      company = create(:company)
      user = create(:user, company: company, roles: "admin", deleted_at: Time.now)
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: user.id, scopes: "company")
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.company).to eq nil
    end
  end

  describe "application" do
    it "returns nil when there is no token" do
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.application).to be_nil
    end

    it "returns nothing on a user token" do
      company = create(:rescue_company)
      user = create(:user, company: company, roles: "admin")
      access_token = Doorkeeper::AccessToken.create!(scopes: "rescue", resource_owner_id: user.id)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.application).to be_nil
    end

    it "returns the application on a company scoped token" do
      application = Doorkeeper::Application.create!(name: "Test Application", redirect_uri: "https://example.com/")
      company = create(:rescue_company)
      user = create(:user, company: company, roles: "admin")
      access_token = Doorkeeper::AccessToken.create!(scopes: "company", resource_owner_id: user.id, application_id: application.id)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.application).to eq application
    end

    it "returns the aplication on an application level token" do
      company = create(:fleet_company)
      application = Doorkeeper::Application.create!(name: "Test Application", owner: company, redirect_uri: "https://example.com/")
      create(:user, company: company, roles: "admin")
      access_token = Doorkeeper::AccessToken.create!(scopes: "fleet", resource_owner_id: nil, application_id: application.id)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.application).to eq application
    end
  end

  describe "viewer" do
    it "returns nil when there is no token" do
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.viewer).to eq nil
    end

    it "returns the user on a user token" do
      company = create(:rescue_company)
      user = create(:user, company: company, roles: "admin")
      access_token = Doorkeeper::AccessToken.create!(scopes: "rescue", resource_owner_id: user.id)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.viewer).to eq user
    end

    it "returns the company on a company scoped token" do
      application = Doorkeeper::Application.create!(name: "Test Application", redirect_uri: "https://example.com/")
      company = create(:rescue_company)
      user = create(:user, company: company, roles: "admin")
      access_token = Doorkeeper::AccessToken.create!(scopes: "company", resource_owner_id: user.id, application_id: application.id)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.viewer).to eq company
    end

    it "returns the aplication on an application level token" do
      company = create(:fleet_company)
      application = Doorkeeper::Application.create!(name: "Test Application", owner: company, redirect_uri: "https://example.com/")
      create(:user, company: company, roles: "admin")
      access_token = Doorkeeper::AccessToken.create!(scopes: "fleet", resource_owner_id: nil, application_id: application.id)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.viewer).to eq application
    end
  end

  describe "roles" do
    it "returns an empty array if there is no access token" do
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.roles).to eq []
    end

    it "returns a list of the user's role names if there is a user" do
      user = create(:user, roles: ["rescue", "dispatcher"])
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: user.id)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.roles).to match_array ["rescue", "dispatcher"]
    end

    it "returns roles from the access token scope if there is no user" do
      company = create(:fleet_company)
      application = create(:oauth_application, owner: company, scopes: "")
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: nil, application: application, scopes: "fleet")
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.roles).to match_array described_class::ALL_FLEET_ROLES
    end

    it "returns roles from the application scope if there is no user and not token scopes" do
      company = create(:fleet_company)
      application = create(:oauth_application, owner: company, scopes: "fleet")
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: nil, application: application)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.roles).to match_array described_class::ALL_FLEET_ROLES
    end

    it "returns all roles for a fleet company if the scope is just company" do
      company = create(:fleet_company)
      user = create(:user, company: company, roles: "admin")
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: user.id, scopes: "company")
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.roles).to match_array described_class::ALL_FLEET_ROLES
    end

    it "returns only fleet roles for a super fleet managed company if the scope is just company" do
      company = create(:fleet_company, :agero)
      user = create(:user, company: company, roles: "admin")
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: user.id, scopes: "company")
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.roles).to match_array described_class::ALL_FLEET_ROLES
    end

    it "returns all roles for a partner company if the scope is just company" do
      company = create(:rescue_company)
      user = create(:user, company: company, roles: "admin")
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: user.id, scopes: "company")
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.roles).to match_array described_class::ALL_RESCUE_ROLES
    end

    it "adds the appropriate company role if the scope includes company plus others" do
      company = create(:rescue_company)
      user = create(:user, company: company, roles: "admin")
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: user.id, scopes: "company dispatcher")
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.roles).to match_array ["rescue", "dispatcher"]
    end
  end

  describe "has_role?" do
    it "returns true if the roles contain the required role" do
      user = create(:user, roles: ["rescue", "dispatcher"])
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: user.id)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.roles).to match_array ["rescue", "dispatcher"]
      expect(authenticator.has_role?("dispatcher")).to eq true
      expect(authenticator.has_role?("admin")).to eq false
    end

    it "return false if there are no roles" do
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.has_role?("dispatcher")).to eq false
    end
  end

  describe "has_any_role?" do
    it "returns true if the roles contain any of the required roles" do
      user = create(:user, roles: ["rescue", "dispatcher"])
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: user.id)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.roles).to match_array ["rescue", "dispatcher"]
      expect(authenticator.has_any_role?("dispatcher")).to eq true
      expect(authenticator.has_any_role?("admin", "dispatcher")).to eq true
      expect(authenticator.has_any_role?("admin", "fleet")).to eq false
    end

    it "return false if there are no roles" do
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.has_role?("dispatcher")).to eq false
    end
  end

  describe "has_all_roles?" do
    it "returns true if the roles contains all of the required roles" do
      user = create(:user, roles: ["rescue", "dispatcher"])
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: user.id)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.roles).to match_array ["rescue", "dispatcher"]
      expect(authenticator.has_all_roles?("dispatcher")).to eq true
      expect(authenticator.has_all_roles?("fleet", "dispatcher")).to eq false
      expect(authenticator.has_all_roles?("rescue", "dispatcher")).to eq true
    end

    it "return false if there are no roles" do
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.has_all_roles?("dispatcher")).to eq false
    end
  end

  describe "application type" do
    it "is not a type of application if the access token doesn't have an application" do
      user = create(:user, roles: ["rescue", "dispatcher"])
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: user.id, scopes: ["fleet", "rescue"])
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.partner_application?).to eq false
      expect(authenticator.client_application?).to eq false
      expect(authenticator.super_client_managed_application?).to eq false
    end

    it "is a partner application if the access_token application includes the rescue scope" do
      company = create(:fleet_company)
      application = create(:oauth_application, owner: company, scopes: :rescue)
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: nil, application: application)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.partner_application?).to eq true
      expect(authenticator.client_application?).to eq false
      expect(authenticator.super_client_managed_application?).to eq false
    end

    it "is a client application if the access_token application includes the fleet scope" do
      company = create(:fleet_company)
      application = create(:oauth_application, owner: company, scopes: :fleet)
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: nil, application: application)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.partner_application?).to eq false
      expect(authenticator.client_application?).to eq true
      expect(authenticator.super_client_managed_application?).to eq false
    end

    it "is a super client application if the access_token application includes the super_fleet_managed scope" do
      company = create(:fleet_company, :agero)
      application = create(:oauth_application, owner: company, scopes: :super_fleet_managed)
      access_token = Doorkeeper::AccessToken.create!(resource_owner_id: nil, application: application)
      request.params[:access_token] = access_token.token
      authenticator = SwoopAuthenticator.for(request)
      expect(authenticator.partner_application?).to eq false
      expect(authenticator.client_application?).to eq false
      expect(authenticator.super_client_managed_application?).to eq true
    end
  end
end
