# frozen_string_literal: true

require "rails_helper"

describe AfterCommit do
  it "runs inline if there is no open database transaction" do
    vals = []
    vals << 1
    AfterCommit.call { vals << 2 }
    vals << 3
    expect(vals).to eq [1, 2, 3]
  end

  it "runs blocks only after open database transactions are committed" do
    vals = []
    ApplicationRecord.transaction do
      vals << 1
      AfterCommit.call { vals << 2 }
      ApplicationRecord.transaction do
        AfterCommit.call { vals << 3 }
      end
      vals << 4
    end
    expect(vals).to eq [1, 4, 2, 3]
  end
end
