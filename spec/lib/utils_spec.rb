# frozen_string_literal: true

require 'rails_helper'

describe Utils do
  before do
    stub_class 'Foo' do
      include Utils
    end
  end

  describe '#format_us_phone_number' do
    subject(:format_us_phone_number) { Foo.new.format_us_phone_number(phone_number) }

    let(:phone_number) { '+15558880000' }

    it { is_expected.to eq '5558880000' }
  end

  describe '#human_localtime' do
    subject(:human_localtime) { Foo.new.human_localtime(datetime, 'UTC').to_s }

    let(:datetime) { Date.strptime('1536681600.0', '%s') }

    it { is_expected.to eq '09/11/2018 00:00 (UTC)' }
  end

  describe "#email_valid?" do
    subject { Foo.new }

    it "returns `true`" do
      expect(subject.email_valid?("test@test.com")).to eq(true)
      expect(subject.email_valid?("sameer+invoices@joinswoop.com")).to eq(true)
      expect(subject.email_valid?("invoices@fake.com")).to eq(true)
      expect(subject.email_valid?(" invoices@fake.com")).to eq(true)
      expect(subject.email_valid?("Swoop <operations@joinswoop.com>")).to eq(true)
      expect(subject.email_valid?("<operations@joinswoop.com>")).to eq(true)
    end

    it "returns `false`" do
      expect(subject.email_valid?("test@test")).to eq(false)
      expect(subject.email_valid?("Swoop <operations@joinswoop.com")).to eq(false)
    end
  end
end
