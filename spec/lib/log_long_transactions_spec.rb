# frozen_string_literal: true

require "rails_helper"

describe LogLongTransactions do
  it "does nothing if a transaction is short" do
    save_val = LogLongTransactions.max_transaction_seconds
    begin
      LogLongTransactions.max_transaction_seconds = 1.0
      expect(Rollbar).not_to receive(:warn)
      ApplicationRecord.transaction { true }
    ensure
      LogLongTransactions.max_transaction_seconds = save_val
    end
  end

  it "logs a transaction to Rollbar if it exceeds a threshold" do
    save_val = LogLongTransactions.max_transaction_seconds
    begin
      LogLongTransactions.max_transaction_seconds = 0.001
      # Created a new module LogDeadLockTransactions in that TransactionManager is Prepending it.
      # So here updated with LogDeadLockTransactions Module to receive open_transactions
      allow_any_instance_of(LogDeadLockTransactions).to receive(:open_transactions).and_return(0)
      expect(Rollbar).to receive(:warn).with(instance_of(LogLongTransactions::Warning))
      ActiveRecord::Base.transaction { sleep(0.002) }
    ensure
      LogLongTransactions.max_transaction_seconds = save_val
    end
  end
end
