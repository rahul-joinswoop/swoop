# frozen_string_literal: true

require 'rails_helper'

describe AgeroApi do
  before do
    allow(Rollbar).to receive(:error)
  end

  describe "post" do
    let(:json) { { test: "value" }.to_json }

    it "posts to Agero's API and return the response" do
      stub_request(:post, "#{AgeroApi::SERVICE_URL}/foo").with(
        headers: {
          "Host" => URI.parse(AgeroApi::SERVICE_URL).host,
          "Content-Type" => "application/json; charset=utf-8",
          "Authorization" => /.+/,
          "x-amz-date" => /.+/,
          "x-amz-content-sha256" => /.+/,
        },
        body: json,
      ).to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: '{"foo": "bar"}')
      response = AgeroApi.post(resource: "foo", json: json)
      expect(response.code).to eq 200
      expect(response.body).to eq '{"foo": "bar"}'
      expect(Rollbar).not_to have_received(:error)
    end

    it "returns error responses" do
      stub_request(:post, "#{AgeroApi::SERVICE_URL}/foo").with(
        headers: {
          "Host" => URI.parse(AgeroApi::SERVICE_URL).host,
          "Content-Type" => "application/json; charset=utf-8",
          "Authorization" => /.+/,
          "x-amz-date" => /.+/,
          "x-amz-content-sha256" => /.+/,
        },
        body: json,
      ).to_return(status: 503, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: '{"error": "Server busy"}')
      response = AgeroApi.post(resource: "foo", json: json)
      expect(response.code).to eq 503
      expect(response.body).to eq '{"error": "Server busy"}'
      expect(Rollbar).to have_received(:error)
    end

    it "returns a mock response for exceptions" do
      expect(RestClient::Request).to receive(:new).and_raise(RuntimeError.new("boom"))
      response = AgeroApi.post(resource: "foo", json: json)
      expect(response.code).to eq 500
      expect(response.body).to eq 'RuntimeError: boom'
      expect(Rollbar).to have_received(:error)
    end
  end

  describe "get" do
    it "posts to Agero's API and return the response" do
      stub_request(:get, "#{AgeroApi::SERVICE_URL}/foo").with(
        headers: {
          "Host" => URI.parse(AgeroApi::SERVICE_URL).host,
          "Content-Type" => "application/json; charset=utf-8",
          "Authorization" => /.+/,
          "x-amz-date" => /.+/,
          "x-amz-content-sha256" => /.+/,
        },
        query: { "thing" => "one" },
      ).to_return(status: 200, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: '{"foo": "bar"}')
      response = AgeroApi.get(resource: "foo", query_string_hash: { thing: "one" })
      expect(response.code).to eq 200
      expect(response.body).to eq '{"foo": "bar"}'
      expect(Rollbar).not_to have_received(:error)
    end

    it "returns error responses" do
      stub_request(:get, "#{AgeroApi::SERVICE_URL}/foo").with(
        headers: {
          "Host" => URI.parse(AgeroApi::SERVICE_URL).host,
          "Content-Type" => "application/json; charset=utf-8",
          "Authorization" => /.+/,
          "x-amz-date" => /.+/,
          "x-amz-content-sha256" => /.+/,
        },
        query: { "thing" => "one" },
      ).to_return(status: 503, headers: { "Content-Type" => "application/json; charset=utf-8" }, body: '{"error": "Server busy"}')
      response = AgeroApi.get(resource: "foo", query_string_hash: { thing: "one" })
      expect(response.code).to eq 503
      expect(response.body).to eq '{"error": "Server busy"}'
      expect(Rollbar).to have_received(:error)
    end

    it "returns a mock response for exceptions" do
      expect(RestClient::Request).to receive(:new).and_raise(RuntimeError.new("boom"))
      response = AgeroApi.get(resource: "foo", query_string_hash: { thing: "one" })
      expect(response.code).to eq 500
      expect(response.body).to eq 'RuntimeError: boom'
      expect(Rollbar).to have_received(:error)
    end
  end
end
