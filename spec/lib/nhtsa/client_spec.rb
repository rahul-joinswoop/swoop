# frozen_string_literal: true

require 'rails_helper'

describe NHTSA::Client, :vcr do
  subject(:client) { ::NHTSA::Client.new }

  describe "#get_manufacturer_makes_by(manufacturer, year)" do
    it "returns rows of make data" do
      expect(client.get_manufacturer_makes_by('tesla', 2016)['Count']).to eq 1
    end
  end

  describe "#get_all_makes" do
    it "returns rows of make data" do
      expect(client.get_all_makes['Count']).to eq 8990
    end
  end

  describe "#get_models_for_make(make)" do
    it "returns rows of make data" do
      expect(client.get_models_for_make('Harley Davidson')['Count']).to eq 397
    end
  end

  describe "#get_models_for_make_and_year(make, year)" do
    it "returns rows of make and year data" do
      expect(client.get_models_for_make_and_year('Harley Davidson', 2005)['Count']).to eq 37
    end
  end
end
