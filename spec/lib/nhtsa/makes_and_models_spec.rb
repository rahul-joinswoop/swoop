# frozen_string_literal: true

require 'rails_helper'

describe ::NHTSA::MakesAndModels do
  subject(:makes_models) { ::NHTSA::MakesAndModels.new(persist: true) }

  let!(:vehicle_make) do
    create(:vehicle_make, name: 'Honda').tap do |m|
      m.vehicle_models << create(:vehicle_model, name: "Accord", vehicle_make: m)
    end
  end
  let(:model_results) do
    {
      "Count": 3, "Message": "Results returned successfully", "SearchCriteria": "Make:honda | ModelYear:2015", "Results":
        [
          { "Make_ID": 474, "Make_Name": "Honda", "Model_ID": 1861, "Model_Name": "Accord" }.stringify_keys,
          { "Make_ID": 474, "Make_Name": "Honda", "Model_ID": 1863, "Model_Name": "Civic" }.stringify_keys,
          { "Make_ID": 474, "Make_Name": "Honda", "Model_ID": 1864, "Model_Name": "Pilot" }.stringify_keys,
        ],
    }.stringify_keys
  end
  let(:client) { double(get_models_for_make_and_year: model_results) }

  before do
    stub_const('NHTSA::Client', double(:new))
    allow(NHTSA::Client).to receive(:new) { client }
  end

  describe "#update!" do
    context "when update is called" do
      before do
        makes_models.update!
      end

      it "adds 2 new models for Honda make" do
        expect(VehicleModel.count).to eq 3
        expect(vehicle_make.vehicle_models.count).to eq 3
      end

      it "adds the 5 years" do
        expect(VehicleModel.last.years.count).to eq 5
      end
    end
  end
end
