# frozen_string_literal: true

require 'rails_helper'

describe LoggableJob do
  before do
    stub_class 'TestClass' do
      include LoggableJob

      def log_with_job_id
        log :warn, "Yeiyy", job: 1
      end

      def log_with_review_id
        log :warn, "Yeiyy", review: 1
      end

      def log_with_job_and_review_id
        log :warn, "Yeiyy", job: 1, review: 1
      end

      def log_without_job_and_review_id
        log :warn, "Yeiyy"
      end
    end
    allow(Rails).to receive(:logger).and_return(logger_double)
  end

  let(:instance) { TestClass.new }
  let(:logger_double) { double("Logger") }

  context 'with_job_id' do
    subject { instance.log_with_job_id }

    it 'includes JOB(<jobid>) as prefix in the log message' do
      expect(logger_double).to receive(:send).with(:warn, "JOB(1) Origin:TestClass Message:Yeiyy")
      subject
    end
  end

  context 'with_review_id' do
    subject { instance.log_with_review_id }

    it 'includes REVIEW(<review_id>) as prefix in the log message' do
      expect(logger_double).to receive(:send).with(:warn, "REVIEW(1) Origin:TestClass Message:Yeiyy")
      subject
    end
  end

  context 'with_job_and_review_id' do
    subject { instance.log_with_job_and_review_id }

    it 'includes both JOB(<job_id>) and REVIEW(<review_id>) as prefix in the log message' do
      expect(logger_double).to receive(:send).with(:warn, "JOB(1) REVIEW(1) Origin:TestClass Message:Yeiyy")
      subject
    end
  end

  context 'without anything' do
    subject { instance.log_without_job_and_review_id }

    it 'includes just the log message' do
      expect(logger_double).to receive(:send).with(:warn, "Origin:TestClass Message:Yeiyy")
      subject
    end
  end
end
