# frozen_string_literal: true

require "rails_helper"

describe RequestContext do
  describe "empty?" do
    it "is empty if all the fields are blank" do
      expect(RequestContext.new.empty?).to eq true
      expect(RequestContext.new(uuid: "abcd").empty?).to eq false
      expect(RequestContext.new(user_id: 1).empty?).to eq false
      expect(RequestContext.new(source_application_id: 2).empty?).to eq false
    end
  end

  describe "user" do
    it "has a user" do
      request_context = RequestContext.new
      expect(request_context.user).to eq nil
      user = create(:user)
      request_context.user_id = user.id
      expect(request_context.user).to eq user
    end
  end

  describe "source_application" do
    it "has a source application" do
      request_context = RequestContext.new
      expect(request_context.source_application).to eq nil
      application = SourceApplication.create!(platform: SourceApplication::PLATFORM_WEB, source: "Swoop")
      request_context.source_application_id = application.id
      expect(request_context.source_application).to eq application
    end
  end

  describe "use" do
    it "returns an empty context if there is none in use" do
      request_context = RequestContext.current
      expect(request_context).not_to eq nil
      expect(request_context.empty?).to eq true
    end

    it "returns the current context for a block" do
      context_1 = RequestContext.new(uuid: "a")
      context_2 = RequestContext.new(uuid: "b")
      RequestContext.use(context_1) do
        expect(RequestContext.current).to eq context_1
        RequestContext.use(context_2) do
          expect(RequestContext.current).to eq context_2
        end
        expect(RequestContext.current).to eq context_1
        RequestContext.use(nil) do
          expect(RequestContext.current.empty?).to eq true
        end
        expect(RequestContext.current).to eq context_1
      end
    end
  end
end
