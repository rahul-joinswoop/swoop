# frozen_string_literal: true

require 'rails_helper'

describe SftpServer do
  let(:mock_sftp) { double(:sftp) }

  before(:each) do
    allow(Net::SFTP).to receive(:start).and_yield(mock_sftp)
  end

  describe "connect_to_server" do
    it "authenticates with a password from the URI" do
      data_source = SftpServer.new("sftp://user:foo@ftp.example.com:1522/")
      expect(Net::SFTP).to receive(:start).with("ftp.example.com", "user", port: 1522, password: "foo").and_yield(mock_sftp)
      data_source.connect_to_server do |sftp|
        expect(sftp).to eq mock_sftp
      end
      expect(data_source.root).to eq "."
    end

    it "authenticates with a password from the options" do
      data_source = SftpServer.new("sftp://user@ftp.example.com:1522/", password: "foo")
      expect(Net::SFTP).to receive(:start).with("ftp.example.com", "user", port: 1522, password: "foo").and_yield(mock_sftp)
      data_source.connect_to_server do |sftp|
        expect(sftp).to eq mock_sftp
      end
      expect(data_source.root).to eq "."
    end

    it "authenticates with an SSH key" do
      data_source = SftpServer.new("sftp://user@ftp.example.com/uploads", ssh_key: "foo")
      expect(Net::SFTP).to receive(:start).with("ftp.example.com", "user", key_data: ["foo\n"], keys: [], keys_only: true).and_yield(mock_sftp)
      data_source.connect_to_server do |sftp|
        expect(sftp).to eq mock_sftp
      end
      expect(data_source.root).to eq "uploads"
    end
  end

  describe "upload" do
    it "uploads a stream" do
      data_source = SftpServer.new("sftp://user@ftp.example.com/upload", password: "foo")
      stream = StringIO.new
      expect(mock_sftp).to receive(:upload!).with(stream, "upload/ready/foo.txt")
      data_source.upload(stream, "ready/foo.txt")
    end
  end
end
