# frozen_string_literal: true

require 'rails_helper'

describe RedisClient do
  it "gets a list of clients by name" do
    names = (RedisClient.names & ["default"])
    expect(names).to match_array ["default"]
  end

  it "gets redis connection options" do
    options = (RedisClient.options(:default).keys & [:driver, :url, :db, :timeout])
    expect(options).to match_array [:driver, :url, :db, :timeout]
  end

  it "gets a redis URL from the configuration" do
    expect(RedisClient.url(:default)).to start_with "redis://"
  end

  it "gets a persistent redis client pool per config" do
    client = RedisClient.connection_pool(:default)
    expect(RedisClient.connection_pool(:default).object_id).to eq client.object_id
  end

  it "gets a persistent redis client pool with an indifferent name" do
    expect(RedisClient.connection_pool(:default)).to eq RedisClient.connection_pool("default")
  end

  it "yields a connection in a with block" do
    retval = nil
    RedisClient.with(:default) { |redis| retval = redis.ping }
    expect(retval).to eq "PONG"
  end

  it "gets a wrapped connection pool that behaves like Redis" do
    client = RedisClient.client(:default)
    expect(client.ping).to eq "PONG"
    expect(client.with { |redis| redis.ping }).to eq "PONG"
  end

  it "closes client connections and automatically reopen them" do
    default_client = RedisClient.client(:default)
    default_redis = nil
    default_client.with { |redis| default_redis = redis }

    default_client.ping

    RedisClient.disconnect!

    # Test disconnected
    expect(default_redis.connected?).to eq false
    expect { default_client.ping }.to raise_error(ConnectionPool::PoolShuttingDownError)

    # Test automatic reconnection
    expect(RedisClient.client(:default).ping).to eq "PONG"
  end

  it "returns nil for blank redis connections" do
    expect(RedisClient.options(:no_connection)[:url]).to be_blank
    expect(RedisClient.client(:no_connection)).to eq nil
  end

  describe RedisClient::WrappedRedis do
    it "returns an object that behaves like Redis that gets the connection at runtime" do
      client = RedisClient::WrappedRedis.new(:default)
      expect(client.ping).to eq "PONG"
      RedisClient.disconnect!
      expect(client.ping).to eq "PONG"
    end
  end

  describe RedisClient::WrappedConnectionPool do
    it "returns a ConnectionPool that gets the connection at runtime" do
      pool = RedisClient::WrappedConnectionPool.new(:default)
      expect(pool).to be_a(ConnectionPool)
      expect(pool.with { |client| client.ping }).to eq "PONG"
      RedisClient.disconnect!
      expect(pool.with { |client| client.ping }).to eq "PONG"
      begin
        expect(pool.checkout).to be_a(Redis)
      ensure
        pool.checkin
      end
      expect(pool.size).to eq RedisClient.connection_pool(:default).size
      expect(pool.available).to eq RedisClient.connection_pool(:default).available
    end
  end
end
