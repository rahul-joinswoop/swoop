# frozen_string_literal: true

require 'rails_helper'
describe UUID do
  subject { UUID.uuid? uuid }

  context "with a valid uuid" do
    let(:uuid) { SecureRandom.uuid }

    it "works" do
      expect(subject).to be(true)
    end
  end

  context "with something else" do
    let(:uuid) { Faker::Lorem.sentence }

    it "works" do
      expect(subject).to be(false)
    end
  end
end
