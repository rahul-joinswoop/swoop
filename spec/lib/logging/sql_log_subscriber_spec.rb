# frozen_string_literal: true

require "rails_helper"

describe Logging::SqlLogSubscriber do
  it "does not log queries that run faster than the threshold" do
    stub_const("Logging::SqlLogSubscriber::LONG_RUNNING_QUERY_MILLISECONDS", 5000)
    stub_const("Logging::SqlLogSubscriber::ROLLBAR_LONG_RUNNING_QUERY_MILLISECONDS", 5000)
    expect_any_instance_of(Logging::SqlLogSubscriber).not_to receive(:long_running_query)
    User.joins(:company).where(id: 1, email: "user@example.com").first
  end

  it "logs queries that take longer than the threshold to run" do
    stub_const("Logging::SqlLogSubscriber::LONG_RUNNING_QUERY_MILLISECONDS", 0)
    stub_const("Logging::SqlLogSubscriber::ROLLBAR_LONG_RUNNING_QUERY_MILLISECONDS", 0)
    expect_any_instance_of(Logging::SqlLogSubscriber).to receive(:long_running_query).and_call_original
    expect(Rollbar).to receive(:warn)
    User.joins(:company).where(id: 1, email: "user@example.com").first
  end

  it "logs can turn off Rollbar logging" do
    stub_const("Logging::SqlLogSubscriber::LONG_RUNNING_QUERY_MILLISECONDS", 0)
    stub_const("Logging::SqlLogSubscriber::ROLLBAR_LONG_RUNNING_QUERY_MILLISECONDS", -1)
    expect_any_instance_of(Logging::SqlLogSubscriber).to receive(:long_running_query).and_call_original
    expect(Rollbar).not_to receive(:warn)
    User.joins(:company).where(id: 1, email: "user@example.com").first
  end
end
