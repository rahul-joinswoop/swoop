# frozen_string_literal: true

require 'rails_helper'

describe JobSimulator do
  around :each do |example|
    ClimateControl.modify(JOB_SIMULATOR_ENABLED: "true") { example.call }
  end

  describe "enabled?" do
    it "only be enabled if JOB_SIMULATOR_ENABLED=true" do
      expect(JobSimulator.enabled?).to eq true
      ClimateControl.modify(JOB_SIMULATOR_ENABLED: nil) do
        expect(JobSimulator.enabled?).to eq false
      end
    end
  end

  describe "simulator" do
    it "loads a scenario from a magic string value" do
      simulator = JobSimulator.simulator("Simulator Job 100")
      expect(simulator.config["scenario"]).to eq "100"
      expect(simulator.transitions).to be_a(Array)
    end

    it "merges options from JSON in the magic string" do
      simulator = JobSimulator.simulator('Simulator Job  100  { "status_delay": 1, "other": "foo" }')
      expect(simulator.config["scenario"]).to eq "100"
      expect(simulator.config["status_delay"]).to eq 1
      expect(simulator.config["other"]).to eq "foo"
    end

    it "returns nil if there is no magic string" do
      simulator = JobSimulator.simulator("Job Bimulator 100")
      expect(simulator).to eq nil
    end

    it "ignores invalid JSON options" do
      simulator = JobSimulator.simulator("Simulator Job 100 { stuff }")
      expect(simulator).to be_a(JobSimulator)
    end

    it "finds and load a single scenario from a multiline value" do
      simulator = JobSimulator.simulator <<~EOS
        Some text
        Simulator Job 100 {"foo": "bar"}
        Simulator Job 200 {"foo": "baz"}
        more text
      EOS
      expect(simulator.config["scenario"]).to eq "100"
      expect(simulator.config["foo"]).to eq "bar"
    end
  end

  describe "status_delay" do
    it "returns the status delay from the configuration" do
      simulator = JobSimulator.new("status_delay" => 1)
      expect(simulator.status_delay).to eq 1
    end

    it "has a default status delay" do
      simulator = JobSimulator.new({})
      expect(simulator.status_delay).to eq 20
    end
  end

  describe "travel_time" do
    it "returns 60 seconds by default" do
      simulator = JobSimulator.new("status_delay" => 5)
      expect(simulator.travel_time(Job.new(status: "En Route"))).to eq 60
    end

    it "returns the en_route_time if defined and job is en route" do
      simulator = JobSimulator.new("status_delay" => 5, "travel_time" => 6, "en_route_time" => 8)
      expect(simulator.travel_time(Job.new(status: "En Route"))).to eq 8
    end

    it "returns the towing_time if defined and job is towing" do
      simulator = JobSimulator.new("status_delay" => 5, "travel_time" => 6, "towing_time" => 8)
      expect(simulator.travel_time(Job.new(status: "Towing"))).to eq 8
    end

    it "returns the travel_time if defined and the more specific time is not set" do
      simulator = JobSimulator.new("status_delay" => 5, "travel_time" => 6)
      expect(simulator.travel_time(Job.new(status: "Towing"))).to eq 6
    end

    it "returns the status_delay if travel time is less than the status delay" do
      simulator = JobSimulator.new("status_delay" => 5, "travel_time" => 6, "towing_time" => 4)
      expect(simulator.travel_time(Job.new(status: "Towing"))).to eq 5
    end
  end

  describe "partner" do
    it "lookups the partner company from the name in the configuration" do
      company_1 = create(:rescue_company, name: "Joe's Towing")
      company_2 = create(:rescue_company, name: "Joey's Towing")
      expect(JobSimulator.new("partner" => "Joe's Towing").partner).to eq company_1
      expect(JobSimulator.new("partner" => "Joey's Towing").partner).to eq company_2
    end

    it "returns nil if the partner company does not exist" do
      create(:fleet_company, name: "A1 Towing")
      expect(JobSimulator.new("partner" => "A1 Towing").partner).to eq nil
    end

    it "returns nil if the partner is deleted" do
      create(:rescue_company, name: "A1 Towing", deleted_at: Time.current)
      expect(JobSimulator.new("partner" => "A1 Towing").partner).to eq nil
    end
  end

  describe "site" do
    let(:rescue_company) { create(:rescue_company, name: "A1 Towing") }
    let!(:hq) { create(:site, company: rescue_company, name: "HQ") }

    it "defaults to the partner HQ" do
      simulator = JobSimulator.new("partner" => rescue_company.name)
      expect(simulator.site).to eq hq
    end

    it "looks up partner site by name" do
      site_1 = create(:site, company: rescue_company, name: "Off Site")
      create(:site, company: create(:rescue_company), name: "Off Site")
      simulator = JobSimulator.new("partner" => rescue_company.name, "site" => "Off Site")
      expect(simulator.site).to eq site_1
    end

    it "defaults to the partner HQ if the site can't be found" do
      simulator = JobSimulator.new("partner" => rescue_company.name, "site" => "Off Site")
      expect(simulator.site).to eq hq
    end
  end

  describe "available_truck" do
    let(:rescue_company) { create(:rescue_company) }
    let!(:vehicle) { create(:rescue_vehicle, company: rescue_company) }
    let(:simulator) { JobSimulator.new({}) }

    it "is nil if there is no company" do
      expect(simulator.available_truck(nil)).to eq nil
    end

    it "returns a partner truck not currently working on a job" do
      truck = simulator.available_truck(rescue_company)
      expect(truck).to eq vehicle
    end

    it "creates a new truck if there are none available" do
      create(:fleet_managed_job, status: "Towing", rescue_company: rescue_company, rescue_vehicle: vehicle)
      truck = simulator.available_truck(rescue_company)
      expect(truck).not_to eq vehicle
      expect(truck).to be_persisted
      expect(truck.company).to eq rescue_company
    end
  end

  describe "available_driver" do
    let(:rescue_company) { create(:rescue_company) }
    let!(:user) { create(:user, company: rescue_company).tap { |driver| driver.add_role(:driver) } }
    let(:simulator) { JobSimulator.new({}) }

    before :each do
      create(:role, :driver)
      create(:role, :rescue)
    end

    it "is nil if there is no company" do
      expect(simulator.available_driver(nil)).to eq nil
    end

    it "returns a partner driver not currently working on a job" do
      driver = simulator.available_driver(rescue_company)
      expect(driver).to eq user
    end

    it "creates a new driver if there are none available" do
      create(:fleet_managed_job, status: "Towing", rescue_company: rescue_company, rescue_driver: user)
      driver = simulator.available_driver(rescue_company)
      expect(driver).not_to eq user
      expect(driver).to be_persisted
      expect(driver.company).to eq rescue_company
      expect(driver.first_name.present?).to eq true
      expect(driver.phone.present?).to eq true
    end
  end

  describe "next_transition" do
    it "lookups the next transition from a list" do
      simulator = JobSimulator.new("transitions" => ["start", "next", "finish"])
      expect(simulator.transitions).to eq ["start", "next", "finish"]
      expect(simulator.next_transition("start")).to eq "next"
      expect(simulator.next_transition("next")).to eq "finish"
      expect(simulator.next_transition("finish")).to eq nil
    end
  end

  describe "transition_job" do
    let(:simulator) { JobSimulator.new("transitions" => ["thing", "move_to_pending", "completed"]) }
    let(:job) { create(:fleet_managed_job, status: "Draft") }

    it "calls the matching transition_* method on the simulator if it exists" do
      def simulator.transition_thing(job)
        "thing #{job.id}"
      end

      expect(simulator.transition_job(job, "thing")).to eq "thing #{job.id}"
    end

    it "calls the state machine event if it exists" do
      simulator.transition_job(job, "move_to_pending")
      expect(job.status.downcase).to eq "pending"
    end

    it "sets the job status if no state machine event exists" do
      simulator.transition_job(job, "completed")
      expect(job.status.downcase).to eq "completed"
    end
  end

  describe "start" do
    let(:rescue_company) { create(:rescue_company) }
    let(:config) { { "partner" => rescue_company.name, "transitions" => ["first", "next", "finish"] } }
    let(:simulator) { JobSimulator.new(config) }
    let(:job) { create(:fleet_managed_job) }

    it "invokes the first transition after a delay", freeze_time: true do
      simulator.start(job)
      expect(JobSimulator::TransitionWorker).to have_enqueued_sidekiq_job(job.id, "first", config).in(simulator.status_delay)
    end
  end

  describe JobSimulator::TransitionWorker, freeze_time: true do
    let(:rescue_company) { create(:rescue_company) }
    let(:hq) { create(:site, company: rescue_company, name: "HQ") }
    let!(:service_location) { create(:location, :mission_safeway) }
    let!(:tow_location) { create(:location, :oakland_beer_garden) }
    let(:job) { create(:fleet_managed_job, rescue_company: rescue_company, service_location: service_location, drop_location: tow_location, site: hq) }
    let(:transitions) { ["partner_accepted", "partner_dispatched", "partner_enroute", "partner_onsite", "partner_towing", "partner_towdestination"] }
    let(:config) { { "partner" => rescue_company.name, "transitions" => transitions } }
    let(:simulator) { JobSimulator.new(config) }

    it "transitions the job to the next state and schedule another worker" do
      job.update_columns(status: "Assigned", rescue_company_id: nil)
      JobSimulator::TransitionWorker.new.perform(job.id, "partner_accepted", config)
      job.reload
      expect(job).to be_accepted
      expect(job.rescue_company).to eq rescue_company
      expect(job.rescue_company).to eq rescue_company
      expect(JobSimulator::TransitionWorker).to have_enqueued_sidekiq_job(job.id, "partner_dispatched", config).in(simulator.status_delay)
      expect(JobSimulator::TravelWorker.jobs).to be_empty
    end

    it "gracefully stops the simulator if the job enters an illegal state" do
      job.update_columns(status: "Canceled", rescue_company_id: nil)
      expect { JobSimulator::TransitionWorker.new.perform(job.id, "partner_accepted", config) }.not_to raise_error
      expect(JobSimulator::TransitionWorker.jobs).to be_empty
    end

    context "rescue company is not live" do
      let(:rescue_company) { create(:rescue_company, live: false) }

      it "assigns a sta on assign_and_dispatch" do
        job.update_columns(status: "Pending")
        JobSimulator::TransitionWorker.new.perform(job.id, "assign_and_dispatch", config)
        job.reload
        expect(job.sta).to be_present
        expect(job).to be_dispatched
      end

      it "transitions the job to the next state and schedule a TravelWorker if the job is enroute" do
        job.update_columns(
          status: "Dispatched",
          rescue_vehicle_id: simulator.available_truck(rescue_company).id,
          rescue_driver_id: simulator.available_driver(rescue_company).id,
        )
        hq.location = create(:location, :johns_cabin)
        hq.save!
        JobSimulator::TransitionWorker.new.perform(job.id, "partner_enroute", config)
        expect(JobSimulator::TransitionWorker).to have_enqueued_sidekiq_job(job.id, "partner_onsite", config).in(simulator.status_delay)
        expect(JobSimulator::TravelWorker.jobs).to be_empty
      end

      it "transitions the job to the next state and schedule a TravelWorker if the job is towing" do
        job.update_columns(
          status: "On Site",
          rescue_vehicle_id: simulator.available_truck(rescue_company).id,
          rescue_driver_id: simulator.available_driver(rescue_company).id,
        )
        JobSimulator::TransitionWorker.new.perform(job.id, "partner_towing", config)
        expect(JobSimulator::TransitionWorker).to have_enqueued_sidekiq_job(job.id, "partner_towdestination", config).in(simulator.status_delay)
        expect(JobSimulator::TravelWorker.jobs).to be_empty
      end
    end

    context "rescue company is live" do
      let(:rescue_company) { create(:rescue_company, live: true) }

      it "assigns a bta on partner_accepted" do
        job.update_columns(status: "Assigned")
        JobSimulator::TransitionWorker.new.perform(job.id, "partner_accepted", config)
        job.reload
        expect(job.bta).to be_present
        expect(job).to be_accepted
      end

      it "assigns a driver and truck if the status is dispatched" do
        job.update_columns(status: "Accepted")
        JobSimulator::TransitionWorker.new.perform(job.id, "partner_dispatched", config)
        job.reload
        expect(job.rescue_vehicle).to be_present
        expect(job.rescue_driver).to be_present
        expect(job.rescue_vehicle.driver).to eq job.rescue_driver
      end

      it "transitions the job to the next state and schedule a TravelWorker if the job is enroute" do
        job.update_columns(
          status: "Dispatched",
          rescue_vehicle_id: simulator.available_truck(rescue_company).id,
          rescue_driver_id: simulator.available_driver(rescue_company).id,
        )
        hq.location = create(:location, :johns_cabin)
        hq.save!
        JobSimulator::TransitionWorker.new.perform(job.id, "partner_enroute", config)
        hq_loc = [hq.location.lat, hq.location.lng]
        job_loc = [service_location.lat, service_location.lng]
        expect(JobSimulator::TravelWorker).to have_enqueued_sidekiq_job(job.id, hq_loc, job_loc, 0, "partner_onsite", config)
        expect(JobSimulator::TransitionWorker.jobs).to be_empty
      end

      it "transitions the job to the next state and schedule a TravelWorker if the job is towing" do
        job.update_columns(
          status: "On Site",
          rescue_vehicle_id: simulator.available_truck(rescue_company).id,
          rescue_driver_id: simulator.available_driver(rescue_company).id,
        )
        JobSimulator::TransitionWorker.new.perform(job.id, "partner_towing", config)
        tow_loc = [tow_location.lat, tow_location.lng]
        job_loc = [service_location.lat, service_location.lng]
        expect(JobSimulator::TravelWorker).to have_enqueued_sidekiq_job(job.id, job_loc, tow_loc, 0, "partner_towdestination", config)
        expect(JobSimulator::TransitionWorker.jobs).to be_empty
      end
    end

    it "transitions the job and not schedule another worker if the simulator is done" do
      job.update_columns(status: "Towing")
      JobSimulator::TransitionWorker.new.perform(job.id, "partner_towdestination", config)
      expect(JobSimulator::TransitionWorker.jobs).to be_empty
      expect(JobSimulator::TravelWorker.jobs).to be_empty
    end
  end

  describe JobSimulator::TravelWorker, freeze_time: true do
    let(:rescue_company) { create(:rescue_company) }
    let(:truck) { create(:rescue_vehicle, company: rescue_company) }
    let(:job) { create(:fleet_managed_job, status: "En Route", rescue_company: rescue_company, rescue_vehicle: truck) }
    let(:transitions) { ["partner_accepted", "partner_dispatched", "partner_enroute", "partner_onsite"] }
    let(:config) { { "partner" => rescue_company.name, "en_route_time" => 60, "towing_time" => 60, "transitions" => transitions } }
    let(:simulator) { JobSimulator.new(config) }
    let(:loc_1) { [45.1, -110.1] }
    let(:loc_2) { [45.5, -110.5] }

    it "schedules a vehicle update and another worker for the next segment" do
      JobSimulator::TravelWorker.new.perform(job.id, loc_1, loc_2, 1, "partner_onsite", config)
      expect(UpdateVehicleLocationWorker).to have_enqueued_sidekiq_job(rescue_company.id, truck.id, 45.133386452, -110.133118502, Time.now.utc.iso8601)
      expect(JobSimulator::TravelWorker).to have_enqueued_sidekiq_job(job.id, loc_1, loc_2, 2, "partner_onsite", config).in(5)
      expect(JobSimulator::TransitionWorker.jobs).to be_empty
    end

    it "schedules a transition to the next state if travel is done" do
      JobSimulator::TravelWorker.new.perform(job.id, loc_1, loc_2, 12, "partner_onsite", config)
      expect(UpdateVehicleLocationWorker).to have_enqueued_sidekiq_job(rescue_company.id, truck.id, 45.5, -110.5, Time.now.utc.iso8601)
      expect(JobSimulator::TravelWorker.jobs).to be_empty
      expect(JobSimulator::TransitionWorker).to have_enqueued_sidekiq_job(job.id, "partner_onsite", config)
    end

    it "does not schedule a transition to the next state if the simulator is done" do
      JobSimulator::TravelWorker.new.perform(job.id, loc_1, loc_2, 12, nil, config)
      expect(UpdateVehicleLocationWorker).to have_enqueued_sidekiq_job(rescue_company.id, truck.id, 45.5, -110.5, Time.now.utc.iso8601)
      expect(JobSimulator::TravelWorker.jobs).to be_empty
      expect(JobSimulator::TransitionWorker.jobs).to be_empty
    end
  end
end
