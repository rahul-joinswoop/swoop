# frozen_string_literal: true

require "rails_helper"

describe Base62Encryptor do
  let(:key) { SecureRandom.hex(32) }
  let(:iv) { SecureRandom.hex(16) }
  let(:values) { Faker::Lorem.words }
  let(:values_json) { JSON.fast_generate values }

  describe "#initialize" do
    subject { Base62Encryptor.new(opts) }

    let(:opts) { { key: key, iv: iv } }

    it "uses arguments" do
      expect(subject.key).to eq([key].pack("H*"))
      expect(subject.iv).to eq([iv].pack("H*"))
    end

    it "works" do
      encrypted = subject.encrypt(values_json)
      expect(encrypted).not_to eq(values_json)
      decrypted = JSON.parse! subject.decrypt(encrypted)
      expect(decrypted).to eq(values)
    end
  end
end
