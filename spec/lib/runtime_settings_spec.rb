# frozen_string_literal: true

require 'rails_helper'

describe RuntimeSettings do
  describe "puma_thread_count" do
    it "returns the value from the MAX_THREADS environment variable" do
      ClimateControl.modify("MAX_THREADS" => "14") do
        expect(RuntimeSettings.puma_thread_count).to eq 14
      end
    end

    it "defaults to 5" do
      ClimateControl.modify("MAX_THREADS" => nil) do
        expect(RuntimeSettings.puma_thread_count).to eq 5
      end
    end
  end

  describe "sidekiq_thread_count" do
    it "returns the number of sidekiq threads from the Sidekiq" do
      expect(RuntimeSettings.sidekiq_thread_count).to eq Sidekiq.options[:concurrency]
    end
  end

  describe "max_db_connections" do
    it "defaults to 1" do
      override_script_name("/tmp/foo") do
        expect(RuntimeSettings.max_db_connections).to eq 1
      end
    end

    it "sets the number of connection to the puma thread count when running puma" do
      expect(RuntimeSettings).to receive(:puma_thread_count).and_return(7)
      override_script_name("bin/puma") do
        expect(RuntimeSettings.max_db_connections).to eq 7
      end
    end

    it "sets the number of connection to the sidekiq thread count when running sidekiq" do
      expect(RuntimeSettings).to receive(:sidekiq_thread_count).and_return(27)
      override_script_name("bin/sidekiq") do
        expect(RuntimeSettings.max_db_connections).to eq 27
      end
    end

    it "is able to add additional connections with a scriptname_EXTRA_DB_CONNECTIONS variable" do
      override_script_name("bin/foo") do
        ClimateControl.modify("FOO_EXTRA_DB_CONNECTIONS" => "16") do
          expect(RuntimeSettings.max_db_connections).to eq 17
        end
      end
    end

    it "is able to set a minimum number of connections with DB_POOL" do
      override_script_name("bin/foo") do
        ClimateControl.modify("DB_POOL" => "8") do
          expect(RuntimeSettings.max_db_connections).to eq 8
          ClimateControl.modify("FOO_EXTRA_DB_CONNECTIONS" => "9") do
            expect(RuntimeSettings.max_db_connections).to eq 10
          end
        end
      end
    end
  end
end

def override_script_name(scriptname)
  save_val = $PROGRAM_NAME
  $PROGRAM_NAME = scriptname
  yield
ensure
  $0 = save_val
end
