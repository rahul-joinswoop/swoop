# frozen_string_literal: true

require "rails_helper"

describe PoiSiteSerializer do
  let(:attributes) { PoiSiteSerializer.new(place).as_json }

  let(:place) do
    build_stubbed :poi_site, name: "In my place", phone: "+14155550001"
  end

  it "has :id" do
    allow(place).to receive(:id).and_return(123)
    expect(attributes[:id]).to be(123)
  end

  it "has :name" do
    expect(attributes[:name]).to eql("In my place")
  end

  it "has :phone" do
    expect(attributes[:phone]).to eql("+14155550001")
  end

  it "has :manager" do
    expect(attributes[:manager]).not_to be_nil
  end
end
