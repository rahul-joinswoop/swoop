# frozen_string_literal: true

require "rails_helper"

describe Agero::RSA::CoordinateSerializer do
  describe '#as_json' do
    let(:location) do
      create(:location, lat: 10.001, lng: 11.001)
    end

    let(:attributes) { described_class.new(location).as_json }

    let(:expected) do
      {
        latitude: 10.001,
        longitude: 11.001,
      }
    end

    it 'matches serializer output' do
      expect(attributes).to eq(expected)
    end
  end
end
