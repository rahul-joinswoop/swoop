# frozen_string_literal: true

require "rails_helper"

describe Agero::RSA::SubmitRequestSerializer do
  describe '#as_json' do
    let!(:service_location) { create(:location) }
    let!(:drop_location) { create(:location) }
    let!(:drive) { create(:drive) }
    let!(:user) { drive.user }
    let!(:vehicle) { drive.vehicle }
    let!(:job) do
      create(:job, driver: drive,
                   drop_location: drop_location,
                   service_location: service_location)
    end
    let!(:question) do
      create(:question, question: Question::CUSTOMER_WITH_VEHICLE)
    end
    let!(:answer) { create(:answer, answer: "Yes") }
    let!(:question_result) do
      create(:question_result, job: job, question: question, answer: answer)
    end

    let(:attributes) { described_class.new(job).as_json }

    let(:expected) do
      {
        "requestor": {
          "name": {
            "firstName": job.customer.first_name,
            "lastName": job.customer.last_name,
            "middleInitial": "",
          },
          "callbackNumber": job.customer.phone,
        },
        "disablementLocation": {
          "address": {
            "streetAddress1": service_location.address,
            "streetAddress2": "",
            "city": service_location.city,
            "state": service_location.state,
            "country": service_location.country,
            "zip": service_location.zip,
          },
          "geographicalCoordinates": {
            "latitude": service_location.lat,
            "longitude": service_location.lng,
          },
          "customerAtLocation": "Yes",
          "locationType": 'Roadside-Street',
        },
        "towDestination": {
          "destinationBusinessName": nil, # unknown
          "destinationType": 'Roadside-Street',
          "address": {
            "streetAddress1": drop_location.address,
            "streetAddress2": "",
            "city": drop_location.city,
            "state": drop_location.state,
            "country": drop_location.country,
            "zip": drop_location.zip,
          },
          "geographicalCoordinates": {
            "latitude": drop_location.lat,
            "longitude": drop_location.lng,
          },
        },
        "vehicle": {
          "make": vehicle.make,
          "model": vehicle.model,
          "year": vehicle.year,
          "color": vehicle.color,
          "licensePlate": vehicle.license,
          "mileage": job.odometer,
          "vin": job.driver.vehicle.vin,
          "fuelType": nil, # todo
        },
        "notificationPreferences": {
          "preferredMode": "SMS",
          "emailId": nil,
          "textMessageNumber": job.customer.phone,
          "primaryPhoneNumber": nil,
        },
        "serviceDetails": {
          "disablementReason": job.symptom.name,
          "comments": [{
            "value": job.notes,
          }],
        },
      }
    end

    it 'matches serializer output' do
      expect(attributes).to eq(expected)
    end
  end
end
