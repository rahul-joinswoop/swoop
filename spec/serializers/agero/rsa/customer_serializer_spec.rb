# frozen_string_literal: true

require "rails_helper"

describe Agero::RSA::CustomerSerializer do
  describe '#as_json' do
    let(:customer) do
      build_stubbed(:user, first_name: "Bob",
                           last_name: "Doodledob",
                           phone: "555-555-5555")
    end

    let(:attributes) { described_class.new(customer).as_json }

    let(:expected) do
      {
        name: {
          firstName: "Bob",
          lastName: "Doodledob",
          middleInitial: "",
        },
        callbackNumber: "555-555-5555",
      }
    end

    it 'matches serializer output' do
      expect(attributes).to eq(expected)
    end
  end
end
