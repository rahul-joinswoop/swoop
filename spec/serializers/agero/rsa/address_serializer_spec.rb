# frozen_string_literal: true

require "rails_helper"

describe Agero::RSA::AddressSerializer do
  describe '#as_json' do
    let(:location) do
      build_stubbed(:location, address: "26 O'Farrell Street",
                               city: "San Francisco",
                               state: "California",
                               country: "USA",
                               zip: 99999)
    end

    let(:attributes) { described_class.new(location).as_json }

    let(:expected) do
      {
        streetAddress1: "26 O'Farrell Street",
        streetAddress2: "",
        city: "San Francisco",
        state: "California",
        country: "USA",
        zip: "99999",
      }
    end

    it 'matches serializer output' do
      expect(attributes).to eq(expected)
    end
  end
end
