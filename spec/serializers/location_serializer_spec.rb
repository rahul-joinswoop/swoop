# frozen_string_literal: true

require "rails_helper"

describe LocationSerializer do
  let(:location) { create(:location, :new_jerzy_towing_hq) }
  let(:attributes) { LocationSerializer.new(location).as_json }

  context 'street number and street name segregation' do
    it 'includes street, street_number and street_name' do
      expect(attributes[:street]).to be_present
      expect(attributes[:street_number]).to be_present
      expect(attributes[:street_name]).to be_present
    end

    it 'includes valid street, street_number and street_name' do
      expect(attributes[:street]).to eq("2381 18th Avenue")
      expect(attributes[:street_number]).to eq("2381")
      expect(attributes[:street_name]).to eq("18th Avenue")
    end
  end
end
