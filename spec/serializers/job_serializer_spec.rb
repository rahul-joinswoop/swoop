# frozen_string_literal: true

require "rails_helper"

describe JobSerializer do
  let(:attributes) { JobSerializer.new(job).as_json }
  let(:storage) { create(:storage, released_to_user: released_to_user) }
  let(:job) { create(:rescue_job, storage: storage) }

  context 'when job has storage' do
    let(:released_to_user) { nil }

    it 'serializes storage' do
      expect(attributes[:storage]).to be_present
    end

    context 'required attributes' do
      let(:released_to_user) { create(:user, first_name: 'First Name') }

      it 'has correct response structure and values for serialized `storage`' do
        expect(attributes[:storage]).to eq({
          id: storage.id,
          stored_at: storage.stored_at.to_datetime.as_json,
          site_id: storage.site_id,
          released_to_account_id: nil,
          released_to_email: nil,
          replace_invoice_info: nil,
          released_to_user: released_to_user,
        })
      end
    end
  end
end
