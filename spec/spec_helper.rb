# frozen_string_literal: true

require_relative './support/simplecov'
require 'webmock/rspec'
require 'vcr'

WebMock.disable_net_connect!(allow_localhost: true)

# don't try to match these query parameters in vcr.
# TODO - these are specific to the google maps api and might clobber other requests
# in the future, we may need to make this more generic
QUERY_PARAM_BLACKLIST = [:key, :client, :channel, :signature].freeze

VCR.configure do |config|
  config.ignore_request do |request|
    ["localhost", "127.0.0.1"].include?(URI(request.uri).host)
  end
  config.cassette_library_dir = "spec/vcr_cassettes"
  config.hook_into :webmock
  config.configure_rspec_metadata!

  # Let's set default VCR mode with VCR=all for re-recording
  # episodes. :once is VCR default
  record_mode = ENV["VCR"] ? ENV["VCR"].to_sym : :once

  config.default_cassette_options = {
    record: record_mode,
    match_requests_on: [:method, VCR.request_matchers.uri_without_params(*QUERY_PARAM_BLACKLIST)],
  }

  # it's good to have everything "cassetized" or ignored
  # config.allow_http_connections_when_no_cassette = true
end

RSpec.configure do |config|
  # enable slowest 10 examples output
  config.profile_examples = ENV.fetch("DISABLE_RSPEC_PROFILE", "").empty?
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
    expectations.syntax = :expect
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.around do |example|
    if example.metadata[:with_request_context]
      context = RequestContext.new(uuid: 'abcd')
      RequestContext.use(context) { example.call }
    else
      example.call
    end
  end
end

RSpec::Matchers.define_negated_matcher :not_change, :change

RSpec::Support::ObjectFormatter.default_instance.max_formatted_output_length = 2048
