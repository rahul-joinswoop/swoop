# frozen_string_literal: true

ENV["RAILS_ENV"] ||= "test"
require File.expand_path("../../config/environment", __FILE__)
abort("The Rails environment is running in production mode!") if Rails.env.production?

# require_relative this so that we can do `require_relative './spec/rails_helper'` from
# a rails console to play with factories / etc
require_relative "./spec_helper"
require "rspec/rails"
require "factory_bot_rails"
require "active_support/testing/time_helpers"
require "rspec-sidekiq"
require 'sidekiq/transaction_guard/database_cleaner'
require "rspec/json_expectations"

Dir[Rails.root.join("spec/support/**/*.rb")].sort.each { |f| require f }
require "capybara/rspec"

ActiveRecord::Migration.maintain_test_schema!

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :rails
  end
end

Timecop.safe_mode = true

RSpec::Sidekiq.configure do |config|
  config.warn_when_jobs_not_processed_by_sidekiq = false
end

RSpec.configure do |config|
  if ENV['NOISY_JS']
    config.after(:each, type: :system, js: true) do |spec|
      errors = page.driver.browser.manage.logs.get(:browser)
        .map(&:message)
        .to_a
      if errors.present?
        warn errors.join("\n\n")
      end
    end
  end

  config.before(:each, type: :system, js: true) do
    driven_by :swoop_chrome_headless
  end

  if ENV['FPROF']
    config.before(:each) do |example|
      ActiveSupport::Notifications.subscribe("factory_bot.run_factory") do |name, start, finish, id, payload|
        warn "FactoryBot: #{payload[:strategy]}(:#{payload[:name]})"
      end
    end
  end

  config.infer_spec_type_from_file_location!
  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.before(:all) do
    self.class.set_fixture_class drives: Drive
  end

  config.use_transactional_fixtures = false
  config.color = true

  config.include System::SessionHelpers, type: :system
  config.include Requests::JsonHelpers, type: :controller
  config.include Requests::JsonHelpers, type: :request
  config.include FactoryBot::Syntax::Methods
  config.include Devise::Test::ControllerHelpers, type: :controller
  config.include ActiveSupport::Testing::TimeHelpers
  config.include StubClass

  config.before do
    clear_instances

    # otherwise you'll see some Faker::UniqueGenerator::RetryLimitExceeded exploding
    Faker::UniqueGenerator.clear
  end

  config.before(:suite) do
    SwoopSeeder.setup_logging
    SwoopSeeder.seed_if_necessary

    # Do one initial check that the database was seeded properly
    SeedMigration::Migrator.check_pending!

    DatabaseCleaner[:redis].strategy = :truncation
  end

  config.around(:each) do |example|
    # skip_db_transactions is a bit misleading - by default we run inside of a
    # parent transaction because of use_transactional_fixtures=true and
    # DatabaseCleaner.strategy = :transaction. this makes some things hard to
    # test (see application_record_spec) so we allow individual specs to opt out.
    if example.metadata[:skip_db_transactions]
      DatabaseCleaner[:active_record].strategy = :truncation
      DatabaseCleaner.start
      SwoopSeeder.unseeded
    elsif example.metadata[:js]
      # rails system tests don't seem to need this so we don't use it (and in fact enabling it will break specs)
      DatabaseCleaner[:active_record].strategy = nil
      SwoopSeeder.seed_if_necessary
    else
      DatabaseCleaner[:active_record].strategy = :transaction
      SwoopSeeder.seed_if_necessary
    end

    DatabaseCleaner.cleaning do
      example.run
    end
  end

  # Allow setting timecop through rspec metadata, e.g. `freeze_time: Time.new(2018, 7, 1, 12, 0)`
  config.around(:each) do |example|
    time = (example.metadata[:time_travel] || example.metadata[:freeze_time])
    if time
      if time == true
        time = Time.at(Time.now.to_i)
      else
        time = time.to_time if time.respond_to?(:to_time)
      end
      if example.metadata[:time_travel]
        Timecop.travel(time) { example.run }
      else
        Timecop.freeze(time) { example.run }
      end
    else
      example.run
    end
  end

  # Avoid pushing jobs to background with Delayed::Job and run them synchronously.
  # We need to mention `:run_delayed_jobs_synchronously` on the test case to get thsi into effect.
  # E.g.:
  #     it 'runs the jobs synchronously', :run_delayed_jobs_synchronously do
  #       ..
  #       ..
  #     end
  config.around(:each, :run_delayed_jobs_synchronously) do |example|
    Delayed::Worker.delay_jobs = false
    example.run
    Delayed::Worker.delay_jobs = true
  end

  config.before(:all, type: :request) do
    host!("swoop.example.com")
  end

  # Clearing is the unique generators is non-trivial so only do it once per file.
  config.before(:all) do
    Faker::UniqueGenerator.clear
  end
end

def dump_table(clz)
  Rails.logger.debug "DUMPING #{clz.name}"
  clz.all.each do |row|
    Rails.logger.debug "#{row.inspect}"
  end
end

# add useful negating matcher
RSpec::Matchers.define_negated_matcher :avoid_changing, :change
RSpec::Matchers.define_negated_matcher :avoid_raising_error, :raise_error
RSpec::Matchers.define_negated_matcher :avoid_including, :include
