# frozen_string_literal: true

require "rails_helper"

describe "POST /ageroapi/authcode", type: :request do
  subject do
    post "/ageroapi/authcode", params: sample_payload
  end

  let(:sample_payload) do
    {
      "authorizationCode": "AUTH_CODE",
      "serviceProviderId": "NA",
      "callbackNumber": "NA",
      "echoUUID": "NA",
      "testCallbackURL": "NA",
    }
  end

  it 'attempts to create access token', :vcr do
    expect(Agero::ApiRequest::AccessToken).to receive(:get)
      .with(sample_payload[:authorizationCode])
      .and_call_original

    expect { subject }.to change(AgeroRSALog, :count).by(1)

    expect(response).to have_http_status(:ok)

    expect(json['success']).to eq(true)
  end
end
