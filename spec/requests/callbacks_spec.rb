# frozen_string_literal: true

require 'rails_helper'

describe "Callback requests", type: :request do
  describe "text redirects" do
    let(:uuid) { "deadbeef" }
    let(:uuid_link) { "/txt/#{uuid}" }

    before(:each) do
      allow(AuditSms).to receive(:find_by!).with(uuid: uuid).and_call_original
    end

    context "when link exists" do
      it "handles EtaAuditSms" do
        sms = create(:eta_audit_sms, uuid: uuid)
        get uuid_link

        expect(response).to redirect_to(sms.redirect_url)
      end

      it "handles LocationAuditSms" do
        sms = create(:location_audit_sms, uuid: uuid)
        get uuid_link

        expect(response).to redirect_to(sms.redirect_url)
      end

      it "updates clicked time" do
        sms = create(:eta_audit_sms, uuid: uuid)
        expect(sms.clicked_dttm).to be_nil

        get uuid_link

        first_click = sms.reload.clicked_dttm
        expect(first_click).not_to be_nil

        get uuid_link
        expect(sms.reload.clicked_dttm).to eq first_click
      end
    end

    context "no such sms" do
      it "blows up" do
        expect { get uuid_link }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe '404 page' do
    it 'is the last error route' do
      allow_any_instance_of(WebpackAssets).to receive(:manifest_json).and_return('{"application":{"js":["/assets/runtime.js","/assets/vendor.js","/assets/application.js"]}}')
      get "/this-route-doesnt-exist"
      # all unknown routes return react app and FE decides what to show
      expect(response).to have_http_status :ok
    end
  end

  describe 'twilio_status callback' do
    let(:sms) { create(:eta_audit_sms) }

    context 'when the text exists' do
      it 'validates the message params' do
        post sms.status_callback_url, params: { MessageStatus: 'success' }
        expect(response).to have_http_status :ok
        expect(sms.reload.status).to eq 'success'
      end

      it 'fails with missing MessageStatus' do
        post sms.status_callback_url, params: { ErrorCode: 'this is bad' }
        expect(response).to have_http_status :unprocessable_entity
        expect(sms.reload.status).to be_nil
      end
    end

    context 'when there is no audit sms' do
      it 'reports not found' do
        expect { post '/twilio_status/0' }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe 'review redirect' do
    it 'properly handles complete reviews' do
      review = create(:web_review)
      get "/review/#{review.job.uuid}"

      expect(response).to redirect_to review.redirect_url
    end

    it 'properly handles incomplete data' do
      get "/review/deadlink"
      expect(response).to have_http_status :not_found

      job = create(:job)
      get "/review/#{job.uuid}"
      expect(response).to have_http_status :not_found

      create(:review, job: job)
      get "/review/#{job.uuid}"
      expect(response).to have_http_status :not_found
    end
  end
end
