# frozen_string_literal: true

require "rails_helper"

describe "react", type: :request do
  let(:job) { create :job }
  let(:manifest) { create :manifest }

  before(:each) do
    allow_any_instance_of(WebpackAssets).to receive(:manifest_json).and_return(manifest.to_json)
  end

  shared_examples "manifest" do
    it "works for css and js" do
      expect(subject).to eq(200)
      # js entries have to appear in the page in the same order they are listed
      # in the manifest
      prev_idx = 0
      prev_preload_idx = 0
      manifest[key][:js].each do |js|
        preload_idx = response.body.index <<~PRELOAD
          <link as="script" href="#{js}" rel="preload" />
        PRELOAD
        expect(preload_idx).to be > prev_preload_idx
        prev_preload_idx = preload_idx

        idx = response.body.index <<~SCRIPT
          <script src="#{js}"></script>
        SCRIPT
        expect(idx).to be > prev_idx
        prev_idx = idx

        # make sure our preload tag comes first
        expect(idx).to be > preload_idx
      end

      prev_idx = 0
      prev_preload_idx = 0

      manifest[key][:css].each do |css|
        preload_idx = response.body.index <<~PRELOAD
          <link as="style" href="#{css}" rel="preload" />
        PRELOAD
        expect(preload_idx).to be > prev_preload_idx
        prev_preload_idx = preload_idx

        idx = response.body.index <<~LINK
          <link rel="stylesheet" media="all" href="#{css}" />
        LINK
        expect(idx).to be > prev_idx
        prev_idx = idx

        # make sure our preload tag comes first
        expect(idx).to be > preload_idx
      end
    end
  end

  context "root" do
    subject { get "/" }

    let(:key) { :application }

    it_behaves_like "manifest"
  end

  context "get_location" do
    subject { get "/get_location/#{job.uuid}" }

    let(:key) { :get_location }

    it_behaves_like "manifest"
    it "loads job and updates get_location_clicked" do
      expect(Job).to receive(:find_by!).with(uuid: job.uuid).once.and_call_original
      expect(subject).to eq(200)
      expect { job.reload }.to change(job, :get_location_clicked).to(true)
    end
  end

  context "show_eta" do
    subject { get "/show_eta/#{job.uuid}" }

    let(:key) { :show_eta }

    it_behaves_like "manifest"
    it "loads job" do
      expect(Job).to receive(:find_by!).with(uuid: job.uuid).once.and_call_original
      expect(subject).to eq(200)
    end
  end

  context "survey" do
    subject { get "/s/#{sms.job.uuid}" }

    let(:fleet_company) { create :fleet_company }
    let(:survey) { create :survey, :with_questions, company: fleet_company }
    let(:job) { create :job, fleet_company: fleet_company }
    let(:review) { create :web_review, company: fleet_company, job: job, survey: survey }
    let(:sms) { create :audit_sms, :sms_branded_review_link, job: review.job }

    let(:key) { :survey }

    it_behaves_like "manifest"
    it "contains questions" do
      expect(subject).to eq(200)
      expect(match = response.body.match(/gon\.survey_questions=(.*?);/m)).to be_present
      questions = JSON.parse(match[1])
      expect(questions).to eq(survey.questions.as_json(only: [:id, :type, :question]))
    end
  end
end
