# frozen_string_literal: true

require "rails_helper"

describe "Welcome", type: :request do
  describe "apple_app_site_association" do
    shared_examples "it serves apple-app-site-association" do
      let(:association) { WelcomeController::APPLE_APP_SITE_ASSOCIATION }
      it "works" do
        subject
        expect(response.body).to eq(association)
        expect(response.headers['Content-Type']).to eq("application/json; charset=utf-8")
      end
    end
    context "/.well-known/apple-app-site-association" do
      subject { get "/.well-known/apple-app-site-association" }

      it_behaves_like "it serves apple-app-site-association"
    end

    context "/apple-app-site-association" do
      subject { get "/apple-app-site-association" }

      it_behaves_like "it serves apple-app-site-association"
    end
  end
end
