# frozen_string_literal: true

require "rails_helper"

describe "CORS", type: :request do
  subject do
    process :options, "/stats", headers: headers
  end

  context "origin" do
    let(:headers) { { Origin: origin } }

    context "with a good origin" do
      let(:origin) { ENV.fetch('SITE_URL', 'http://localhost:3000') }

      it 'works' do
        expect(subject).to eq(204)
        expect(response.headers['Access-Control-Allow-Origin']).to eq(origin)
        expect(response.headers['Access-Control-Allow-Methods']).to eq("GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD")
      end
    end

    context "with a bad origin" do
      let(:origin) { "https://locasdfasdfalhost:3000" }

      it 'works' do
        expect(subject).to eq(204)
        expect(response.headers.keys).not_to include('Access-Control-Allow-Origin', 'Access-Control-Allow-Methods')
      end
    end
  end

  context "without an origin" do
    let(:headers) { {} }

    it 'works' do
      expect(subject).to eq(204)
      expect(response.headers.keys).not_to include('Access-Control-Allow-Origin', 'Access-Control-Allow-Methods')
    end
  end
end
