# frozen_string_literal: true

require 'rails_helper'

describe '/api/v1/root/companies', type: :request do
  before(:each) do
    # The CompanySearchController calls CompanySearchService, whose search capability
    # needs to be intercepted by a "Dummy" object so that Elastic isn't actually
    # called, because currently Elastic searching is not functional on CircleCI. -JJG
    stub_class 'ElasticResponseDummy' do
      def empty?
        false
      end

      def order(_)
      end
    end

    allow_any_instance_of(CompanySearchService).to receive(:search).and_return([1, ElasticResponseDummy.new])
    allow_any_instance_of(ElasticResponseDummy).to receive_message_chain('order.includes.distinct') { Company.all.to_a }
  end

  let(:root_user) { create(:user, :root) }
  let(:access_token) do
    create(
      :doorkeeper_access_token,
      resource_owner_id: root_user.id
    )
  end
  let(:request_headers) do
    {
      'Authorization': "Bearer #{access_token.token}",
      'Accept': 'application/json',
      'Content-type': 'application/json',
    }
  end
  let(:response_code) { response.status }
  let(:response_body) { JSON.parse(response.body) }

  context 'given' do
    it 'starts out with one SuperCompany from the root user' do
      expect(Company.count).to eql 1
      expect(Company.first).to be_kind_of SuperCompany
    end
  end

  describe 'GET /api/v1/root/companies/search' do
    subject { get "/api/v1/root/companies/search/?page=#{page_no}&per_page=#{per_page}&order=id,desc&original=+&", headers: request_headers }

    let(:page_no) { 1 }
    let(:per_page) { 100000000 }

    describe 'fleet companies' do
      let(:response_body_fleet_companies) { response_body['companies']&.select { |company| company['type'] == 'FleetCompany' } }

      describe 'managed' do
        let!(:managed_fleet_company) { create :fleet_managed_company, :fleet_demo, :with_ranker, :with_questions }

        describe 'autoassign rankers' do
          let!(:ranker) { managed_fleet_company.ranker }

          it 'render the default ranker' do
            expect(FleetCompany.count).to eql 1
            expect(managed_fleet_company.ranker).to be_kind_of Auction::Ranker
            subject
            expect(response_code).to eql(200)
            expect(response_body.keys).to contain_exactly('companies', 'total')
            expect(response_body['companies'].size).to eql 2
            expect(response_body_fleet_companies.size).to eql 1
            expect(response_body_fleet_companies.first['id']).to eql managed_fleet_company.id
            expect(response_body_fleet_companies.first.keys).not_to include('questions')
            expect(response_body_fleet_companies.first.keys).not_to include('sites')
            expect(response_body_fleet_companies.first.keys).to include('autoassignment_ranker')
            expect(response_body_fleet_companies.first['autoassignment_ranker'].keys).to contain_exactly('id', 'type', 'weights', 'constraints')
            expect(response_body_fleet_companies.first['autoassignment_ranker']['id']).to eql ranker.id
            expect(response_body_fleet_companies.first['autoassignment_ranker']['type']).to eql 'Auction::WeightedSumRanker'
            expect(response_body_fleet_companies.first['autoassignment_ranker']['weights'].keys).to contain_exactly('speed', 'quality', 'cost')
            expect(response_body_fleet_companies.first['autoassignment_ranker']['constraints'].keys).to contain_exactly("max_cost", "max_eta", "max_rating", "min_cost", "min_eta", "min_rating")
            expect(response_body_fleet_companies.first['autoassignment_ranker']['weights']['speed']).to eql 1.0
            expect(response_body_fleet_companies.first['autoassignment_ranker']['weights']['quality']).to eql 0.0
            expect(response_body_fleet_companies.first['autoassignment_ranker']['weights']['cost']).to eql 0.0
            expect(response_body_fleet_companies.first['autoassignment_ranker']['constraints']['min_eta']).to eql 5
            expect(response_body_fleet_companies.first['autoassignment_ranker']['constraints']['max_eta']).to eql 60
            expect(response_body_fleet_companies.first['autoassignment_ranker']['constraints']['min_cost']).to eql 20
            expect(response_body_fleet_companies.first['autoassignment_ranker']['constraints']['max_cost']).to eql 150
            expect(response_body_fleet_companies.first['autoassignment_ranker']['constraints']['min_rating']).to eql 40
            expect(response_body_fleet_companies.first['autoassignment_ranker']['constraints']['max_rating']).to eql 100
          end
        end
      end

      describe 'in-house' do
        let!(:inhouse_fleet_company) { create :fleet_in_house_company }

        describe 'autoassign rankers' do
          it 'do not render' do
            expect(FleetCompany.count).to eql 1
            expect(inhouse_fleet_company.ranker).to be_nil
            subject
            expect(response_code).to eql(200)
            expect(response_body.keys).to contain_exactly('companies', 'total')
            expect(response_body_fleet_companies.first.keys).not_to include('questions')
            expect(response_body_fleet_companies.first.keys).not_to include('sites')
            expect(response_body['companies'].size).to eql 2
            expect(response_body_fleet_companies.size).to eql 1
            expect(response_body_fleet_companies.first['id']).to eql inhouse_fleet_company.id
            expect(response_body_fleet_companies.first.keys).not_to include('autoassignment_ranker')
          end
        end
      end
    end
  end

  describe 'PATCH /api/v1/root/companies/:id' do
    subject { patch "/api/v1/root/companies/#{company.id}", params: request_body.to_json, headers: request_headers }

    describe 'fleet companies' do
      describe 'managed or in-house' do
        let!(:fleet_company) { create :fleet_in_house_company }
        let(:company) { fleet_company }

        describe 'attributes' do
          let(:request_body) do
            {
              company: {
                auction_max_distance: "40",
                id: fleet_company.id,
              },
            }
          end

          it 'updates' do
            expect(fleet_company.auction_max_distance).not_to eql 40
            subject
            expect(response_body['auction_max_distance']).to eql 40
          end
        end
      end

      describe 'managed' do
        let!(:managed_fleet_company) { create :fleet_managed_company, :fleet_demo, :with_ranker }
        let(:company) { managed_fleet_company }

        describe 'autoassign rankers' do
          let!(:ranker) { managed_fleet_company.get_or_create_ranker }

          context 'with valid ranker attributes' do
            def before_check
              managed_fleet_company.reload
              expect(managed_fleet_company.ranker).to be_kind_of Auction::Ranker
              expect(managed_fleet_company.ranker.weights['speed']).to eql 1.0
              expect(managed_fleet_company.ranker.weights['quality']).to eql 0.0
              expect(managed_fleet_company.ranker.weights['cost']).to eql 0.0
              expect(managed_fleet_company.ranker.constraints['min_eta']).to eql 5
              expect(managed_fleet_company.ranker.constraints['max_eta']).to eql 60
              expect(managed_fleet_company.ranker.constraints['min_cost']).to eql 20
              expect(managed_fleet_company.ranker.constraints['max_cost']).to eql 150
              expect(managed_fleet_company.ranker.constraints['min_rating']).to eql 40
              expect(managed_fleet_company.ranker.constraints['max_rating']).to eql 100
            end

            def after_check
              expect(response_code).to eql(200)
              expect(response_body['id']).to eql managed_fleet_company.id
              expect(response_body['autoassignment_ranker'].keys).to contain_exactly('id', 'type', 'weights', 'constraints')
              expect(response_body['autoassignment_ranker']['id']).not_to eql ranker.id
              expect(response_body['autoassignment_ranker']['type']).to eql 'Auction::WeightedSumRanker'
              expect(response_body['autoassignment_ranker']['weights'].keys).to contain_exactly('speed', 'quality', 'cost')
              expect(response_body['autoassignment_ranker']['constraints'].keys).to contain_exactly('min_eta', 'max_eta', 'min_cost', 'max_cost', 'min_rating', 'max_rating')
            end

            context 'updating only weights' do
              let(:request_body) do
                {
                  company: {
                    autoassignment_ranker: {
                      weights: {
                        speed: 0.5,
                        quality: 0.5,
                      },
                    },
                    id: managed_fleet_company.id,
                  },
                }
              end

              it 'updates it with a new ranker' do
                before_check
                subject
                after_check
                expect(response_body['autoassignment_ranker']['weights']['speed']).to eql 0.5
                expect(response_body['autoassignment_ranker']['weights']['quality']).to eql 0.5
                expect(response_body['autoassignment_ranker']['weights']['cost']).to eql 0.0
                expect(response_body['autoassignment_ranker']['constraints']['min_eta']).to eql 5
                expect(response_body['autoassignment_ranker']['constraints']['max_eta']).to eql 60
                expect(response_body['autoassignment_ranker']['constraints']['min_cost']).to eql 20
                expect(response_body['autoassignment_ranker']['constraints']['max_cost']).to eql 150
                expect(response_body['autoassignment_ranker']['constraints']['min_rating']).to eql 40
                expect(response_body['autoassignment_ranker']['constraints']['max_rating']).to eql 100
              end
            end

            context 'updating only constraints' do
              let(:request_body) do
                {
                  company: {
                    autoassignment_ranker: {
                      constraints: {
                        min_eta: 1,
                        max_eta: 2,
                        min_cost: 3,
                        max_cost: 4,
                        min_rating: 5,
                        max_rating: 6,
                      },
                    },
                    id: managed_fleet_company.id,
                  },
                }
              end

              it 'updates it with a new ranker' do
                before_check
                subject
                after_check
                expect(response_body['autoassignment_ranker']['weights']['speed']).to eql 1.0
                expect(response_body['autoassignment_ranker']['weights']['quality']).to eql 0.0
                expect(response_body['autoassignment_ranker']['weights']['cost']).to eql 0.0
                expect(response_body['autoassignment_ranker']['constraints']['min_eta']).to eql 1
                expect(response_body['autoassignment_ranker']['constraints']['max_eta']).to eql 2
                expect(response_body['autoassignment_ranker']['constraints']['min_cost']).to eql 3
                expect(response_body['autoassignment_ranker']['constraints']['max_cost']).to eql 4
                expect(response_body['autoassignment_ranker']['constraints']['min_rating']).to eql 5
                expect(response_body['autoassignment_ranker']['constraints']['max_rating']).to eql 6
              end
            end

            context 'updating weights and constraints' do
              let(:request_body) do
                {
                  company: {
                    autoassignment_ranker: {
                      weights: {
                        speed: 0.5,
                        quality: 0.5,
                      },
                      constraints: {
                        max_cost: 95,
                      },
                    },
                    id: managed_fleet_company.id,
                  },
                }
              end

              it 'updates it with a new ranker' do
                before_check
                subject
                after_check
                expect(response_body['autoassignment_ranker']['weights']['speed']).to eql 0.5
                expect(response_body['autoassignment_ranker']['weights']['quality']).to eql 0.5
                expect(response_body['autoassignment_ranker']['weights']['cost']).to eql 0.0
                expect(response_body['autoassignment_ranker']['constraints']['min_eta']).to eql 5
                expect(response_body['autoassignment_ranker']['constraints']['max_eta']).to eql 60
                expect(response_body['autoassignment_ranker']['constraints']['min_cost']).to eql 20
                expect(response_body['autoassignment_ranker']['constraints']['max_cost']).to eql 95
                expect(response_body['autoassignment_ranker']['constraints']['min_rating']).to eql 40
                expect(response_body['autoassignment_ranker']['constraints']['max_rating']).to eql 100
              end
            end

            context 'updating all weights and constraints' do
              let(:request_body) do
                {
                  company: {
                    autoassignment_ranker: {
                      weights: {
                        cost: 0.55,
                        speed: 0.4,
                        quality: 0.05,
                      },
                      constraints: {
                        min_eta: 1,
                        max_eta: 2,
                        min_cost: 3,
                        max_cost: 4,
                        min_rating: 5,
                        max_rating: 6,
                      },
                    },
                    id: managed_fleet_company.id,
                  },
                }
              end

              it 'updates it with a new ranker' do
                before_check
                subject
                after_check
                expect(response_body['autoassignment_ranker']['weights']['speed']).to eql 0.4
                expect(response_body['autoassignment_ranker']['weights']['quality']).to eql 0.05
                expect(response_body['autoassignment_ranker']['weights']['cost']).to eql 0.55
                expect(response_body['autoassignment_ranker']['constraints']['min_eta']).to eql 1
                expect(response_body['autoassignment_ranker']['constraints']['max_eta']).to eql 2
                expect(response_body['autoassignment_ranker']['constraints']['min_cost']).to eql 3
                expect(response_body['autoassignment_ranker']['constraints']['max_cost']).to eql 4
                expect(response_body['autoassignment_ranker']['constraints']['min_rating']).to eql 5
                expect(response_body['autoassignment_ranker']['constraints']['max_rating']).to eql 6
              end
            end

            context 'updating ranker weights summing up to 1.0 when one of the weight is 0.1' do
              let(:request_body) do
                {
                  company: {
                    autoassignment_ranker: {
                      weights: {
                        cost: 0.6999,
                        speed: 0.2,
                        quality: 0.011,
                      },
                    },
                    id: managed_fleet_company.id,
                  },
                }
              end

              it 'updates it with a new ranker' do
                before_check
                subject
                after_check
                expect(response_body['autoassignment_ranker']['weights']['speed']).to eql 0.2
                expect(response_body['autoassignment_ranker']['weights']['quality']).to eql 0.011
                expect(response_body['autoassignment_ranker']['weights']['cost']).to eql 0.6999
                expect(response_body['autoassignment_ranker']['constraints']['min_eta']).to eql 5
                expect(response_body['autoassignment_ranker']['constraints']['max_eta']).to eql 60
                expect(response_body['autoassignment_ranker']['constraints']['min_cost']).to eql 20
                expect(response_body['autoassignment_ranker']['constraints']['max_cost']).to eql 150
                expect(response_body['autoassignment_ranker']['constraints']['min_rating']).to eql 40
                expect(response_body['autoassignment_ranker']['constraints']['max_rating']).to eql 100
              end
            end
          end

          context 'with invalid ranker attributes' do
            describe 'weights do not add to 1.0' do
              let(:request_body) do
                {
                  company: {
                    autoassignment_ranker: {
                      weights: {
                        speed: 0.45,
                        quality: 0.45,
                        cost: 0.11,
                      },
                    },
                    id: managed_fleet_company.id,
                  },
                }
              end

              it 'rejects the update and responds with an error' do
                subject
                expect(response_code).to eql(422)
                expect(response_body['message']).to include("Validation failed: Weights Cost + Speed + Quality must add up to 1.0")
              end
            end
          end
        end
      end

      describe 'in-house' do
        let!(:inhouse_fleet_company) { create :fleet_in_house_company }
        let(:company) { inhouse_fleet_company }

        describe 'cannot provide ranker attributes' do
          let(:request_body) do
            {
              company: {
                autoassignment_ranker: {
                  weights: {
                    speed: 0.5,
                    quality: 0.5,
                  },
                },
                id: inhouse_fleet_company.id,
              },
            }
          end

          it 'rejects the update and responds with an error' do
            subject
            expect(response_code).to eql(500)
            expect(response_body['message']).to include("Autoassignment rankings cannot be provided for non-managed fleet companies")
          end
        end
      end
    end
  end
end
