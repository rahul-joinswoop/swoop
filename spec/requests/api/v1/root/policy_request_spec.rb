# frozen_string_literal: true

require 'rails_helper'

describe '/api/v1/root/policy', type: :request do
  let(:root_user) { create(:user, :root) }
  let(:access_token) do
    create(
      :doorkeeper_access_token,
      resource_owner_id: root_user.id
    )
  end
  let(:request_headers) do
    {
      'Authorization': "Bearer #{access_token.token}",
      'Accept': 'application/json',
      'Content-type': 'application/json',
    }
  end
  let(:response_code) { response.status }
  let(:response_body) { JSON.parse(response.body) }

  # Depricated with /api/v1/root/policy/search
  describe 'POST /api/v1/root/policy' do
    subject { post "/api/v1/root/policy", params: request_body.to_json, headers: request_headers }

    describe 'works' do
      describe 'attributes' do
        let!(:fleet_company) { create :fleet_managed_company, :century }
        let!(:job) { create :fleet_managed_job, fleet_company: fleet_company }

        let(:request_body) do
          {
            number: 1234567,
            job_id: job.id,
          }
        end

        before do
          fleet_company.settings << Setting.create!(key: Setting::PCC_SERVICE_NAME, value: 'Farmers')
        end

        it 'returns an async object' do
          subject
          # {"async_request":{"id":1999}}
          expect(response_body['async_request']).to eql({ "id" => API::AsyncRequest.last.id })
        end
      end
    end
  end

  describe 'POST /api/v1/root/policy/search' do
    subject { post "/api/v1/root/policy/search", params: request_body.to_json, headers: request_headers }

    describe 'works' do
      describe 'attributes' do
        let!(:fleet_company) { create :fleet_managed_company, :century }
        let!(:job) { create :fleet_managed_job, fleet_company: fleet_company }

        let(:search_terms) { { policy_number: 1234567 } }

        let(:request_body) do
          {
            search_terms: search_terms,
            job_id: job.id,
          }
        end

        before do
          fleet_company.settings << Setting.create!(key: Setting::PCC_SERVICE_NAME, value: 'Farmers')
        end

        it 'queues the search and returns an async object' do
          subject
          expect(PCC::PolicyLookup::Farmers::Worker).to have_enqueued_sidekiq_job(API::AsyncRequest.last.id, job.id, search_terms)
          expect(response_body['async_request']).to eql({ "id" => API::AsyncRequest.last.id })
        end
      end
    end
  end

  describe 'POST /api/v1/root/policy/coverage' do
    subject { post "/api/v1/root/policy/coverage", params: request_body.to_json, headers: request_headers }

    describe 'works' do
      describe 'attributes' do
        let!(:fleet_company) { create :fleet_managed_company, :lincoln }
        let!(:job) { create :fleet_managed_job, fleet_company: fleet_company }

        let(:policy) do
          create :pcc_policy, {
            job: job,
          }
        end

        let(:request_body) do
          {
            "pcc_policy_id": policy.id,
            "pcc_vehicle_id": 1234,
            "pcc_driver_id": 6789,
            "job": {
              "id": job.id,
              "service": {
                "name": ServiceCode::TOW,
              },
              "vehicle": {
                "year": 1980,
                "odometer": 55000,
              },
              "location": {
                "service_location": {
                  "lat": 34.5,
                  "lng": -90.5,
                },
                "dropoff_location": {
                  "site_id": 22,
                },
              },
            },
          }
        end

        before do
          fleet_company.settings << Setting.create!(key: Setting::PCC_SERVICE_NAME, value: 'PolicyLookupService')
        end

        it 'queues the search and returns an async object' do
          subject
          expect(response_code).to eq(200)
          expect(PCC::CoverageDetails::PolicyLookupService::Worker).to have_enqueued_sidekiq_job(
            API::AsyncRequest.last&.id,
            job.id,
            policy.id,
            request_body
          )

          expect(response_body['async_request']).to eql({ "id" => API::AsyncRequest.last.id })
        end
      end
    end
  end
end
