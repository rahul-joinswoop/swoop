# frozen_string_literal: true

require 'rails_helper'

describe '/api/v1/root/dropoff_sites', type: :request do
  let(:root_user) { create(:user, :root) }
  let(:access_token) do
    create(
      :doorkeeper_access_token,
      resource_owner_id: root_user.id
    )
  end
  let(:request_headers) do
    {
      'Authorization': "Bearer #{access_token.token}",
      'Accept': 'application/json',
      'Content-type': 'application/json',
    }
  end
  let(:response_code) { response.status }
  let(:response_body) { JSON.parse(response.body) }

  describe 'POST /api/v1/root/dropoff_sites' do
    subject { post "/api/v1/root/dropoff_sites", params: request_body.to_json, headers: request_headers }

    describe 'works' do
      describe 'attributes' do
        let!(:lincoln) { create :fleet_managed_company, :lincoln }

        let!(:job) do
          create :fleet_managed_job, {
            fleet_company: lincoln,
          }
        end

        let(:request_body) do
          {
            "job": {
              "id": job.id,
              "service": {
                "name": ServiceCode::TOW,
              },
              "location": {
                "service_location": {
                  "lat": 34.5,
                  "lng": -90.5,
                },
              },
            },
          }
        end

        it 'queues the request and returns an async object' do
          subject
          expect(response_code).to eq(200)
          expect(Sites::Dropoff::Worker).to(
            have_enqueued_sidekiq_job(API::AsyncRequest.last&.id, job.id, request_body)
          )

          expect(response_body['async_request']).to eql({ "id" => API::AsyncRequest.last.id })
        end
      end
    end
  end
end
