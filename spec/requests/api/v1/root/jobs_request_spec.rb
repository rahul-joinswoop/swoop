# frozen_string_literal: true

require 'rails_helper'

describe '/api/v1/root/companies', type: :request do
  let(:root_user) { create(:user, :root) }
  let(:access_token) do
    create(
      :doorkeeper_access_token,
      resource_owner_id: root_user.id
    )
  end
  let(:request_headers) do
    {
      'Authorization': "Bearer #{access_token.token}",
      'Accept': 'application/json',
      'Content-type': 'application/json',
    }
  end
  let(:response_code) { response.status }
  let(:response_body) { JSON.parse(response.body) }

  describe 'GET /api/v1/root/jobs/:id' do
    subject { get "/api/v1/root/jobs/#{fleet_managed_job.id}", params: nil, headers: request_headers }

    let!(:fleet_managed_job) do
      create(:fleet_managed_job, {

      })
    end

    context 'no experiment' do
      it 'does not show experiment' do
        subject
        Rails.logger.debug "#{JSON.pretty_generate(response_body)}"
        expect(response_body['experiments']).to eq([])
      end
    end

    context 'experiment exists' do
      let!(:experiment1) do
        Experiment.create!(name: 'experiment 1')
      end

      let!(:experiment1_variant_on) do
        Variant.create!(experiment: experiment1,
                        name: 'on')
      end

      let!(:target_variant) do
        TargetVariant.create!(target: fleet_managed_job,
                              variant: experiment1_variant_on) do
        end
      end

      it 'shows experiment' do
        subject
        Rails.logger.debug "#{JSON.pretty_generate(response_body)}"
        expect(response_body['experiments']&.length).to eq(1)
      end
    end
  end

  describe 'PATCH /api/v1/root/jobs/:id' do
    subject { patch "/api/v1/root/jobs/#{fleet_managed_job.id}", params: request_body.to_json, headers: request_headers }

    let!(:fleet_managed_job) { create(:fleet_managed_job, drop_location: existing_drop_location) }

    describe 'drop location' do
      let(:request_body) do
        {
          job: {
            id: fleet_managed_job.id,
            acts_as_company_id: root_user.company.id,
            drop_location: new_drop_location_request_block,
            priority_response: false,
            will_store: false,
          },
        }
      end
      let(:new_drop_location_request_block) do
        {
          address: new_drop_location.address,
          exact: true,
          lat: new_drop_location.lat,
          lng: new_drop_location.lng,
          location_type_id: new_drop_location.location_type.id.to_s,
          site_id: nil,
          place_id: "ABC123",
        }
      end

      context 'valid' do
        context 'adding' do
          let!(:existing_drop_location) { nil }
          let!(:new_drop_location) { create(:location, :new_jerzy_towing_hq) }

          it "returns the job's drop location" do
            expect(fleet_managed_job.drop_location).to be_nil
            expect(request_body[:job][:drop_location]).not_to be_nil
            subject
            expect(response_code).to eql(200)
            expect(response_body['drop_location']).not_to be_nil
            expect(response_body['drop_location']['address']).to eql(new_drop_location.address)
            expect(response_body['drop_location']['lat']).to eql(new_drop_location.lat)
            expect(response_body['drop_location']['lng']).to eql(new_drop_location.lng)
          end
        end

        context 'removing' do
          let!(:existing_drop_location) { create(:location, :new_jerzy_towing_hq) }
          let!(:new_drop_location) { nil }
          let(:new_drop_location_request_block) { nil }

          it "returns the job's null drop location" do
            expect(fleet_managed_job.drop_location).not_to be_nil
            expect(fleet_managed_job.drop_location).to be_a(Location)
            expect(request_body[:job][:drop_location]).to be_nil
            subject
            expect(response_code).to eql(200)
            expect(response_body[:drop_location]).to be_nil
            fleet_managed_job.reload
            expect(fleet_managed_job.drop_location).to be_nil
          end
        end

        context 'changing' do
          let!(:existing_drop_location) { create(:location, :new_jerzy_towing_hq) }
          let!(:new_drop_location) { create(:location, :durand_durand_towing_hq) }

          it "returns the job's changed drop location" do
            expect(fleet_managed_job.drop_location).not_to be_nil
            expect(fleet_managed_job.drop_location).to be_a(Location)
            expect(fleet_managed_job.drop_location.address).to eql("2381 18th Avenue, San Francisco, CA 94116")
            expect(request_body[:job][:drop_location]).not_to be_nil
            subject
            expect(response_code).to eql(200)
            expect(response_body['drop_location']).not_to be_nil
            expect(response_body['drop_location']['address']).to eql(new_drop_location.address)
            expect(response_body['drop_location']['lat']).to eql(new_drop_location.lat)
            expect(response_body['drop_location']['lng']).to eql(new_drop_location.lng)
          end
        end
      end

      context 'invalid' do
        let!(:existing_drop_location) { nil }

        describe 'without lat or lng' do
          let!(:new_drop_location) { create(:location, :durand_durand_towing_hq, lat: nil) }

          it 'errors' do
            expect(fleet_managed_job.drop_location).to be_nil
            subject
            expect(response_code).to eql(422)
            expect(response_body['message']).to include("must have a LAT and LNG")
          end
        end

        describe 'with invalid drop location object' do
          let(:new_drop_location_request_block) do
            {
              favorite_song: 'Barbie Girl',
            }
          end

          it 'does nothing' do
            expect(fleet_managed_job.drop_location).to be_nil
            subject
            expect(response_code).to eql(200)
            fleet_managed_job.reload
            expect(fleet_managed_job.drop_location).to be_nil
          end
        end
      end
    end
  end
end
