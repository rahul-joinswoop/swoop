# frozen_string_literal: true

require 'rails_helper'

describe 'Locations', type: :request do
  let(:response_body) { JSON.parse(response.body) }

  describe 'PATCH /api/v1/root/jobs/:id' do
    let(:access_token) do
      create(
        :doorkeeper_access_token,
        resource_owner_id: user.id,
        application: application,
      )
    end

    let(:service_location) { create(:location) }
    let(:drop_location) { create(:location) }

    let(:application) { create(:doorkeeper_application) }

    context 'as root' do
      let(:user) { create(:user, :root) }

      context 'with rescue jobs' do
        before do
          @job = create(:job, fleet_company: create(:fleet_company), rescue_company: create(:rescue_company))
        end

        it 'creates a location and enqueue a PlaceResolver' do
          expect(PlacesResolver).to receive(:perform_async)

          patch "/api/v1/root/jobs/#{@job.id}.json", headers: {
            'Authorization' => "Bearer #{access_token.token}",
          }, params: {
            job: {
              service_location: {
                address: "106 Pine St, Salina, KS 67401, USA",
                lat: 38.84144840936784,
                lng: -97.66230091479275,
                exact: true,
                place_id: "ChIJDzGjnOfOvIcRIzwSszHuWbI",
                site_id: nil,
                location_type_id: 1,
              },
              acts_as_company_id: user.company.id,
              id: @job.id,
              will_store: false,
            },
          }

          expect(response.status).to eq(200)
          expect(response_body['service_location'].keys).to include('street', 'street_name', 'street_number')
        end

        context 'update the location' do
          subject(:patch_job) do
            patch "/api/v1/root/jobs/#{@job.id}.json", headers: {
              'Authorization' => "Bearer #{access_token.token}",
            }, params: {
              job: {
                service_location: {
                  address: @address,
                  id: @job.service_location.id,
                },
                acts_as_company_id: user.company.id,
                id: @job.id,
                will_store: false,
              },
            }
          end

          it "doesn't enqueue the PlaceResolver" do
            @address = nil
            expect(PlacesResolver).not_to receive(:perform_async)

            @job.update!(service_location: create(:location, address: @address))
            patch_job

            expect(response.status).to eq(200)
            expect(response_body['service_location'].keys).to include('street', 'street_name', 'street_number')
            expect(response_body['service_location']['street_name']).to eq(nil)
            expect(response_body['service_location']['street_number']).to eq(nil)
          end
        end
      end
    end
  end
end
