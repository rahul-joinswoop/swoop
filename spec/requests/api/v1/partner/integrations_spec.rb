# frozen_string_literal: true

require "rails_helper"

describe "/api/v1/partner/integrations", type: :request do
  let!(:user) { create(:user, :rescue_with_sites) }
  let!(:token) { Doorkeeper::AccessToken.create!(resource_owner_id: user.id) }
  let!(:company) { user.company }

  describe "DELETE /:id" do
    subject do
      delete "/api/v1/partner/integrations/#{api_access_token.vendorid}",
             params: { access_token: token.token }
    end

    let!(:api_access_token) { create(:api_access_token, company: company) }

    before do
      allow_any_instance_of(Agero::Rsc::API).to receive(
        :unsubscribe_from_server_notifications
      ).and_return(true)

      allow_any_instance_of(Agero::Rsc::API).to receive(:sign_out).and_return(true)
    end

    context "when user is part of company" do
      include_context "basic rsc setup"

      # Assigning the access token to the rescue company created in "basic rsc setup"
      let!(:user) { create(:user, :rescue_with_sites, company: rescue_company) }
      let!(:api_access_token) { create(:api_access_token, company: rescue_company) }
      let(:vendor_id) { "vendor-id" }
      let(:facility_id) { "1" }

      it "is successful" do
        subject
        expect(response).to have_http_status(:ok)
        expect(Agero::Rsc::DisconnectOauthWorker).to have_enqueued_sidekiq_job(api_access_token.company_id, api_access_token.vendorid)
      end
    end

    context "when user is not part of company" do
      let(:fleet_company) { create(:fleet_company) }

      before do
        user.update!(company: fleet_company)
      end

      it "returns forbidden" do
        subject
        expect(response).to have_http_status(:forbidden)
      end

      it "does not delete the access token" do
        subject
        expect(Agero::Rsc::DisconnectOauthWorker.jobs).to be_empty
      end
    end
  end
end
