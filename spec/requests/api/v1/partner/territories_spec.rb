# frozen_string_literal: true

require 'rails_helper'

describe 'Territories', type: :request do
  let(:rescue_company) { create(:rescue_company) }
  let(:application) { create(:doorkeeper_application) }
  let(:user) { create(:user, :rescue, company: rescue_company) }

  let(:access_token) do
    create(
      :doorkeeper_access_token,
      resource_owner_id: user.id,
      application: application,
    )
  end

  let(:response_body) { JSON.parse(response.body) }

  describe 'DELETE /api/v1/partner/companies/:company_id/territories/destroy_all' do
    let(:other_rescue_company) { create :rescue_company }
    let!(:territory_belonging_to_the_other_rescue_company) do
      create(:territory, polygon_area: 'POLYGON ((-10.5 10.5, 10.5 10.5, 10.5 -10.5, -10.5 -10.5))', company: other_rescue_company)
    end
    let!(:territory) do
      create(:territory, polygon_area: 'POLYGON ((-10.5 10.5, 10.5 10.5, 10.5 -10.5, -10.5 -10.5))', company: rescue_company)
    end
    let(:company_id) { rescue_company.id }

    subject(:delete_territories) do
      delete "/api/v1/partner/companies/#{company_id}/territories/destroy_all",
             headers: {
               'Authorization' => "Bearer #{access_token.token}",
             }
    end

    context 'negative cases' do
      context 'entities are not found' do
        context 'company not found' do
          let(:company_id) { -1 }

          it "returns '404 - Not Found'" do
            delete_territories

            expect(response.status).to eq(404)
          end
        end
      end

      context 'unauthorized' do
        context 'calling with a fleet company which is not authorized' do
          let(:unauthorized_fleet_company) { create :fleet_company }
          let(:user) { create :user, :fleet_demo, company: unauthorized_fleet_company }

          it "returns '403 - Forbidden'" do
            delete_territories

            expect(response.status).to eq(403)
            expect(response_body).to include_json({
              "message" => "You are authenticated, but unauthorized to access this resource",
            })
          end
        end
      end
    end

    context 'positive cases' do
      it "returns 200" do
        delete_territories
        expect(response.status).to eq(200)
      end
    end
  end

  describe 'DELETE /api/v1/partner/territories/:territory_id' do
    let!(:territory) { create(:territory, polygon_area: 'POLYGON ((-10.5 10.5, 10.5 10.5, 10.5 -10.5, -10.5 -10.5))', company: rescue_company) }
    let(:company_id) { rescue_company.id }
    let(:territory_id) { territory.id }

    subject(:delete_territory) do
      delete "/api/v1/partner/territories/#{territory_id}",
             headers: {
               'Authorization' => "Bearer #{access_token.token}",
             }
    end

    context 'negative cases' do
      context 'entities are not found' do
        context 'territory not found' do
          let(:territory_id) { -1 }

          it "returns '404 - Not Found'" do
            delete_territory

            expect(response.status).to eq(404)
          end
        end
      end

      context 'unauthorized' do
        context 'calling with a territory which belongs to other rescue company' do
          let(:another_rescue_company) { create :rescue_company }
          let(:territory_belonging_to_another_rescue_company) { create(:territory, polygon_area: 'POLYGON ((-10.5 10.5, 10.5 10.5, 10.5 -10.5, -10.5 -10.5))', company: another_rescue_company) }
          let(:territory_id) { territory_belonging_to_another_rescue_company.id }

          it "returns '403 - Forbidden'" do
            delete_territory

            expect(response.status).to eq(403)
            expect(response_body).to include_json({
              "message" => "You are authenticated, but unauthorized to access this resource",
            })
          end
        end
      end
    end

    context 'positive cases' do
      it "returns 200" do
        delete_territory
        expect(response.status).to eq(200)
      end
    end
  end

  describe 'GET Requests' do
    let!(:first_territory_for_rescue_company) do
      create(:territory, polygon_area: 'POLYGON ((-10.5 10.5, 10.5 10.5, 10.5 -10.5, -10.5 -10.5))', company: rescue_company)
    end

    let!(:second_territory_for_rescue_company) do
      create(:territory, polygon_area: 'POLYGON ((-10.5 10.5, 10.5 10.5, 10.5 -10.5))', company: rescue_company)
    end

    let!(:territory_for_other_rescue_company) do
      create(:territory, polygon_area: 'POLYGON ((-10.5 10.5, 10.5 10.5, 10.5 -10.5))', company: create(:rescue_company))
    end

    describe 'GET /api/v1/partner/companies/:company_id/territories' do
      subject(:get_territories) do
        get "/api/v1/partner/companies/#{company_id_for_api_request}/territories", headers: {
          'Authorization' => "Bearer #{access_token.token}",
        }
      end

      context "Positive Cases" do
        context "When using authorized rescue company" do
          let(:company_id_for_api_request) { rescue_company.id }

          context "When rescue company has territories" do
            context "Responds with all territories" do
              before { get_territories }

              it "returns 200" do
                expect(response.status).to eq(200)
              end

              it "returns all territories for the rescue company" do
                expect(response_body['territories'].count).to eq(2)
              end

              it "doesn't return territories of other rescue companies" do
                expect(response_body['territories'].count).to eq(2)
                expect(response_body['territories'].collect { |t| t['id'] }).not_to include(territory_for_other_rescue_company.id)
              end

              it "returns correct response format and data" do
                expect(response_body.keys).to contain_exactly("territories")

                response_body["territories"].each do |territory|
                  expect(territory.keys).to contain_exactly("id", "paths", "deleted_at")
                end

                expect(response_body).to include_json({
                  "territories" => [
                    {
                      "paths" => [
                        {
                          "lat" => -10.5,
                          "lng" => 10.5,
                        }, {
                          "lat" => 10.5,
                          "lng" => 10.5,
                        }, {
                          "lat" => 10.5,
                          "lng" => -10.5,
                        }, {
                          "lat" => -10.5,
                          "lng" => -10.5,
                        }, {
                          "lat" => -10.5,
                          "lng" => 10.5,
                        },
                      ],
                      "deleted_at" => nil,
                    }, {
                      "paths" => [
                        {
                          "lat" => -10.5,
                          "lng" => 10.5,
                        }, {
                          "lat" => 10.5,
                          "lng" => 10.5,
                        }, {
                          "lat" => 10.5,
                          "lng" => -10.5,
                        }, {
                          "lat" => -10.5,
                          "lng" => 10.5,
                        },
                      ],
                      "deleted_at" => nil,
                    },
                  ],
                })
              end
            end

            it "does not include soft-deleted territories" do
              second_territory_for_rescue_company.update! deleted_at: Time.now

              get_territories
              expect(response.status).to eq(200)

              expect(response_body['territories'].count).to eq(1)
              expect(response_body['territories'].first['deleted_at']).to be_nil
            end
          end

          context "When the company doesn't have any territory" do
            let(:another_rescue_company) { create(:rescue_company) }
            let(:company_id_for_api_request) { another_rescue_company.id }
            let(:user) { create(:user, :rescue, company: another_rescue_company) }

            it 'returns an empty array' do
              get_territories
              expect(response.status).to eq(200)

              expect(response_body).to eq({
                "territories" => [],
              })
            end
          end
        end
      end

      context "Negative Cases" do
        context "When using unauthorized company" do
          let(:unauthorized_rescue_company) { create(:rescue_company) }
          let(:company_id_for_api_request) { unauthorized_rescue_company.id }

          it "returns '403 - Forbidden'" do
            get_territories

            expect(response.status).to eq(403)
            expect(response_body).to include_json({
              "message" => "You are authenticated, but unauthorized to access this resource",
            })
          end
        end
      end
    end

    describe 'GET /api/v1/partner/territories/:id' do
      let(:company_id_for_api_request) { rescue_company.id }

      subject(:get_territory) do
        get "/api/v1/partner/territories/#{territory_id}", headers: {
          'Authorization' => "Bearer #{access_token.token}",
        }
      end

      before { get_territory }

      context "Positive Cases" do
        context "When required territory exists" do
          let(:territory_id) { first_territory_for_rescue_company.id }

          it "returns the required territory for the company" do
            expect(response.status).to eq(200)
            expect(response_body.keys).to contain_exactly("territory")
            expect(response_body["territory"].keys).to contain_exactly("id", "paths", "deleted_at")

            expect(response_body).to include_json({
              "territory" => {
                "paths" => [
                  {
                    "lat" => -10.5,
                    "lng" => 10.5,
                  }, {
                    "lat" => 10.5,
                    "lng" => 10.5,
                  }, {
                    "lat" => 10.5,
                    "lng" => -10.5,
                  }, {
                    "lat" => -10.5,
                    "lng" => -10.5,
                  }, {
                    "lat" => -10.5,
                    "lng" => 10.5,
                  },
                ],
                "deleted_at" => nil,
              },
            })
          end
        end
      end

      context "Negative Cases" do
        context "When required territory doesn't exists" do
          let(:territory_id) { -1 }

          it "returns 404 - Not Found" do
            expect(response.status).to eq(404)
          end
        end

        context "Unauthorized Call" do
          let(:territory_id) { first_territory_for_rescue_company.id }

          let(:fleet_company) { create(:fleet_company, :tesla) }
          let(:company_id_for_api_request) { fleet_company.id }
          let(:user) { create(:user, :fleet_demo, company: fleet_company) }

          it "returns '403 - Forbidden'" do
            expect(response.status).to eq(403)
            expect(response_body).to include_json({
              "message" => "You are authenticated, but unauthorized to access this resource",
            })
          end
        end
      end
    end
  end

  describe 'POST /api/v1/partner/companies/:company_id/territories' do
    let(:company_id) { rescue_company.id }

    let(:params)  do
      {
        territory: {
          paths: [
            {
              lat: -10.5,
              lng: 10.5,
            },
            {
              lat: 10.5,
              lng: 10.5,
            },
            {
              lat: 10.5,
              lng: -10.5,
            },
            {
              lat: -10.5,
              lng: -10.5,
            },
          ],
        },
      }
    end

    subject(:create_territory) do
      post "/api/v1/partner/companies/#{company_id}/territories",
           params: params,
           headers: {
             'Authorization' => "Bearer #{access_token.token}",
           }
    end

    context 'negative cases' do
      context 'entities are not found' do
        context 'company not found' do
          let(:company_id) { -1 }

          it "returns '404 - Not Found'" do
            create_territory

            expect(response.status).to eq(404)
          end
        end
      end

      context 'unauthorized' do
        context 'calling with a rescue company which is not authorized' do
          let(:unauthorized_rescue_company) { create :rescue_company }
          let(:user) { create :user, :rescue, company: unauthorized_rescue_company }

          it "returns '403 - Forbidden'" do
            create_territory

            expect(response.status).to eq(403)
            expect(response_body).to include_json(
              { "message" => 'You are authenticated, but unauthorized to access this resource' }
            )
          end
        end
      end
    end

    context 'positive cases' do
      it "returns 201" do
        create_territory
        expect(response.status).to eq(201)
      end

      it "returns JSON" do
        create_territory

        expect(response_body).to include_json(
          { "territory" =>
            {
              "paths" =>
               [
                 { "lat" => -10.5, "lng" => 10.5 },
                 { "lat" => 10.5, "lng" => 10.5 },
                 { "lat" => 10.5, "lng" => -10.5 },
                 { "lat" => -10.5, "lng" => -10.5 },
                 { "lat" => -10.5, "lng" => 10.5 },
               ],
            } }
        )
      end
    end
  end
end
