# frozen_string_literal: true

require 'rails_helper'

describe 'PATCH /api/v1/partner/jobs/:id', type: :request do
  let(:rescue_company) { create(:rescue_company) }
  let(:rescue_company_user) { create(:user, :rescue, company: rescue_company) }
  let(:application) { create(:doorkeeper_application) }

  let(:access_token) do
    create(
      :doorkeeper_access_token,
      resource_owner_id: rescue_company_user.id,
      application: application,
    )
  end

  let(:fleet_company) { create(:fleet_company) }
  let(:account) { create(:account, company: rescue_company, client_company: fleet_company) }
  let(:invoice) { job.invoice }

  # Create services
  let(:fuel_delivery_service_code) { create(:service_code, name: ServiceCode::FUEL_DELIVERY) }
  let(:tow_service_code) { create(:service_code, name: ServiceCode::TOW) }
  let(:battery_jump_service_code) { create(:service_code, name: ServiceCode::BATTERY_JUMP) }
  let(:accident_tow_service_code) { create(:service_code, name: ServiceCode::ACCIDENT_TOW) }

  before do
    # Allow to run jobs asynchronously
    allow(CreateOrUpdateInvoice).to receive(:perform_async) { |job_id| CreateOrUpdateInvoice.new.perform(job_id) }
  end

  context "user updates service_code" do
    # Create Rates for the services
    let!(:battery_jump_miles_en_route_and_towed_rate) do
      create(
        :miles_p2p_rate,
        service_code: battery_jump_service_code,
        live: true,
        company_id: rescue_company.id,
        account: account
      )
    end
    let!(:battery_jump_miles_enroute) do
      create(
        :rate,
        :miles_enroute,
        type: 'MilesEnRouteRate',
        service_code: battery_jump_service_code,
        live: false,
        company_id: rescue_company.id,
        account: account
      )
    end
    let!(:accident_tow_miles_towed) do
      create(
        :rate,
        :miles_towed,
        type: 'MilesTowedRate',
        service_code: accident_tow_service_code,
        live: true,
        company_id: rescue_company.id,
        account: account
      )
    end

    let(:job) { create(:job, rescue_company: rescue_company, account: account, service_code: accident_tow_service_code) }

    subject(:patch_accident_tow_job) do
      patch "/api/v1/partner/jobs/#{job.id}.json", headers: {
        'Authorization' => "Bearer #{access_token.token}",
      }, params: { job: { service_code_id: battery_jump_service_code.id } }
    end

    # Create invoice and line items for the job
    before { CreateOrUpdateInvoice.new.perform(job.id) }

    it "updates the invoice using tow company's rate of the new service code" do
      expect(job.reload.invoice.rate_type).to eq('MilesTowedRate')
      expect(job.invoice.attributes['rate_type']).to be_nil

      patch_accident_tow_job
      expect(response.status).to eq(200)
      expect(job.reload.service_code).to eq(battery_jump_service_code)
      # It traverse invoice's line items and returns the description of the main line item
      expect(job.invoice.rate_type).to eq('MilesP2PRate')
      # The user has not overriden the rate type on the invoice, they've only changed the job's service code,
      # so the rate type override column on the invoice should still be null
      expect(job.invoice.attributes['rate_type']).to be_nil
    end
  end

  context "user updates the invoice rate type" do
    # Create Rates for the services
    let!(:fuel_delivery_miles_deadhead) do
      create(
        :miles_p2p_rate,
        service_code: fuel_delivery_service_code,
        live: true,
        company_id: rescue_company.id,
        account: account
      )
    end
    let!(:tow_hourly_p2p) do
      create(
        :hourly_p2p,
        service_code: tow_service_code,
        live: true,
        company_id: rescue_company.id,
        account: account
      )
    end
    let!(:tow_miles_enroute) do
      create(
        :rate,
        :miles_enroute,
        type: 'MilesEnRouteRate',
        service_code: tow_service_code,
        live: false,
        company_id: rescue_company.id,
        account: account
      )
    end

    let(:job) { create(:job, rescue_company: rescue_company, account: account, service_code: fuel_delivery_service_code) }

    subject(:patch_fuel_delivery_job) do
      patch "/api/v1/partner/jobs/#{job.id}.json", headers: {
        'Authorization' => "Bearer #{access_token.token}",
      }, params: { job: { service_code_id: tow_service_code.id } }
    end

    context 'update invoice rate type and then service code' do
      subject(:post_fuel_delivery_job_invoice) do
        post "/api/v1/partner/invoices.json", headers: {
          'Authorization' => "Bearer #{access_token.token}",
        }, params: {
          invoice: {
            state: 'partner_new',
            rate_type: 'MilesTowedRate',
            line_items: [{ job_id: job.id, description: 'Miles Towed Rate' }],
            job_id: job.id,
            recipient_id: rescue_company_user.id,
            recipient_type: 'User',
          },
          companyId: rescue_company.id,
        }
      end

      # Create invoice and line items for the job
      before { CreateOrUpdateInvoice.new.perform(job.id) }

      it 'retains the invoice rate type even after service code change' do
        expect(job.reload.invoice.rate_type).to eq('MilesP2PRate')
        expect(job.reload.invoice.attributes['rate_type']).to be_nil

        # Update job's invoice rate type
        post_fuel_delivery_job_invoice
        expect(response.status).to eq(201)

        # This will call the instance method `invoice.rate_type`
        expect(job.reload.invoice.rate_type).to eq('MilesTowedRate')
        # This will compare value of `rate_type` which is stored in the factory record.
        expect(job.reload.invoice.attributes['rate_type']).to eq('MilesTowedRate')

        # Update job's service code
        patch_fuel_delivery_job
        expect(response.status).to eq(200)
        expect(job.reload.service_code).to eq(tow_service_code)
        # It traverse invoice's line items and returns the description of the main line item
        expect(job.reload.invoice.rate_type).to eq('MilesTowedRate')
        # The user has overriden the rate type on the invoice,
        # so the rate type override column on the invoice should be updated as 'MilesTowRate'
        expect(job.reload.invoice.attributes['rate_type']).to eq('MilesTowedRate')
      end
    end
  end
end
