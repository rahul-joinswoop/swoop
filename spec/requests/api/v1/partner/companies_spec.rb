# frozen_string_literal: true

require 'rails_helper'

describe 'Company', type: :request do
  describe 'GET /api/v1/partner/companies/:id' do
    let(:rescue_company) { create(:rescue_company) }
    let(:application) { create(:doorkeeper_application) }
    let(:user) { create(:user, :rescue, company: rescue_company) }

    let(:access_token) do
      create(
        :doorkeeper_access_token,
        resource_owner_id: user.id,
        application: application,
      )
    end

    let!(:first_territory_for_rescue_company) do
      create(:territory, polygon_area: 'POLYGON ((-10.5 10.5, 10.5 10.5, 10.5 -10.5, -10.5 -10.5))', company: rescue_company)
    end

    let!(:second_territory_for_rescue_company) do
      create(:territory, polygon_area: 'POLYGON ((-10.5 10.5, 10.5 10.5, 10.5 -10.5))', company: rescue_company)
    end

    let!(:soft_deleted_territory_for_rescue_company) do
      create(:territory, polygon_area: 'POLYGON ((-10.5 10.5, 10.5 10.5, 10.5 -10.5))', company: rescue_company, deleted_at: Time.now)
    end

    let!(:territory_for_other_rescue_company) do
      create(:territory, polygon_area: 'POLYGON ((-10.5 10.5, 10.5 10.5, 10.5 -10.5))', company: create(:rescue_company))
    end

    let(:response_body) { JSON.parse(response.body) }

    subject(:get_company) do
      get "/api/v1/partner/companies/#{company_id_for_api_request}.json", headers: {
        'Authorization' => "Bearer #{access_token.token}",
      }
    end

    context "Positive Cases" do
      context "When using authorized company" do
        let(:company_id_for_api_request) { rescue_company.id }

        context "When company have territories" do
          context "Responds with all territories" do
            before { get_company }

            it "returns response status - 200" do
              expect(response.status).to eq(200)
            end

            it "returns all territories for the company, except soft-deleted one" do
              expect(response_body['territories'].count).to eq(2)
              expect(response_body['territories'].collect { |t| t['deleted_at'] }).to eq([nil, nil])
            end

            it "doesn't return territories of other rescue companies" do
              expect(response_body['territories'].collect { |t| t['id'] }).not_to include(territory_for_other_rescue_company.id)
            end

            it "returns correct response format and data" do
              expect(response_body).to include_json({
                "territories" => [
                  {
                    "paths" => [
                      {
                        "lat" => -10.5,
                        "lng" => 10.5,
                      }, {
                        "lat" => 10.5,
                        "lng" => 10.5,
                      }, {
                        "lat" => 10.5,
                        "lng" => -10.5,
                      }, {
                        "lat" => -10.5,
                        "lng" => -10.5,
                      }, {
                        "lat" => -10.5,
                        "lng" => 10.5,
                      },
                    ],
                    "deleted_at" => nil,
                  }, {
                    "paths" => [
                      {
                        "lat" => -10.5,
                        "lng" => 10.5,
                      }, {
                        "lat" => 10.5,
                        "lng" => 10.5,
                      }, {
                        "lat" => 10.5,
                        "lng" => -10.5,
                      }, {
                        "lat" => -10.5,
                        "lng" => 10.5,
                      },
                    ],
                    "deleted_at" => nil,
                  },
                ],
              })
            end
          end
        end

        context "When the company doesn't have any territory" do
          let(:rescue_company) { create(:rescue_company) }
          let(:company_id_for_api_request) { rescue_company.id }
          let(:application) { create(:doorkeeper_application) }
          let(:user) { create(:user, :rescue, company: rescue_company) }

          let(:access_token) do
            create(
              :doorkeeper_access_token,
              resource_owner_id: user.id,
              application: application,
            )
          end

          it 'returns an empty array' do
            get_company
            expect(response.status).to eq(200)

            expect(response_body).to include_json({
              "territories" => [],
            })
          end
        end
      end
    end

    context "Negative Cases" do
      context "When using unauthorized company" do
        let(:unauthorized_rescue_company) { create(:rescue_company) }
        let(:company_id_for_api_request) { unauthorized_rescue_company.id }

        it "returns '403 - Forbidden'" do
          get_company

          expect(response.status).to eq(403)
          expect(response_body).to eq({
            "status" => "403",
            "message" => "You are authenticated, but unauthorized to access this resource",
          })
        end
      end
    end
  end
end
