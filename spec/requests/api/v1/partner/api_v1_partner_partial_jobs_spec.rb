# frozen_string_literal: true

require 'rails_helper'

describe 'Partner PartialJobs', type: :request do
  let(:response_body) { JSON.parse(response.body) }

  describe 'GET /api/v1/partner/partial_jobs' do
    let(:access_token) do
      create(
        :doorkeeper_access_token,
        resource_owner_id: user.id,
        application: application,
      )
    end

    let(:service_location) { create(:location) }
    let(:drop_location) { create(:location) }

    let(:application) { create(:doorkeeper_application) }

    context 'as a partner' do
      let(:user) { create(:user, :rescue) }

      context 'with no rescue jobs' do
        it 'responds with no jobs' do
          get '/api/v1/partner/partial_jobs', headers: {
            'Authorization' => "Bearer #{access_token.token}",
          }

          expect(response.status).to eq(200)
          expect(response_body['meta']['total']).to eq(0)
          expect(response_body['jobs']).to eq([])
        end
      end

      context 'with rescue jobs' do
        before do
          user.company.rescue_jobs << create_list(
            :rescue_job,
            2,
            service_location: service_location,
            drop_location: drop_location
          )
          user.company.save!

          # these need to be different than our user's company
          create_list(:job, 4, fleet_company: create(:fleet_company), rescue_company: create(:rescue_company))
        end

        it 'responds with the jobs' do
          get '/api/v1/partner/partial_jobs', headers: {
            'Authorization' => "Bearer #{access_token.token}",
          }

          expect(response.status).to eq(200)
          expect(response_body.keys).to include 'jobs'
          expect(response_body['meta']['total']).to eq(2)
          expect(response_body['jobs'].count).to eq(2)

          expect(
            response_body['jobs'].first
            .keys
          ).to include('will_store')

          expect(
            response_body['jobs'].first['service_location']
              .keys
          ).to include('location_type_id')
        end
      end

      context 'as a non partner' do
        let(:user) { create(:user, :fleet_demo) }

        context 'with no jobs' do
          it 'does it' do
            get '/api/v1/partner/partial_jobs', headers: {
              'Authorization' => "Bearer #{access_token.token}",
            }

            expect(response.status).to eq(403)
          end
        end
      end
    end
  end
end
