// generate our babelrc based on our environment - code based on
// https://github.com/facebook/create-react-app/blob/next/packages/babel-preset-react-app/create.js
const okEnvs = ['production', 'development', 'test']

module.exports = (api) => {
  api.cache.using(() => process.env.NODE_ENV)

  const config = {
    presets: [
      ['@babel/preset-env', {
        // `entry` transforms `@babel/polyfill` into individual requires for
        // the targeted browsers. This is safer than `usage` which performs
        // static code analysis to determine what's required.
        // This is probably a fine default to help trim down bundles when
        // end-users inevitably import '@babel/polyfill'.
        useBuiltIns: 'entry',
        corejs: 2,
        // Do not transform modules to CJS
        modules: false,
      }],
      ['@babel/preset-react', {
        // Adds component stack to warning messages
        // Adds __self attribute to JSX which React will use for some warnings
        development: api.env(['development', 'test']),
        // Will use the native built-in instead of trying to polyfill
        // behavior for any plugins that require one.
        useBuiltIns: true,
      }],
    ],
    plugins: [
      // react-hot-loader is a noop in production
      'react-hot-loader/babel',
      // add 'use strict' everywhere
      '@babel/plugin-transform-strict-mode',
      '@babel/plugin-transform-modules-commonjs',
      // we should figure out what we do that depends on this - in es6 classes you
      // can do:
      //
      // class Foo {
      //   static bar = () => {
      //     // this is now our class
      //   }
      // }
      //
      // for whenever you need this functionality.
      'autobind-class-methods',
      // Necessary to include regardless of the environment because
      // in practice some other transforms (such as object-rest-spread)
      // don't work without it: https://github.com/babel/babel/issues/7215
      '@babel/plugin-transform-destructuring',
      // this has to come before plugin-proposal-class-properties, see
      // https://babeljs.io/docs/en/next/babel-plugin-proposal-decorators.html
      [
        '@babel/plugin-proposal-decorators',
        {
          // Use the legacy (stage 1) decorators syntax and behavior.
          legacy: true,
        },
      ],
      // class { handleClick = () => { } }
      // Enable loose mode to use assignment instead of defineProperty
      // See discussion in https://github.com/facebook/create-react-app/issues/4263
      ['@babel/plugin-proposal-class-properties', { loose: true }],
      // The following two plugins use Object.assign directly, instead of Babel's
      // extends helper. Note that this assumes `Object.assign` is available.
      // { ...todo, completed: true }
      // Will use the native built-in instead of trying to polyfill behavior
      ['@babel/plugin-proposal-object-rest-spread', { useBuiltIns: true }],
      // Polyfills the runtime needed for async/await and generators
      ['@babel/plugin-transform-runtime', {
        helpers: false,
        regenerator: true,
      }],
      // do our (js) module resolution here instead of in webpack, makes things
      // easier for test + eslint.
      // TODO - can't make this work for webpack assets?
      ['babel-plugin-module-resolver', {
        root: ['./app/assets/javascripts'],
        extensions: ['.js', '.jsx', '.coffee'],
        alias: {
          images: './app/assets/images',
          fonts: './app/assets/fonts',
          stylesheets: './app/assets/stylesheets',
          public: './public',
          vendor: './vendor/assets',
        },
      }],
      // from here on the ordering was built by babel-upgrade so be careful when
      // modifying it
      '@babel/plugin-syntax-dynamic-import',
      '@babel/plugin-syntax-import-meta',
      '@babel/plugin-proposal-json-strings',
      '@babel/plugin-proposal-function-sent',
      '@babel/plugin-proposal-export-namespace-from',
      '@babel/plugin-proposal-numeric-separator',
      '@babel/plugin-proposal-throw-expressions',
      '@babel/plugin-proposal-export-default-from',
      '@babel/plugin-proposal-logical-assignment-operators',
      '@babel/plugin-proposal-optional-chaining',
      [
        '@babel/plugin-proposal-pipeline-operator',
        {
          // The Pipeline Proposal is one of three competing implementations. Which
          // implementation the plugin should use is configured with the "proposal"
          // option. This option is required and should be one of:
          //
          // "minimal" – Minimal Pipeline
          // "fsharp" - F#-Style Pipeline (Coming Soon!)
          // "smart" - Smart Pipeline (Coming Soon!)
          // Only "minimal" is currently supported. The other two flags are in progress.
          //
          // When one of the implementations is accepted, it will become the default
          // and the "proposal" option will no longer be required.
          proposal: 'minimal',
        },
      ],
      '@babel/plugin-proposal-nullish-coalescing-operator',
      '@babel/plugin-proposal-do-expressions',
      '@babel/plugin-proposal-function-bind',
      // transform __DEV__, __TEST__, and __PROD__ to boolean values, similar to
      // https://www.npmjs.com/package/babel-plugin-dev-expression
      ['minify-replace', {
        replacements: [
          {
            identifierName: '__DEV__',
            replacement: {
              type: 'booleanLiteral',
              value: api.env('development'),
            },
          },
          {
            identifierName: '__TEST__',
            replacement: {
              type: 'booleanLiteral',
              value: api.env('test'),
            },
          },
          {
            identifierName: '__PROD__',
            replacement: {
              type: 'booleanLiteral',
              value: api.env('production'),
            },
          },
        ],
      }],

    ],
    compact: true,
  }

  if (!api.env(okEnvs)) {
    throw new Error(
      'Using babel requires that you specify the NODE_ENV environment variable.' +
      `Valid values are ${JSON.stringify(okEnvs)}.` +
      `Instead, received: ${JSON.stringify(api.env())}.`,
    )
  }

  if (api.env('test')) {
    // Transform dynamic import to require
    config.plugins.push('babel-plugin-transform-dynamic-import')
  } else {
    // function* () { yield 42; yield 43; }
    config.plugins.push(['@babel/plugin-transform-regenerator',
      {
        // Async functions are converted to generators by @babel/preset-env
        async: false,
      },
    ])
  }

  if (api.env('production')) {
    // Remove PropTypes from production build
    config.plugins.push(['babel-plugin-transform-react-remove-prop-types', {
      removeImport: true,
    }])
  }
  return config
}
